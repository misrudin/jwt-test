<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware(['cors'])->group(function () {
Route::prefix('v1')->group(function () {
    Route::get('/', function () {
        return Response::json([
            "status" => 200,
            "message" => "tomps-pm-svc-sysreport"
        ]);
    });
    Route::get('/ping', function () {
        return Response::json([
            "status" => 200,
            "message" => "tomps-pm-svc-sysreport"
        ]);
    });
    //Skala Proyek
    Route::middleware([ 'jwt.verify' ])->group(function() {
    Route::get('/test', 'App\Http\Controllers\DashboardController@test');

    Route::get('/skalaProyek/getCountAllSkalaProyek', 'App\Http\Controllers\VwSkalaProyekController@getCountAllSkalaProyek');
    Route::get('/skalaProyek/getAllSkalaProyek', 'App\Http\Controllers\VwSkalaProyekController@getAllSkalaProyek');
    Route::get('/skalaProyek/getAllSkalaProyekBySkalaName', 'App\Http\Controllers\VwSkalaProyekController@getAllSkalaProyekBySkalaName');

    //Progress Proyek
    Route::get('/progressProyek/getCountAllProgressProyek', 'App\Http\Controllers\VwProgressProyekController@getCountAllProgressProyek');
    Route::get('/progressProyek/listProgressProyekStatus', 'App\Http\Controllers\VwProgressProyekController@listProgressProyekStatus');
    Route::get('/progressProyek/getGrafikAllProgressProyek', 'App\Http\Controllers\VwProgressProyekController@getGrafikAllProgressProyek');

    

    //Anggaran Proyek
    Route::get('/anggaranProyek/getCountAllAnggaranProyek', 'App\Http\Controllers\VwAnggaranProyekController@getCountAllAnggaranProyek');

    //Anggaran Proyek General
    Route::get('/anggaranProyek/getCountAllAnggaranProyekGeneral', 'App\Http\Controllers\VwAnggaranProyekGeneralController@getCountAllAnggaranProyekGeneral');


    //Term of Payment
    Route::get('/termOfPayment/getAllTermOfPaymentProyek', 'App\Http\Controllers\VwTermOfPaymentController@getAllTermOfPaymentProyek');

    

    //Kategori Proyek
    Route::get('/kategoriProyek/getAllKatergoriProyek', 'App\Http\Controllers\VwKategoriProyekController@getAllKatergoriProyek');

    

    //Segmen Proyek
    Route::get('/segmenProyek/getAllSegmenProyek', 'App\Http\Controllers\VwSegmenProyekController@getAllSegmenProyek');

    //Stake Holder
    Route::get('/stakeHolder/getAllStakeHolder', 'App\Http\Controllers\VwStakeHolderController@getAllStakeHolder');

    //Stake Holder General
    Route::get('/stakeHolder/getAllStakeHolderGeneral', 'App\Http\Controllers\VwStakeholderGeneralController@getAllStakeHolderGeneral');


    //Lokasi Proyek
    Route::get('/lokasiProyek/getAllLokasiProyek', 'App\Http\Controllers\VwLokasiProyekController@getAllLokasiProyek');

    //Issue Proyek
    Route::get('/issueProyek/getAllIssueProyek', 'App\Http\Controllers\VwIssueProyekController@getAllIssueProyek');

    


    Route::get('/stackholder/listPartnerProyek', 'App\Http\Controllers\VwStakholderController@listPartnerProyek');
    Route::get('/stackholder/searchpartnerproyek', 'App\Http\Controllers\VwStakholderController@searchPartnerProyek');
    Route::get('/stackholder/listPartnerProyekDocument/{ProjectId}', 'App\Http\Controllers\VwStakholderController@listPartnerProyekDocument');

    Route::get('/stackholder/listProjectManager', 'App\Http\Controllers\VwStakholderController@listProjectManager'); ///////////////
    Route::get('/stackholder/searchprojectmanager', 'App\Http\Controllers\VwStakholderController@searchProjectManager');
    Route::get('/stackholder/ProjectManagerDetail/{code}', 'App\Http\Controllers\VwStakholderController@ProjectManagerDetail'); ///////////////

    Route::get('/project/listSegment/', 'App\Http\Controllers\VwProjectController@listSegment');
    Route::get('/project/searchlistsegment/', 'App\Http\Controllers\VwProjectController@searchListSegment');

    Route::get('/project/listRepostCost/', 'App\Http\Controllers\VwProjectController@listRepostCost');
    Route::get('/project/searchlistreportcost/', 'App\Http\Controllers\VwProjectController@searchListRepostCost');

    Route::get('/dokument/listDirektoriDocument/', 'App\Http\Controllers\VwDocumentController@listDirektoriDocument');
    Route::get('/dokument/listDirektoriDocumentByHirarki/', 'App\Http\Controllers\VwDocumentController@listDirektoriDocumentByHirarki'); ///////////////
    Route::get('/dokument/searchListDirektoriDocument/', 'App\Http\Controllers\VwDocumentController@searchListDirektoriDocument');


    Route::get('/laporan/dokumen/documentLopWin/', 'App\Http\Controllers\Laporan\VwLaporanDocumentProyekController@documentLopWin');

    Route::get('/laporan/dokumen/documentSpkMitra/', 'App\Http\Controllers\Laporan\VwLaporanDocumentProyekController@documentSpkMitra');
    Route::get('/laporan/dokumen/filterDocumentSpkMitra/', 'App\Http\Controllers\Laporan\VwLaporanDocumentProyekController@filterDocumentSpkMitra');


    Route::get('/laporan/dokumen/documentTopKb/', 'App\Http\Controllers\Laporan\VwLaporanDocumentProyekController@documentTopKb');
    Route::get('/laporan/dokumen/documentTopKl/', 'App\Http\Controllers\Laporan\VwLaporanDocumentProyekController@documentTopKl');
    Route::get('/laporan/dokumen/getAllSegment/', 'App\Http\Controllers\Laporan\VwLaporanDocumentProyekController@getAllSegment');
    Route::get('/laporan/dokumen/regional/', 'App\Http\Controllers\Laporan\VwLaporanRegionalController@getAllRegional');
    Route::get('/laporan/dokumen/get-regional/', 'App\Http\Controllers\Laporan\VwLaporanRegionalController@getRegionalType');
    Route::get('/laporan/dokumen/get-pic/', 'App\Http\Controllers\Laporan\VwLaporanRegionalController@getPicType');

    // Dashboard

    Route::get('/dashboard/getStatusProyek/', 'App\Http\Controllers\DashboardController@getStatusProyek');
    Route::get('/dashboard/getDetailStatusProyek/', 'App\Http\Controllers\DashboardController@getDetailStatusProyek');

    // Dashboad Proyek General
    Route::get('/dashboard/getStatusProyekGeneral/', 'App\Http\Controllers\DashboardGeneralController@getStatusProyekGeneral');
    Route::get('/dashboard/getDetailStatusProyekGeneral/', 'App\Http\Controllers\DashboardGeneralController@getDetailStatusProyekGeneral');
    // Dasboard Proyek skala
    Route::get('/skalaProyek/getCountAllSkalaProyekGeneral', 'App\Http\Controllers\DashboardGeneralController@getCountAllSkalaProyekGeneral');


    ////LAPORAN////
    //Project Manager
    Route::get('/laporan/projectManager', 'App\Http\Controllers\Laporan\VwLaporanProjectManagerController@index');

    Route::get('/laporan/projectManager/searchProjectManager', 'App\Http\Controllers\Laporan\VwLaporanProjectManagerController@searchProjectManager');

    Route::get('/laporan/projectManager/profile', 'App\Http\Controllers\Laporan\VwLaporanProjectManagerController@getProfileProjectManager');
    Route::get('/laporan/projectManager/searchProjectManagerProfile', 'App\Http\Controllers\Laporan\VwLaporanProjectManagerController@searchProjectManagerProfile');

    Route::get('/laporan/projectManager/projectDetail', 'App\Http\Controllers\Laporan\VwLaporanProjectManagerController@getProjectDetailProjectManager');
    Route::get('/laporan/projectManager/searchProjectManagerProjectDetail', 'App\Http\Controllers\Laporan\VwLaporanProjectManagerController@searchProjectManagerProjectDetail');

    //Progress Pencapaian
    Route::get('/laporan/progressPencapaian/getLaporanAllProgressPencapaian', 'App\Http\Controllers\Laporan\VwLaporanProgressPencapaianController@getLaporanAllProgressPencapaian');

    Route::get('/laporan/progressPencapaian/searchLaporanProgressPencapaian', 'App\Http\Controllers\Laporan\VwLaporanProgressPencapaianController@searchLaporanProgressPencapaian');

    Route::get('/laporan/progressPencapaian/getLaporanFilterProgressPencapaian', 'App\Http\Controllers\Laporan\VwLaporanProgressPencapaianController@getLaporanFilterProgressPencapaian');

    //Skala Proyek
    Route::get('/laporan/skalaProyek/getLaporanAllSkalaProyek', 'App\Http\Controllers\Laporan\VwLaporanSkalaProyekController@getLaporanAllSkalaProyek');
    Route::get('/laporan/skalaProyek/getValueSkala', 'App\Http\Controllers\Laporan\VwLaporanSkalaProyekController@getValueSkala');
    
    Route::get('/laporan/skalaProyek/searchLaporanSkalaProyek', 'App\Http\Controllers\Laporan\VwLaporanSkalaProyekController@searchLaporanSkalaProyek');

    Route::get('/laporan/skalaProyek/getLaporanFilterSkalaProyek', 'App\Http\Controllers\Laporan\VwLaporanSkalaProyekController@getLaporanFilterSkalaProyek');

    //Category Project
    Route::get('/laporan/categorySegmentProyek/getLaporanAllCategorySegmentProyek', 'App\Http\Controllers\Laporan\VwLaporanCategorySegmentProyekController@getLaporanAllCategorySegmentProyek');
    Route::get('/laporan/categorySegmentProyek/searchLaporanCategorySegmentProyek', 'App\Http\Controllers\Laporan\VwLaporanCategorySegmentProyekController@searchLaporanCategorySegmentProyek');

    //Anggaran Project
    Route::get('/laporan/anggaranProyek/getLaporanAllAnggaranProyek', 'App\Http\Controllers\Laporan\VwLaporanAnggaranProyekController@getLaporanAllAnggaranProyek');

    Route::get('/laporan/anggaranProyek/searchLaporanAnggaranProyek', 'App\Http\Controllers\Laporan\VwLaporanAnggaranProyekController@searchLaporanAnggaranProyek');

    Route::get('/laporan/anggaranProyek/getLaporanFilterAnggaranProyek', 'App\Http\Controllers\Laporan\VwLaporanAnggaranProyekController@getLaporanFilterAnggaranProyek');

    //Issue Project
    Route::get('/laporan/issueProyek/getLaporanAllIssueProyek', 'App\Http\Controllers\Laporan\VwLaporanIssueProyekController@getLaporanAllIssueProyek');

    Route::get('/laporan/issueProyek/searchIssueProyek', 'App\Http\Controllers\Laporan\VwLaporanIssueProyekController@searchIssueProyek');


    //Lokasi Project
    Route::get('/laporan/lokasiProyek/getLaporanAllLokasiProyek', 'App\Http\Controllers\Laporan\VwLaporanLokasiProyekController@getLaporanAllLokasiProyek');

    Route::get('/laporan/lokasiProyek/searchLokasiProyek', 'App\Http\Controllers\Laporan\VwLaporanLokasiProyekController@searchLokasiProyek');


    //TermOfPayment Project
    Route::get('/laporan/termOfPaymentProyek/getLaporanAllTermOfPaymentProyek', 'App\Http\Controllers\Laporan\VwLaporanTermOfPaymentProyekController@getLaporanAllTermOfPaymentProyek');


    //Partner & Customer Project
    Route::get('/laporan/partnerCustomer/getLaporanAllPartnerCustomerProyek', 'App\Http\Controllers\Laporan\VwLaporanPartnerCustomerProyekController@getLaporanAllPartnerCustomerProyek');

   
    // SUmber Anggaran General
    Route::get('/anggaran/getSumberAnggaranGeneral', 'App\Http\Controllers\VwSumberAnggaranGeneralController@getSumberAnggaranGeneral');

    
    // DASHBOARD GENERAL
    // Progress Proyek General
    Route::get('/progressProyek/getCountAllProgressProyekGeneral', 'App\Http\Controllers\VwProgressProyekGeneralController@getCountAllProgressGeneral');
    Route::get('/progressProyek/listProgressProyekStatusGeneral', 'App\Http\Controllers\VwProgressProyekGeneralController@listProgressProyekStatusGeneral');
    Route::get('/progressProyek/getGrafikAllProgressProyekGeneral', 'App\Http\Controllers\VwProgressProyekGeneralController@getGrafikAllProgressProyekGeneral');
    //Term of Payment General
    Route::get('/termOfPayment/getAllTermOfPaymentProyekGeneral', 'App\Http\Controllers\VwDashboardTopGeneralController@getAllTopProyekGeneral');
     // Detail Pembayaran General
     Route::get('/detail-pembayaran/getDetailPembayaranGeneral', 'App\Http\Controllers\VwDashboardTopGeneralController@getDetailPembayaranGeneral');
    //Kategori Proyek General
    Route::get('/kategoriProyek/getAllKatergoriProyekGeneral', 'App\Http\Controllers\VwKategoriProyekGeneralController@getAllKatergoriProyekGeneral');

    //Segmen Proyek General
    Route::get('/segmenProyek/getAllSegmenProyekGeneral', 'App\Http\Controllers\VwKategoriProyekGeneralController@getAllSegmenProyekGeneral');
    //Issue Proyek General
    Route::get('/issueProyek/getAllIssueProyekGeneral', 'App\Http\Controllers\VwIssueProyekGeneralController@getAllIssueProyekGeneral');
    // Peringkat proyek anggaran general
    Route::get('/anggaran/getPeringkatProyekGeneral', 'App\Http\Controllers\VwPeringkatProyekController@index');
    Route::get('/anggaran/getMainProyekGeneral', 'App\Http\Controllers\VwMainProyekGeneralController@index');
    Route::get('/dashboard/getLessonProyekGeneral', 'App\Http\Controllers\VwLessonLearnedController@index');
    Route::get('/dashboard/getResikoGeneral', 'App\Http\Controllers\VwResikoGeneralController@index');
    Route::get('/dashboard/getLokasiGeneral', 'App\Http\Controllers\VwLokasiGeneraalController@index');
    


    // SUMMARY
    Route::get('/dashboard-detail/summary/{projectId}', '\App\Http\Controllers\DashboardDetailController@getSummary');

    //SCURVES
    Route::get('/dashboard-detail/scurves/{projectId}', '\App\Http\Controllers\DashboardDetailController@getSCurves');
    Route::post('/dashboard-detail/scurves-chart', '\App\Http\Controllers\DashboardDetailController@getChartScurves');
    Route::get('/dashboard-detail/filter-scurves-chart', '\App\Http\Controllers\DashboardDetailController@filterScurvesChart');

    //EARN VALUE ANALYSIS
    Route::get('/dashboard-detail/earnValueAnalysis/get/{projectId}', '\App\Http\Controllers\DashboardDetailController@getEarnValueAnalysis');
    Route::post('/dashboard-detail/earnValueAnalysis/', '\App\Http\Controllers\DashboardDetailController@saveEarnValue');
    Route::put('/dashboard-detail/earnValueAnalysis/', '\App\Http\Controllers\DashboardDetailController@updateEarnValue');
    Route::delete('/dashboard-detail/earnValueAnalysis/{taskId}', '\App\Http\Controllers\DashboardDetailController@deleteEarnValue');
    Route::post('/dashboard-detail/earnValueAnalysis/detail', '\App\Http\Controllers\DashboardDetailController@saveEarnValueDetail');
    Route::post('/dashboard-detail/earnValueAnalysis/actualCost', '\App\Http\Controllers\DashboardDetailController@saveActualCost');
    Route::get('/dashboard-detail/earnValueAnalysis/chart', '\App\Http\Controllers\DashboardDetailController@getEarnValueChart');
    Route::post('/dashboard-detail/earnValueAnalysis/getChart', '\App\Http\Controllers\DashboardDetailController@getEarnValueChartPost');
    Route::get('/dashboard-detail/earnValueAnalysis/getEarnValueReal/{taskId}', '\App\Http\Controllers\DashboardDetailController@getEarnValueReal');

    // API DOWNLOAD
    Route::get('/download/laporan/categorySegmentProyek','\App\Http\Controllers\Download\LaporanCategoryProjectController@exportCategory');
    Route::get('/download/laporan/anggaranProyek','\App\Http\Controllers\Download\LaporanAnggaranProjectController@getLaporanAnggaran');
    Route::get('/download/laporan/skalaProyek','\App\Http\Controllers\Download\LaporanSkalaProjectController@getAllSkalaProyek');
    Route::get('/download/laporan/progressProyek','\App\Http\Controllers\Download\LaporanProgressProjectController@getAllProgress');
    Route::get('/download/laporan/termOfPaymentProyek','\App\Http\Controllers\Download\LaporanTopProjectController@getAllTop');
    Route::get('/download/laporan/locationProyek','\App\Http\Controllers\Download\LaporanLocationProjectController@getAllLocation');
    Route::get('/download/laporan/documentSpk','\App\Http\Controllers\Download\LaporanDocumentSpkController@getAllDocumentSpk');
    Route::get('/download/laporan/documentTopKb','\App\Http\Controllers\Download\LaporanDocumentTopKbController@getAllTopKb');
    Route::get('/download/laporan/documentTopKl','\App\Http\Controllers\Download\LaporanDocumentTopKlController@getAllDocumentKl');
    Route::get('/download/laporan/documentLopWin','\App\Http\Controllers\Download\LaporanDocumentLopWinController@getAllDocumentLopWin');
    Route::get('/download/laporan/getAllStakeholder','\App\Http\Controllers\Download\LaporanStakeholderController@getAllStakeholder');
    Route::get('/download/laporan/getAllIssue','\App\Http\Controllers\Download\LaporanIssueProjectController@getAllIssue');
    Route::get('/download/laporan/getAllRegional','\App\Http\Controllers\Download\LaporanRegionalController@getAllRegionalDownload');

    // HELP
    Route::resource('/help/contactUs','\App\Http\Controllers\ContactUsController');
    Route::put('/help/contactUs','\App\Http\Controllers\ContactUsController@updateHelp');
    // CATEGORY FAQ
    Route::resource('/help/categoryFaq','\App\Http\Controllers\CategoryFaqController');
    Route::put('/help/categoryFaq','\App\Http\Controllers\CategoryFaqController@update');
    Route::get('/help/getAllFaqs','\App\Http\Controllers\CategoryFaqController@getAllFaqs');
    Route::post('/help/updateAllFaq','\App\Http\Controllers\CategoryFaqController@updateAll');
    
    // FAQS
    Route::resource('/help/faqs','\App\Http\Controllers\FaqController');
    Route::put('/help/faqs','\App\Http\Controllers\FaqController@update');
    // CATEGORY VIDEO
    Route::resource('/help/categoryVideo','\App\Http\Controllers\CategoryVideoController');
    Route::put('/help/categoryVideo','\App\Http\Controllers\CategoryVideoController@update');
    Route::get('/help/getAllVideo','\App\Http\Controllers\CategoryVideoController@getAllVideo');
    Route::post('/help/updateAllVideo','\App\Http\Controllers\CategoryVideoController@updateAll');
    // Tutor Video
    Route::resource('/help/videoTutorial','\App\Http\Controllers\VideoTutorialController');
    Route::put('/help/videoTutorial','\App\Http\Controllers\VideoTutorialController@update');


    // Cost conclusion
    Route::get('/dashboard/costConclusion','\App\Http\Controllers\CostScheduleController@costConclusion');
    Route::get('/dashboard/scheduleConclusion','\App\Http\Controllers\CostScheduleController@scheduleConclusion');

    });
});

//});
