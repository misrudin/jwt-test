<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\ProjectFileController;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'api',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])
->prefix('api/schedule')
->group(function() {
    Route::get('/', fn () => response()->success() );
    Route::get('/{p_id}', [ScheduleController::class, "show"]);
    Route::post('/store/{p_id}', [ScheduleController::class, "store"]);
});

Route::middleware([
    'api',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])
->prefix('api/project_file')
->group(function() {
    Route::get('/',[ProjectFileController::class, "list"]);
    Route::get('/{p_id}', [ProjectFileController::class, "show"]);
    Route::post('/store/{p_id}', [ProjectFileController::class, "store"]);
});