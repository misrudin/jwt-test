<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Response::json([
//         "status" => 200,
//         "message" => "tomps-pm-svc-file",
//         "description" => "Application API --APP_ENV-- Environment."
//     ]);
// });

// Route::get('/ping', function () {
//     return Response::json([
//         "status" => 200,
//         "message" => "tomps-pm-svc-file",
//         "description" => "Application API --APP_ENV-- Environment."
//     ]);
// });

// $router->get('/info_php', function () use ($router) {
//     phpinfo();
// });

// Route::any('/schedule', function () {
//     return redirect("/api/schedule");
// });

// Route::middleware(['cors'])->group(function () {
    
// Route::get('/project_file/list', 'App\Http\Controllers\ProjectFileController@list');
// Route::get('/project_file/show/', 'App\Http\Controllers\ProjectFileController@show');
// Route::get('/project_file/search/', 'App\Http\Controllers\ProjectFileController@search');
// Route::post('/project_file/insert', 'App\Http\Controllers\ProjectFileController@insert');
// Route::get('/project_file/download/{id}', 'App\Http\Controllers\ProjectFileController@download');
// Route::get('/project_file/delete/', 'App\Http\Controllers\ProjectFileController@delete');

// Route::get('/category_file/list', 'App\Http\Controllers\CategoryFileController@list');
// Route::get('/category_file/show/{id}', 'App\Http\Controllers\CategoryFileController@show');
// Route::post('/category_file/insert', 'App\Http\Controllers\CategoryFileController@insert');
// Route::post('/category_file/update', 'App\Http\Controllers\CategoryFileController@update');
// Route::get('/category_file/delete/{id}', 'App\Http\Controllers\CategoryFileController@delete');

// });


