    #######################################################################
#                    PM Tomps Service FILE Application             #
#######################################################################

#------------- Setup Environment -------------------------------------------------------------

# Pull base image
FROM ubuntu:18.04

# Install common tools 
RUN apt-get update
RUN apt-get install -y \
    wget \
    curl \
    nano \
    htop \
    git \
    unzip \
    bzip2 \
    software-properties-common \
    locales

# Set evn var to enable xterm terminal
ENV TERM=xterm

# Set timezone to UTC to avoid tzdata interactive mode during build
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Set working directory
WORKDIR /var/www/html

# Set up locales 
# RUN locale-gen

#------------- Application Specific Stuff ----------------------------------------------------

# Install PHP
RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
RUN apt update && apt clean
RUN apt-get install -y \
    php7.4-fpm \
    php7.4-common \
    php7.4-curl \
    php7.4-mbstring \
    php7.4-json \
    php7.4-xml \
    php7.4-bcmath \
    php7.4-dom \
    php7.4-gd \
    php7.4-zip \
    php7.4-pgsql

#------------- Composer & laravel configuration ----------------------------------------------------
# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . .

#------------- Build apps  ----------------------------------------------------

# composer install
RUN php /usr/local/bin/composer update -W \
    && php /usr/local/bin/composer install \
    && /usr/local/bin/composer require illuminate/mail \
    && chmod 777 /var/www/html/run.sh && chmod 777 /var/www/html/bootstrap/cache

ADD run.sh .

CMD ["/var/www/html/run.sh"]