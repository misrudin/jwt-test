#!/bin/bash

# ----- Declare Vars
ART="php artisan"
DIR_PHP="/var/www/html"

echo '------------------------------------------------------------------------------------------'
echo "Run db migration - PM service backend sysreport."

# create folder running shell script
touch migrate.sh
touch nohup-queue.sh

echo """
#!/bin/bash
php artisan migrate --force;
php artisan db:seed;
""" >> migrate.sh

chmod 777 migrate.sh;
./migrate.sh;
rm -rf migrate.sh

echo "Nohup configuration automation."
# Nohup.conf set

echo """
#!/bin/bash
nohup php artisan queue:work --daemon &
nohup php artisan queue:work --daemon > /dev/null 2>&1 &
nohup php artisan queue:work --daemon > storage/logs/queue.logs &
""" >> nohup-queue.sh

chmod 777 nohup-queue.sh;
./nohup-queue.sh;

{
echo -e "\n"
echo "======================================================================================================"
echo -e "\n"
echo -e "\tSuccessfully migrate data to database PM.\n";
echo -e """
        status => 200,
        message => pm-tomps-svc-sysreport
        """
echo -e "\tIn Application Ready!!!";
echo -e "\n"
echo -e "======================================================================================================="
echo -e "\n"
echo -e "\n"
echo -e "Logs - Application >> PM TOMPS SVC sysreport."
} | fmt

php -S 0.0.0.0:9000 -t public;