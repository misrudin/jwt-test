<table>
    <thead>
        <table>
            <tr>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">No</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Nama Proyek</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Nama Mitra</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Tipe Pembayaran</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Total Nilai</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Status</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Jenis TOP</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF"></th>
            </tr>
        </table>
    </thead>
    <tbody> 
        @foreach ($data as $item)
            <table>
                <tr>
                    <td style="font-size:13px;border:2px solid black;">{{ $loop->iteration }}</td>
                    <td style="font-size:13px;border:2px solid black;">{{ $item['proyek_name'] }}</td>
                    <td style="font-size:13px;border:2px solid black;">{{ $item['partner_name'] }}</td>
                    <td style="font-size:13px;border:2px solid black;">{{ $item['payment_type'] }} {{ count($item['detail_payment']) }}</td>
                    <td style="font-size:13px;border:2px solid black;">Rp. {{ number_format($item['total_value'],0,'.','.') }}</td>
                    <td style="font-size:13px;border:2px solid black;">Paid {{ $item['paid_payment'] }}, Unpaid {{ $item['unpaid_payment'] }}</td>
                    <td style="font-size:13px;border:2px solid black;">{{ $item['type_partner'] }}</td>
                </tr>
                <tr style="font-weight: bold;border:2px solid black;">
                    <th style="border:2px solid black;"></th>
                    <th style="border:2px solid black;"></th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Tipe Pembayaran</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Total Nilai</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Status</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Tanggal Bayar</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Tenggat Waktu</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Sisa Hari</th>
                </tr>
                @foreach ($item['detail_payment'] as $bayar)
                    <tr style="border:2px solid black;">
                        <td style="border:2px solid black;"></td>
                        <td style="border:2px solid black;"></td>
                        <td style="border:2px solid black;">{{ $bayar->payment_type }} {{ $loop->iteration }}</td>
                        <td style="border:2px solid black;">Rp. {{ number_format($bayar->value,0,'.','.') }}</td>
                        <td style="border:2px solid black;">{{ $bayar->top_status }}</td>
                        <td style="border:2px solid black;">{{ date('d F Y', strtotime($bayar->paid_at)) }}</td>
                        <td style="border:2px solid black;">{{ date('d F Y', strtotime($bayar->due_date)) }}</td>
                        <td style="border:2px solid black;">{{ \Carbon\Carbon::parse($bayar->paid_at)->diffInDays(\Carbon\Carbon::parse($bayar->due_date)) }} Hari</td>
                    </tr>
                @endforeach
            </table>
        @endforeach  
       
    </tbody>
</table>