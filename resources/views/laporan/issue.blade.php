<table>
    <thead>
        <table>
            <tr>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">No</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Kategori Isu</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Jumlah Isu</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Nama Proyek</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Keterangan Isu</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Tindak Lanjut</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Dampak</th>
                <th style="font-weight:500; border:2px solid black; text-align:center; vertical-align:center; height:60px; font-size:16px; padding-top:10px;background-color:#FFA3FF">Tanggal Update</th>
            </tr>
        </table>
    </thead>
    <tbody> 
        @foreach ($data as $item)
            <table>
                <tr>
                    <td style="font-size:13px;border:2px solid black;">{{ $loop->iteration }}</td>
                    <td style="font-size:13px;border:2px solid black;">{{ $item['IssueCategory'] }}</td>
                    <td style="font-size:13px;border:2px solid black;">{{ $item['totalProject'] }}</td>
                </tr>
                <tr style="font-weight: bold;border:2px solid black;">
                    <th style="border:2px solid black;"></th>
                    <th style="border:2px solid black;"></th>
                    <th style="border:2px solid black;"></th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Proyek</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Keterangan Issue</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Tindak Lanjut</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Dampak</th>
                    <th style="font-weight:bold; font-size:12px;border:2px solid black;">Tanggal Update</th>
                </tr>
                @foreach ($item['dataProject'] as $issue)
                    <tr style="border:2px solid black;">
                        <td style="border:2px solid black;"></td>
                        <td style="border:2px solid black;"></td>
                        <td style="border:2px solid black;"></td>
                        <td style="border:2px solid black;">{{ $issue->name_proyek }} {{ $loop->iteration }}</td>
                        <td style="border:2px solid black;">{{ $issue->issue_name }}</td>
                        <td style="border:2px solid black;">{{ $issue->response_plans }}</td>
                        <td style="border:2px solid black;">{{ $issue->impacts }}</td>
                        <td style="border:2px solid black;">{{ date('d F Y', strtotime($issue->updated_at)) }}</td>
                    </tr>
                @endforeach
            </table>
        @endforeach  
       
    </tbody>
</table>