<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_lokasi_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwLokasiProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

            /**
      * @OA\GET(
     *     path="/api/v1/lokasiProyek/getAllLokasiProyek",
     *     tags={"Term Of Payment"},
     *     operationId="getAllLokasiProyek",
        *      @OA\Parameter(
        *          name="tipe_partner",
        *          description="Tipe Partner (PARTNER, CUSTOMER )",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getAllLokasiProyek(Request $request){
        $table = new V_dash_lokasi_proyek();
        $cities = DB::table('cities')->select('name')->get();
        // return $cities;
        if (!empty($cities)){
            
            $dataFinal = array();
            $features = array();

        // $dataArray['data'] = array();
            foreach ($cities as $p) {
            $data = array();

                if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                    if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_lokasi_proyek WHERE 
                        (city = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                        (city = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (city = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (city = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                         "));
                       
                    }
                }else{
                    $response =  DB::select(DB::raw("SELECT * FROM vw_dash_lokasi_proyek WHERE city = '$p->name'"));
                }
                $totalNilai=0;
                $issue=0;
                $provinceName='';
                $geojson ='';
                foreach ($response as $q) {
                    $totalNilai = $totalNilai+(int)$q->nilai_proyek;
                    $issue = $issue+(int)$q->issue;
                    $provinceName = $q->provinsi;
                    $geojson =  $q->geojson;
                }
                $geojson = json_decode($geojson);
                $geojsonFormat =  array();
                $coordinates =  array();
                if(!empty($geojson)){
                    $coordinates = $geojson->coordinates;
                    $geojsonFormat = ["type"=> $geojson->type, "coordinates"=> $coordinates];
                    $features[] = [ 'type' => "Feature", 'properties' => ['province' =>$provinceName, 'city' => $p->name, 'totalProyek' =>count($response), 'totalNilai' => $totalNilai, 'isu' => $issue],'geometry' =>$geojsonFormat];
                }else{
                    $features[] = [ 'type' => "Feature", 'properties' => ['province' =>$provinceName, 'city' => $p->name, 'totalProyek' =>count($response), 'totalNilai' => $totalNilai, 'isu' => $issue],'geometry' =>(object)[]];
                }
                
                 }


            // $features = collect($features);
            // $sorting = $features->sortBy('totalNilai')->reverse();
            // $sorted = $sorting->values()->toArray();

            $dataFinal[] = ["type"=> "FeatureCollection", "features"=> $features];
       
        
            $result = array('status'=>true,'kode'=>1,'data'=>$dataFinal,'pesan'=>'Data Ditemukan');
                 
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;





    //     if($request->has('filterDateFrom') && $request->has('filterDateTo')){
    //         if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
    //             $oneTimecharge =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','One Time Charge (OTC)')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //             $terminBased =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Termin Based')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //             $perProgress =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Progress Based')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //             $locationBased =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Location Based')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //             $monthlyRecurring =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Monthly Recurring')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //         }
    //     }else{
    //     $oneTimecharge =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','One Time Charge (OTC)')->get();
    //     $terminBased =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Termin Based')->get();
    //     $perProgress =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Progress Based')->get();
    //     $locationBased =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Location Based')->get();
    //     $monthlyRecurring =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Monthly Recurring')->get();
    //     }
    //     // return $data = $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Location Based')->get();

    //     if (!empty($oneTimecharge) && !empty($terminBased) && !empty($perProgress) && !empty($locationBased) && !empty($monthlyRecurring)){
    //         $totalOneTimecharge=0;
    //         $totalTerminBased=0;
    //         $totalPerProgress=0;
    //         $totalOneTimecharge=0;
    //         $totalLocationBased=0;
    //         $totalMonthlyRecurring=0;
    //         foreach ($oneTimecharge as $p) {
    //            $totalOneTimecharge = $totalOneTimecharge+(int)$p->total;
    //             }
    //        foreach ($terminBased as $p) {
    //            $totalTerminBased = $totalTerminBased+(int)$p->total;
    //             }
    //         foreach ($perProgress as $p) {
    //             $totalPerProgress = $totalPerProgress+(int)$p->total;
    //             }
    //         foreach ($locationBased as $p) {
    //             $totalLocationBased = $totalLocationBased+(int)$p->total;
    //             }
    //         foreach ($monthlyRecurring as $p) {
    //             $totalMonthlyRecurring = $totalMonthlyRecurring+(int)$p->total;
    //             }

    //         $data = [
    //             ['Jenis Pembayaran' => 'One Time Charge (OTC)','Total Proyek' => count($oneTimecharge), 'Total Nilai' => $totalOneTimecharge],
    //             ['Jenis Pembayaran' => 'Termin Based', 'Total Proyek' => count($terminBased), 'Total Nilai' => $totalTerminBased],
    //             ['Jenis Pembayaran' => 'Progress Based', 'Total Proyek' => count($perProgress), 'Total Nilai' => $totalPerProgress],
    //             ['Jenis Pembayaran' => 'Location Based', 'Total Proyek' => count($locationBased), 'Total Nilai' => $totalLocationBased],
    //             ['Jenis Pembayaran' => 'Monthly Recurring', 'Total Proyek' => count($monthlyRecurring), 'Total Nilai' => $totalMonthlyRecurring],
    //             // array('Jenis Pembayaran'=>'One Time Charge (OTC)','Total Proyek'=>count($oneTimecharge),'Total Nilai'=>$totalOneTimecharge),
    //             // array('Jenis Pembayaran'=>'Termin Based','Total Proyek'=>count($terminBased),'Total Nilai'=>$totalTerminBased),
    //             // array('Jenis Pembayaran'=>'Progress Based','Total Proyek'=>count($perProgress),'Total Nilai'=>$totalPerProgress),
    //             // array('Jenis Pembayaran'=>'Location Based','Total Proyek'=>count($locationBased),'Total Nilai'=>$totalLocationBased),
    //             // array('Jenis Pembayaran'=>'Monthly Recurring','Total Proyek'=>count($monthlyRecurring),'Total Nilai'=>$totalMonthlyRecurring)
    //         ];
            
    //         $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
    //     }else{
    //         $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
    //     }
    //     return $result;
    }

}