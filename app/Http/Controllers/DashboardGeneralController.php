<?php

namespace App\Http\Controllers;

use App\Models\DashboardGeneral;
use Illuminate\Http\Request;
use DB;

class DashboardGeneralController extends Controller
{
    // status proyek general
    public function __construct() {
        //
    }
     /**
     * @OA\GET(
     *     path="/api/v1/dashboard/getStatusProyekGeneral/",
     *     tags={"Status Proyek"},
     *     operationId="getStatusProyekGeneral",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

    public function getStatusProyekGeneral(Request $request){  
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){

                $closed =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_general WHERE 
                (status = 'Closed' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (status = 'Closed' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Closed' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Closed' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));

                $aktif =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_general WHERE 
                (status = 'Aktif' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (status = 'Aktif' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Aktif' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Aktif' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                "));

                $kandidat =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_general WHERE 
                (status = 'Kandidat' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (status = 'Kandidat' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Kandidat' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Kandidat' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                "));
            }
        }else{
            $closed =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_general WHERE status = 'Closed'"));
            $aktif =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_general WHERE status = 'Aktif'"));
            $kandidat =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_general WHERE status = 'Kandidat'"));
        }

       
            $data = [
                count($kandidat),
                count($aktif),
                count($closed),
            ];
            $result = [
                'status'=>true,
                'kode'=>1,
                'totalProyek'=>count($kandidat)+  count($aktif)+  count($closed),
                'data'=>$data,
                'pesan'=>'Data Ditemukan'
            ];
        return $result;
    }


    /**
     * @OA\GET(
     *     path="/api/v1/dashboard/getDetailStatusProyekGeneral/",
     *     tags={"Status Proyek"},
     *     operationId="getDetailStatusProyekGeneral",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getDetailStatusProyekGeneral(Request $request){  
        $table = DashboardGeneral::Select('vw_dash_status_proyek_general.*');
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $data =  $table
                ->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo]);
            }
            return  $data->paginate(10)->appends(['filterDateFrom'=>$request->filterDateFrom, 'filterDateTo' => $request->filterDateTo]);
        }else{
            $data = $table;
            return $data->paginate(10);
        }
    }

    // skala proyek general
    public function getCountAllSkalaProyekGeneral(Request $request){  
        $kategori = DB::table('charter_value_categories')->where('deleted_at', null)->get();
        if (!empty($kategori)){
            $data = array();
            foreach ($kategori as $p) {
                if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                    if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_skala_general WHERE 
                        (skala = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                        (skala = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (skala = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (skala = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                         "));    
                    }
                }else{
                    $response =  DB::select(DB::raw("SELECT * FROM vw_dash_skala_general WHERE skala = '$p->name' ")); 
                }
                // return $response;
                $totalNilai=0;
                if(!empty($response)){
                    foreach ($response as $q) {
                        $totalNilai = $totalNilai+(int)$q->nilai_proyek;
                    }          
    
                    $data[] = ['value' =>  count($response),'type' => $p->name];
                     }
                    $data = collect($data);
                    $sorted = $data->sortBy('value')->reverse();

                    $result = array(
                        'status'=>true,
                        'kode'=>1,
                        'data'=>$sorted->values()->toArray(),
                        'pesan'=>'Data Ditemukan');
                }
                 
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

}
