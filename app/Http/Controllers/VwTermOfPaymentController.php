<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_term_of_payment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwTermOfPaymentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

            /**
      * @OA\GET(
     *     path="/api/v1/termOfPayment/getAllTermOfPaymentProyek",
     *     tags={"Term Of Payment"},
     *     operationId="getAllTermOfPaymentProyek",
        *      @OA\Parameter(
        *          name="tipe_partner",
        *          description="Tipe Partner (PARTNER, CUSTOMER )",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getAllTermOfPaymentProyek(Request $request){
        if (empty($request->has('tipe_partner'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter tipe_partner tidak boleh kosong');
            return $result;
        }
        if (empty($request->has('status'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter status tidak boleh kosong');
            return $result;
        }
        $tipe_partner=strtoupper($request->tipe_partner);
        $status=strtoupper($request->status);
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                

                $getData =  DB::select(DB::raw("SELECT * FROM vw_dash_term_of_payment WHERE 
                (tipe_partner = '$tipe_partner' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));
               
            }
        }else{
        $getData =  DB::select(DB::raw("SELECT * FROM vw_dash_term_of_payment WHERE tipe_partner = '$tipe_partner'"));

        }
        if (!empty($getData) ){
            $totalProyek=0;
            $totalPembayaran=0;
            $sudahDibayarkan=0;
            $belumDibayarkan=0;
            $lastValue='';
            $response = array();
            foreach ($getData as $p) {
                // return (date('Y-m-d', strtotime($p->due_date. ' - 30 days')) <= date('Y-m-d', strtotime($request->filterDateFrom)));
                if($status == 'BELUMBAYAR' ){
                    if($p->nama_pertner != $lastValue){
                 
                        $totalProyek=$totalProyek+ $p->total_proyek;
                        $totalPembayaran= $totalPembayaran+ $p->value_total;
                        $belumDibayarkan= $belumDibayarkan+ $p->value_total;
                        $lastValue=$p->nama_pertner;
                    }
                    if($p->top_status == 'PAID'){
                        $sudahDibayarkan= $sudahDibayarkan+ $p->value;
                        $belumDibayarkan= $belumDibayarkan- $p->value;
                    }           
        
                }else if ($status == 'MENDEKATIDEADLINE'){
                    //duedate under 30 days
                    $filter= new DateTime();
                    if (!empty($request->has('tipe_partner'))){
                       $filter=$request->filterDateFrom;
                    }
                    return $filter;
                    if(date('Y-m-d', strtotime($p->due_date. ' - 30 days')) <= date('Y-m-d', strtotime($filter))){
                       
                        if($p->nama_pertner != $lastValue){
                            $totalProyek=$totalProyek+ $p->total_proyek;
                            $totalPembayaran= $totalPembayaran+ $p->value_total;
                            $belumDibayarkan= $belumDibayarkan+ $p->value_total;
                            $lastValue=$p->nama_pertner;
                        }
                        if($p->top_status == 'PAID'){
                            $sudahDibayarkan= $sudahDibayarkan+ $p->value;
                            $belumDibayarkan= $belumDibayarkan- $p->value;
                        }        
                    }
                }              
                
                 }
                 $response[] = ['value' =>  $totalProyek,'type' =>  'totalProyek'];
                 $response[] = ['value' =>  $totalPembayaran,'type' =>  'totalPembayaran'];
                 $response[] = ['value' =>  $sudahDibayarkan,'type' =>  'sudahDibayarkan'];
                 $response[] = ['value' =>  $belumDibayarkan,'type' =>  'belumDibayarkan'];













            $result = array('status'=>true,'kode'=>1,'data'=>$response,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;



        // if (!empty($oneTimecharge) && !empty($terminBased) && !empty($perProgress) && !empty($locationBased) && !empty($monthlyRecurring)){
        //     $totalOneTimecharge=0;
        //     $totalTerminBased=0;
        //     $totalPerProgress=0;
        //     $totalOneTimecharge=0;
        //     $totalLocationBased=0;
        //     $totalMonthlyRecurring=0;
        //     foreach ($oneTimecharge as $p) {
        //        $totalOneTimecharge = $totalOneTimecharge+(int)$p->total;
        //         }
        //    foreach ($terminBased as $p) {
        //        $totalTerminBased = $totalTerminBased+(int)$p->total;
        //         }
        //     foreach ($perProgress as $p) {
        //         $totalPerProgress = $totalPerProgress+(int)$p->total;
        //         }
        //     foreach ($locationBased as $p) {
        //         $totalLocationBased = $totalLocationBased+(int)$p->total;
        //         }
        //     foreach ($monthlyRecurring as $p) {
        //         $totalMonthlyRecurring = $totalMonthlyRecurring+(int)$p->total;
        //         }

        //     $data = [
        //         ['paymentType' => 'One Time Charge (OTC)','totalProject' => count($oneTimecharge), 'totalValue' => $totalOneTimecharge],
        //         ['paymentType' => 'Termin Based', 'totalProject' => count($terminBased), 'totalValue' => $totalTerminBased],
        //         ['paymentType' => 'Progress Based', 'totalProject' => count($perProgress), 'totalValue' => $totalPerProgress],
        //         ['paymentType' => 'Location Based', 'totalProject' => count($locationBased), 'totalValue' => $totalLocationBased],
        //         ['paymentType' => 'Monthly Recurring', 'totalProject' => count($monthlyRecurring), 'totalValue' => $totalMonthlyRecurring],
        //         // array('paymentType'=>'One Time Charge (OTC)','totalProject'=>count($oneTimecharge),'totalValue'=>$totalOneTimecharge),
        //         // array('paymentType'=>'Termin Based','totalProject'=>count($terminBased),'totalValue'=>$totalTerminBased),
        //         // array('paymentType'=>'Progress Based','totalProject'=>count($perProgress),'totalValue'=>$totalPerProgress),
        //         // array('paymentType'=>'Location Based','totalProject'=>count($locationBased),'totalValue'=>$totalLocationBased),
        //         // array('paymentType'=>'Monthly Recurring','totalProject'=>count($monthlyRecurring),'totalValue'=>$totalMonthlyRecurring)
        //     ];
            
        //     $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        // }else{
        //     $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        // }
        return $result;
    }

}