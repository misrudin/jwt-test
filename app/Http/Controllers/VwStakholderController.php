<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwStakholderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    //         /**
    //  * @OA\GET(
    //  *     path="/api/v1/stackholder/listPartnerProyek",
    //  *     tags={"Stack Holder"},
    //  *     operationId="listPartnerProyek",
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */
    public function listPartnerProyek(){
        $data = DB::table('laporan_vw_stakeholder_partner');
        $data = $data->paginate();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }
//     /**
//      * @OA\GET(
//      *     path="/api/v1/stackholder/searchpartnerproyek",
//      *     tags={"Stack Holder"},
//      *     operationId="searchpartnerproyek",
//    *     @OA\Parameter(
//      *         name="nama_pertner",
//      *         description="nama pertner",
//      *          in="query",
//      *         required=true,
//      *         @OA\Schema(
//      *             type="string"
//      *         ) ),
//      *     @OA\Response(
//      *         response="default",
//      *         description="successful operation"
//      *     )
//      * )
//      */
    public function searchPartnerProyek(Request $request){
        $data = DB::table('laporan_vw_stakeholder_partner');
        if($request->has('filterNamaPartner')){
            if($request->filterNamaPartner != ''){
                $data->where("laporan_vw_stakeholder_partner.nama_pertner","like","%{$request->filterNamaPartner}%");
            }
        }
        $data=$data->get();
        if(!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }

        return $result;
    }


    //  /**
    //  * @OA\GET(
    //  *     path="/api/v1/stackholder/listPartnerProyekDocument/{ProjectId}",
    //  *     tags={"Stack Holder"},
    //  *     operationId="listPartnerProyekDocument",
    //     *      @OA\Parameter(
    //     *          name="ProjectId",
    //     *          description="Project id",
    //     *          required=true,
    //     *          in="path",
    //     *          @OA\Schema(
    //     *              type="string"
    //     *          )
    //     *      ),
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */
    public function listPartnerProyekDocument($ProjectId){
        $data = DB::table('project_files')->where('ProjectID',$ProjectId)->get();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }


    //      /**
    //  * @OA\GET(
    //  *     path="/api/v1/stackholder/listProjectManager",
    //  *     tags={"Stack Holder"},
    //  *     operationId="listProjectManager",
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */
    public function listProjectManager(){
        $data = DB::table('vw_laporan_project_manager');
        $data = $data->paginate();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    
    //  /**
    //  * @OA\GET(
    //  *     path="/api/v1/stackholder/searchProjectManager",
    //  *     tags={"Stack Holder"},
    //  *     operationId="searchProjectManager",
    //     *      @OA\Parameter(
    //     *          name="name",
    //     *          description="name",
    //     *          required=true,
    //     *          in="query",
    //     *          @OA\Schema(
    //     *              type="string"
    //     *          )
    //     *      ),
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */
    public function searchProjectManager(Request $request){
        $data = DB::table('vw_laporan_project_manager');
        if($request->has('filterName')){
            if($request->filterName != ''){
                $data->where("vw_laporan_project_manager.name","like","%{$request->filterName}%");
            }
        }
        $data=$data->get();
        if(!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }

        return $result;
    }

    //  /**
    //  * @OA\GET(
    //  *     path="/api/v1/stackholder/ProjectManagerDetail/{code}",
    //  *     tags={"Stack Holder"},
    //  *     operationId="ProjectManagerDetail",
    //     *      @OA\Parameter(
    //     *          name="code",
    //     *          description="code",
    //     *          required=true,
    //     *          in="path",
    //     *          @OA\Schema(
    //     *              type="string"
    //     *          )
    //     *      ),
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */
    public function ProjectManagerDetail($code){
        $data = DB::table('vw_stakeholder_project_manager_detail')->where('code',$code)->get();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $data;
    }

}