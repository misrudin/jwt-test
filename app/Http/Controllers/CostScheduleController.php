<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CostScheduleController extends Controller
{
   
    public function costConclusion(Request $request)
    {
            $charter = DB::table('charters')->join('users','charters.created_by','=','users.id')
            ->select('charters.id','users.tenant_id')->where('users.tenant_id','!=','sda')->whereNull('charters.deleted_at')->get();
            $charterType = array();
            foreach ($charter as $s) {
                $charterType[] = "charter_id='".$s->id."'";
            };
            $charterType = implode(" OR ", $charterType);
            $plan_value = DB::table('vw_dash_plan_value_detail')->whereRaw($charterType)
            ->select('charter_id', 'plan_value_rupiah', DB::raw('sum(plan_value_rupiah) AS total_rupiah'))
            ->groupBy('charter_id', 'plan_value_rupiah')
            ->get();

            $earn_value = DB::table('vw_dash_earn_value_detail')->whereRaw($charterType)
            ->select('charter_id', 'earn_value_rupiah', DB::raw('sum(earn_value_rupiah) AS total_rupiah'))
            ->groupBy('charter_id', 'earn_value_rupiah')
            ->get();
            $totalPv = [];
            foreach ($plan_value as $pv) {
                $totalPv[] = ['plan'=>$pv->total_rupiah];
            }
            foreach ($earn_value as $pv) {
                $totalEv[] = ['earn'=>$pv->earn_value_rupiah];
            };

            $arr = [];
            foreach ($totalPv as $key => $value) {
                array_push($arr, array_merge($value, $totalEv[$key]));
            }
            $hasil =[];
            foreach ($arr as $v) {
                $hasil[] = $v['plan'] != 0 ? $v['earn']/$v['plan'] : 0;
            }
            
            $withinBudget = 0;
            $exceedBudget = 0;
            foreach ($hasil as $item) {
                if($item >= 1 ) {
                    $withinBudget += 1;
                } else {
                    $exceedBudget += 1;
                }   
            }
            $response = [
                [
                    'label' => 'Within Budget',
                    'value' => $withinBudget
                ],
                [
                    'label' => 'Exceed Budget',
                    'value' => $exceedBudget
                ],
            ];
            
        return ['status' => true, 'kode' => 1, 'data' => $response, 'pesan' => 'Data Ditemukan'];
    }

    public function scheduleConclusion(Request $request)
    {
        $charter = DB::table('charters')->join('users','charters.created_by','=','users.id')
        ->select('charters.id','users.tenant_id')->where('users.tenant_id','!=','sda')->whereNull('charters.deleted_at')->get();
            $charterType = array();
            foreach ($charter as $s) {
                $charterType[] = "charter_id='".$s->id."'";
            };
            $charterType = implode(" OR ", $charterType);
            $plan_value = DB::table('vw_dash_plan_value_detail')->whereRaw($charterType)
            ->select('charter_id', 'plan_value_rupiah', DB::raw('sum(plan_value_rupiah) AS total_rupiah'))
            ->groupBy('charter_id', 'plan_value_rupiah')
            ->get();

            $earn_value = DB::table('vw_dash_earn_value_detail')->whereRaw($charterType)
            ->select('charter_id', 'actual_cost', DB::raw('sum(actual_cost) AS total_rupiah'))
            ->groupBy('charter_id', 'actual_cost')
            ->get();
            $totalPv = [];
            foreach ($plan_value as $pv) {
                $totalPv[] = ['plan'=>$pv->total_rupiah];
            }
            foreach ($earn_value as $pv) {
                $totalEv[] = ['earn'=>$pv->actual_cost];
            };
            $arr = [];
            foreach ($totalPv as $key => $value) {
                array_push($arr, array_merge($value, $totalEv[$key]));
            }
            $hasil =[];
            foreach ($arr as $v) {
                $hasil[] = $v['plan'] != 0 ? $v['earn']/$v['plan'] : 0;
            }
            
            $aheadSchedule = 0;
            $onShedule = 0;
            $behindShedule = 0;
            foreach ($hasil as $item) {
                if($item > 1 ) {
                    $aheadSchedule += 1;
                } elseif($item == 1) {
                    $onShedule += 1;
                }  else {
                    $behindShedule += 1;
                }
            }
            $response = [
                [
                    'label' => 'Ahead Schedule',
                    'value' => $aheadSchedule
                ],
                [
                    'label' => 'On Shedule',
                    'value' => $onShedule
                ],
                [
                    'label' => 'Behind Schedule',
                    'value' => $behindShedule
                ],
            ];
        return ['status' => true, 'kode' => 1, 'data' => $response, 'pesan' => 'Data Ditemukan'];
    }
}

