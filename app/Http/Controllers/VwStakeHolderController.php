<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_charter_all;
use App\Models\V_dash_stake_holder;
use App\Models\V_dash_term_of_payment;
use App\Models\V_dash_stakeholder_partner_customer;

use App\Models\V_dash_partner_payment_charters;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwStakeHolderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

            /**
      * @OA\GET(
     *     path="/api/v1/stakeHolder/getAllStakeHolder",
     *     tags={"Stake Holder"},
     *     operationId="getAllStakeHolderProyek",
             *      @OA\Parameter(
        *          name="type",
        *          description="type (partner ,customer, project-manager )",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getAllStakeHolder(Request $request){
        if (empty($request->has('type'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter type tidak boleh kosong');
            return $result;
        }
        $type=strtolower($request->type);
        $table = new V_dash_stake_holder();

        if ($type == 'project-manager'){
            //project manager
            $stakeholder_table = DB::table('stackholders')
            ->join('stackholder_types', 'stackholders.stackholder_type_code', '=', 'stackholder_types.code')
            ->select('stackholders.name as  stakeholder_name','stackholders.code as  stakeholder_code', 'stackholder_types.name as stackholder_type')
            ->where('stackholder_types.name','Project Manager')
            ->get();
            if (!empty($stakeholder_table)){
            $data = array();
            foreach ($stakeholder_table as $p) {

                if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                    if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stake_holder WHERE 
                        (stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                        (stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                        "));
                    }
                }else{
                    $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stake_holder WHERE  stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code'"));
                }

            $nilai=0;
            $proyekActive=0;
                foreach ( $response as $q) {
                    $nilai = $nilai+(int)$q->nilai_proyek;
                    $proyekActive = $proyekActive+(int)$q->total_proyek;
                }
                $data[] = ['dataname' => $p->stakeholder_name,'proyek' => $proyekActive, 'nilai' => $nilai];
                }
                $data = collect($data);
                $sorted = $data->sortBy('nilai')->reverse();
            $result = array('status'=>true,'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
                
            }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
            }
            return $result;
        }elseif ($type == 'partner') {
            $viewDashStakeholderPartnerCustomer = new V_dash_stakeholder_partner_customer();
                    if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                        if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                            $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stakeholder_partner_customer WHERE 
                            (type = 'PARTNER' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                            (type = 'PARTNER' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                            (type = 'PARTNER' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                            (type = 'PARTNER' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                             "));
                            // $response = $viewDashStakeholderPartnerCustomer
                            // ->where('type','PARTNER')
                            // ->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])
                            // ->get();
                        }
                    }else{
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stakeholder_partner_customer WHERE type = 'PARTNER'"));
                        // $response =  $viewDashStakeholderPartnerCustomer
                        // ->where('type','PARTNER')
                        // ->get();
                    }
                $data = collect($response);
                $sorted = $data->sortBy('realisasi')->reverse();

            $result = array('status'=>true,'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
            return $result;

        }elseif ($type == 'customer') {
           $viewDashStakeholderPartnerCustomer = new V_dash_stakeholder_partner_customer();
                    if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                        if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                            $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stakeholder_partner_customer WHERE 
                            (type = 'CUSTOMER' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                            (type = 'CUSTOMER' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                            (type = 'CUSTOMER' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                            (type = 'CUSTOMER' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                             "));

                            // $response = $viewDashStakeholderPartnerCustomer
                            // ->where('type','CUSTOMER')
                            // ->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])
                            // ->get();
                        }
                    }else{
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stakeholder_partner_customer WHERE type = 'CUSTOMER'"));
                        // $response =  $viewDashStakeholderPartnerCustomer
                        // ->where('type','CUSTOMER')
                        // ->get();
                    }
                $data = collect($response);
                $sorted = $data->sortBy('realisasi')->reverse();

            $result = array('status'=>true,'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
            return $result;
        }else{
           return $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        
        


    
        // $response =  $table->where('stackholder_types','Project Manager')->get();
        // return $result = array('status'=>true,'kode'=>1,'data'=>$response,'pesan'=>'Data Ditemukan');
        
        // $segments = DB::table('charter_segments')->get();
        // if (!empty($segments)){
        //     $data = array();
        //     foreach ($segments as $p) {
        //         if($request->has('filterDateFrom') && $request->has('filterDateTo')){
        //             if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
        //                 $response =  $table
        //                 ->where('segmen',$p->name)
        //                 ->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])
        //                 ->get();
        //             }
        //         }else{
        //             $response =  $table
        //             ->where('segmen',$p->name)
        //             ->get();
        //         }

        //         $totalNilai=0;
        //         foreach ($response as $q) {
        //             $totalNilai = $totalNilai+(int)$q->nilai_proyek;
        //         }
        //         $data[] = ['Segmen' => $p->name,'Total_Proyek' => count($response), 'Total_Nilai' => $totalNilai];
        //          }
        //     $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
                 
        // }else{
        //     $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        // }
        // return $result;
    }

}