<?php

namespace App\Http\Controllers;

use App\Models\CoordinateProvince;
use Illuminate\Http\Request;

class CoordinateProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CoordinateProvince  $coordinateProvince
     * @return \Illuminate\Http\Response
     */
    public function show(CoordinateProvince $coordinateProvince)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CoordinateProvince  $coordinateProvince
     * @return \Illuminate\Http\Response
     */
    public function edit(CoordinateProvince $coordinateProvince)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CoordinateProvince  $coordinateProvince
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CoordinateProvince $coordinateProvince)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CoordinateProvince  $coordinateProvince
     * @return \Illuminate\Http\Response
     */
    public function destroy(CoordinateProvince $coordinateProvince)
    {
        //
    }
}
