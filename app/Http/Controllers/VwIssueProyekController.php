<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_issue_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwIssueProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * @OA\GET(
     *     path="/api/v1/lokasiProyek/getAllIssueProyek",
     *     tags={"Term Of Payment"},
     *     operationId="getAllIssueProyek",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getAllIssueProyek(Request $request){
        $table = new V_dash_issue_proyek();
        // $province =$table
        // ->where('issue_status','Occurs')
        // ->orWhere('issue_status','Closed')
        // ->get();
        $kategori = DB::table('risk_categories')->get();
        // return  $kategori;
        if (!empty($kategori)){
            $data = array();
        // $dataArray['data'] = array();
            foreach ($kategori as $p) {
                if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                    if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                        $daystosum = '1';
                        $datesum =  date('d-m-Y', strtotime($request->filterDateTo.' + '.$daystosum.' days'));
                        $response =  $table
                        ->where('issue_category',$p->name)
                        ->whereBetween('created_at', [$request->filterDateFrom, $datesum])
                        ->get();
                    }
                }else{
                    $response =  $table
                    ->where('issue_category',$p->name)
                    ->get();
                }
                $data[] = ['isuKategori' => $p->name,'totalProyek' => count($response)];
                 }
            $data = collect($data);
            $sorted = $data->sortBy('totalProyek')->reverse();
            $maxIssue =$sorted->values()->toArray();
            $result = array('status'=>true,'maxIssue'=>$maxIssue[0]['totalProyek'],'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
                 
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;





    //     if($request->has('filterDateFrom') && $request->has('filterDateTo')){
    //         if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
    //             $oneTimecharge =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','One Time Charge (OTC)')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //             $terminBased =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Termin Based')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //             $perProgress =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Progress Based')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //             $locationBased =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Location Based')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //             $monthlyRecurring =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Monthly Recurring')->whereBetween('date_payment', [$request->filterDateFrom, $request->filterDateTo])->get();
    //         }
    //     }else{
    //     $oneTimecharge =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','One Time Charge (OTC)')->get();
    //     $terminBased =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Termin Based')->get();
    //     $perProgress =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Progress Based')->get();
    //     $locationBased =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Location Based')->get();
    //     $monthlyRecurring =  $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Monthly Recurring')->get();
    //     }
    //     // return $data = $table->where('tipe_partner',$tipe_partner)->where('jenis_top','Location Based')->get();

    //     if (!empty($oneTimecharge) && !empty($terminBased) && !empty($perProgress) && !empty($locationBased) && !empty($monthlyRecurring)){
    //         $totalOneTimecharge=0;
    //         $totalTerminBased=0;
    //         $totalPerProgress=0;
    //         $totalOneTimecharge=0;
    //         $totalLocationBased=0;
    //         $totalMonthlyRecurring=0;
    //         foreach ($oneTimecharge as $p) {
    //            $totalOneTimecharge = $totalOneTimecharge+(int)$p->total;
    //             }
    //        foreach ($terminBased as $p) {
    //            $totalTerminBased = $totalTerminBased+(int)$p->total;
    //             }
    //         foreach ($perProgress as $p) {
    //             $totalPerProgress = $totalPerProgress+(int)$p->total;
    //             }
    //         foreach ($locationBased as $p) {
    //             $totalLocationBased = $totalLocationBased+(int)$p->total;
    //             }
    //         foreach ($monthlyRecurring as $p) {
    //             $totalMonthlyRecurring = $totalMonthlyRecurring+(int)$p->total;
    //             }

    //         $data = [
    //             ['Jenis Pembayaran' => 'One Time Charge (OTC)','Total Proyek' => count($oneTimecharge), 'Total Nilai' => $totalOneTimecharge],
    //             ['Jenis Pembayaran' => 'Termin Based', 'Total Proyek' => count($terminBased), 'Total Nilai' => $totalTerminBased],
    //             ['Jenis Pembayaran' => 'Progress Based', 'Total Proyek' => count($perProgress), 'Total Nilai' => $totalPerProgress],
    //             ['Jenis Pembayaran' => 'Location Based', 'Total Proyek' => count($locationBased), 'Total Nilai' => $totalLocationBased],
    //             ['Jenis Pembayaran' => 'Monthly Recurring', 'Total Proyek' => count($monthlyRecurring), 'Total Nilai' => $totalMonthlyRecurring],
    //             // array('Jenis Pembayaran'=>'One Time Charge (OTC)','Total Proyek'=>count($oneTimecharge),'Total Nilai'=>$totalOneTimecharge),
    //             // array('Jenis Pembayaran'=>'Termin Based','Total Proyek'=>count($terminBased),'Total Nilai'=>$totalTerminBased),
    //             // array('Jenis Pembayaran'=>'Progress Based','Total Proyek'=>count($perProgress),'Total Nilai'=>$totalPerProgress),
    //             // array('Jenis Pembayaran'=>'Location Based','Total Proyek'=>count($locationBased),'Total Nilai'=>$totalLocationBased),
    //             // array('Jenis Pembayaran'=>'Monthly Recurring','Total Proyek'=>count($monthlyRecurring),'Total Nilai'=>$totalMonthlyRecurring)
    //         ];
            
    //         $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
    //     }else{
    //         $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
    //     }
    //     return $result;
    }



}