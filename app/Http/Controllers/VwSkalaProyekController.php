<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_skala_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwSkalaProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

            /**
     * @OA\GET(
     *     path="/api/v1/skalaProyek/getCountAllSkalaProyek",
     *     tags={"Skala Proyek"},
     *     operationId="getCountAllSkalaProyek",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getCountAllSkalaProyek(Request $request){  

        $table = new V_dash_skala_proyek();
        $kategori = DB::table('charter_value_categories')->where('deleted_at',null)->get();
        if (!empty($kategori)){
            $data = array();
            foreach ($kategori as $p) {
                if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                    if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_skala_proyek WHERE 
                        (skala = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                        (skala = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (skala = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (skala = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                         "));    
                    }
                }else{
                    $response =  DB::select(DB::raw("SELECT * FROM vw_dash_skala_proyek WHERE skala = '$p->name' ")); 

                }
                // return $response;
                $totalNilai=0;
                if(!empty($response)){
                    foreach ($response as $q) {
                        $totalNilai = $totalNilai+(int)$q->nilai_proyek;
                    }          
    
                    $data[] = ['value' =>  count($response),'type' => $p->name];
                     }
                $data = collect($data);
                $sorted = $data->sortBy('value')->reverse();

                $result = array('status'=>true,'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
                }
                
                 
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;

        // if($request->has('filterDateFrom') && $request->has('filterDateTo')){
        //     if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                

        //         $MEGA =  $table->where('skala','MEGA')->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])->get();
        //         $BIG =  $table->where('skala','BIG PROYEK')->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])->get();
        //         $REGULAR =  $table->where('skala','REGULAR')->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])->get();
        //         $ORDINARY =  $table->where('skala','ORDINARY')->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])->get();
                
        //         // $result = array('status'=>true,'kode'=>1,'data'=>$MEGA,'pesan'=>'Data Ditemukan');
        //     }
        // }else{
        // $MEGA =  $table->where('skala','MEGA')->get();
        // $BIG =  $table->where('skala','BIG PROYEK')->get();
        // $REGULAR =  $table->where('skala','REGULAR')->get();
        // $ORDINARY =  $table->where('skala','ORDINARY')->get();
        // }

        // if (!empty($MEGA) && !empty($BIG)&& !empty($REGULAR)&& !empty($ORDINARY)){
        //     // $data = [
        //     //     "mega" => count($MEGA),
        //     //     "big" => count($BIG),
        //     //     "regular" => count($REGULAR),
        //     //     "ordinary" => count($ORDINARY)
        //     // ];
        //     $data = array();
        //     $data[] = ['value' => count($MEGA),'type' =>  'mega'];
        //     $data[] = ['value' => count($BIG),'type' =>  'big'];
        //     $data[] = ['value' => count($REGULAR),'type' =>  'regular'];
        //     $data[] = ['value' => count($ORDINARY),'type' =>  'ordinary'];
        //     $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        // }else{
        //     $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        // }
        // return $result;
    }


                /**
     * @OA\GET(
     *     path="/api/v1/skalaProyek/getAllSkalaProyek",
     *     tags={"Skala Proyek"},
     *     operationId="getAllSkalaProyek",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getAllSkalaProyek(Request $request){       
        $table = new V_dash_skala_proyek();
        $table = V_dash_skala_proyek::Select('vw_dash_skala_proyek.*');
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $data =  $table
                ->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo]);
            }
            return  $data->paginate(10)->appends(['filterDateFrom'=>$request->filterDateFrom, 'filterDateTo' => $request->filterDateTo]);
        }else{
        $data = $table;
        return  $data->paginate(10);
        }
    }




                    /**
     * @OA\GET(
     *     path="/api/v1/skalaProyek/getAllSkalaProyekBySkalaName/{skala}",
     *     tags={"Skala Proyek"},
     *     operationId="getAllSkalaProyekBySkalaName",
        *      @OA\Parameter(
        *          name="skala",
        *          description="skala name (MEGA,BIG, REGULAR, ORDINARY, )",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getAllSkalaProyekBySkalaName(Request $request){       
        if (empty($request->has('skala'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter skala tidak boleh kosong');
            return $result;
        }
        $skala=strtoupper($request->skala);
        // return $skala;
        $table = new V_dash_skala_proyek();
          $table = V_dash_skala_proyek::Select('vw_dash_skala_proyek.*')->where('skala',$skala);
          if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $data =  $table
                ->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo]);
            }
            return  $data->paginate(10)->appends(['skala'=>$skala,'filterDateFrom'=>$request->filterDateFrom, 'filterDateTo' => $request->filterDateTo]);
        }else{
        $data = $table;
        return  $data->paginate(10);
        }
    }

    

}