<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class VwLokasiGeneraalController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                    $data = DB::select("select * from fn_dash_lokasi_general('$request->filterDateFrom', '$request->filterDateTo') l  order by l.totalNilai desc");
                }
            } else {
                $data = DB::select("select * from fn_dash_lokasi_general() l  order by l.totalNilai desc");
            }
            // $arr = [];
            // foreach ($data as $value) {
            //     $arr[] = [
            //         'lat' => $value->lat,
            //         'lng' => $value->lng,
            //         'province' => $value->province,
            //         'province_id' => $value->province_id,
            //         'totalproyek' => $value->totalproyek,
            //         'totalnilai' => $value->totalnilai, 
            //         'isu' => $value->isu, 
            //     ];
            // }
            return array('status' => true, 'kode' => 1, 'data' => $data, 'pesan' => 'Data Ditemukan');
        } catch(\Exception $exception){
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }
}
