<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanRegionalProject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class LaporanRegionalController extends Controller
{
   
    public function getAllRegionalDownload(Request $request)
    {
        $data = DB::table('vw_laporan_regional')->get();
        $arr =[];
        foreach ($data as $key => $value) {
            $arr[$key] = [
                'id_lop' => empty(json_decode($value->id_lop)[0]->lop) ? '-' : implode(json_decode($value->id_lop)[0]->lop),
                'proyek_code' => $value->proyek_code,
                'proyek_name' => $value->proyek_name,
                'regional' => $value->name_regional,
                'pic' => $value->pic,
                'progress' => $value->progress,
                'status_proyek' => $value->status_proyek,
            ];
        };
        return Excel::download(new LaporanRegionalProject($arr), 'laporan_regional.xlsx');
    }
}
