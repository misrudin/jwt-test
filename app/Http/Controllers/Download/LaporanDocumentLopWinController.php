<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanDocumentLopWin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class LaporanDocumentLopWinController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllDocumentLopWin(Request $request)
    {
        $data = DB::table('vw_laporan_document_lop_win')->get();
        $arr = [];
        foreach($data as $key => $dt){
            
            $arr[$key] = [
                'project_code' => $dt->project_code,
                'project_name' => $dt->project_name,
                'segment_name' => $dt->segment_name,
                'am' => $dt->am,
                'no_quote' => empty(json_decode($dt->no_quote)[0]->quote) ? '-' : implode(json_decode($dt->no_quote)[0]->quote),
                'no_so' => $dt->no_so,
                'spk' => $dt->spk
            ];
        }

        return Excel::download(new LaporanDocumentLopWin($arr), 'laporan_lop_win.xlsx');
    }
}
