<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanIssueProject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;
class LaporanIssueProjectController extends Controller
{
    public function getAllIssue(Request $request)
    {
        $data = DB::select("select * from fn_dash_issue_project(null, null, '$request->search') l where lower(l.issuecategory) like '%$request->search%' or totalproject like '%$request->search%'");
        $response = [];
        foreach ($data as $d) {
            $response[] = [
                'IssueCategory' => $d->issuecategory,
                'totalProject' => $d->totalproject,
                'dataProject' => json_decode($d->dataproject)
            ];
        }
        return Excel::download(new LaporanIssueProject($response), 'laporan_issue_proyek.xlsx');
    }
}
