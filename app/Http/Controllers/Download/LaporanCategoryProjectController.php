<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanCategoryProject;
use App\Exports\LaporanSegmentProject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class LaporanCategoryProjectController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportCategory(Request $request)
    {
        $param = ucfirst(strtoupper($request->param));
        if ($param == "CATEGORY") {
            $category = DB::table('vw_dash_laporan_category_proyek')->get();
            return Excel::download(new LaporanCategoryProject($category), 'laporan_category.xlsx');
        }
        if ($param == "SEGMENT") {
            $category = DB::table('vw_dash_laporan_segment_proyek')->get();
            return Excel::download(new LaporanSegmentProject($category), 'laporan_segment_proyek.xlsx');
        }

    }
}
