<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanTopProject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use DB;
class LaporanTopProjectController extends Controller
{
    
    public function getAllTop(Request $request)
    {
       
        $param = ucfirst(strtoupper($request->param));
        if ($param == 'MITRA' || $param == 'KLIEN') {
            $parameter = '';
            if ($param == 'MITRA') {
                $parameter = 'PARTNER';
            }
            if ($param == 'KLIEN') {
                $parameter = 'CUSTOMER';
            }
            if ($request->param != '') {
                $data = DB::table('vw_dash_laporan_list_top_charter_proyek')
                    ->where('type_partner', $parameter)->get();
            }
        } 

        if (!empty($data)) {

            $dataFormatting = array();
            foreach ($data as $q) {

                if ($request->has('paymentType')) {
                    $arrayPaymentType = $request->paymentType;
                    $paymentTypePagination = array();
                    $paymentType = array();


                    foreach ($arrayPaymentType as $r) {
                        $formatArrayPaymentType = ucwords(strtolower($r));
                        //handle All
                        if ($formatArrayPaymentType == 'All') {
                            $paymentType = array();
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='One Time Charge (OTC)'"; // AND paymentType= Over Budget, On Budget, All
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='Location Based'";
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='Termin Based'";
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='Progress Based'";
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='Monthly Recurring'";
                            $paymentTypePagination = array();
                            $paymentTypePagination[] = 'One Time Charge (OTC)';
                            $paymentTypePagination[] = 'Location Based';
                            $paymentTypePagination[] = 'Termin Based';
                            $paymentTypePagination[] = 'Progress Based';
                            $paymentTypePagination[] = 'Monthly Recurring';
                            break;
                        }
                        //add modelCost
                        $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='" . $formatArrayPaymentType . "'";
                    }
                    //generate query string
                    $paymentType = implode(" OR ", $paymentType);
                }
                // return $paymentType;
                $dataTmp =  DB::table('vw_dash_laporan_term_of_payment_proyek')
                // ->where('id_partner',$q->id_partner)
                ->whereRaw($paymentType)
                ->get();
                
                if (!empty($dataTmp)) {
                    $paid = 0;
                    $unpaid = 0;
                    foreach ($dataTmp as $payment) {
                        if($payment->top_status === 'UNPAID') {
                            $unpaid += 1;
                        } else {
                            $paid += 1;
                        }
                    };

                    if (!count($dataTmp) == 0) {
                        $dataFormatting[] = [
                            'proyek_name' => $dataTmp[0]->proyek_name,
                            'proyek_code' => $dataTmp[0]->proyek_code,
                            'id_partner' => $q->id_partner,
                            'partner_name' => $dataTmp[0]->name_partner,
                            'payment_type' => $dataTmp[0]->payment_type,
                            'total_value' => $dataTmp[0]->total_value,
                            'paid_payment' => $paid,
                            'unpaid_payment' => $unpaid,
                            'type_partner' => $dataTmp[0]->type_partner,
                            'detail_payment' => $dataTmp
                        ];
                    }
                    
                }
            }
            return Excel::download(new LaporanTopProject($dataFormatting), 'laporan_top_proyek.xlsx');
            
        } 
    }
}
