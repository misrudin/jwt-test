<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanLocationProject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use DB;

class LaporanLocationProjectController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllLocation(Request $request)
    {
        $data = DB::select("select * from fn_dash_lokasi_project() l WHERE lower(province) LIKE '%$request->search%' OR l.totalproyek::text like '%$request->search%' order by l.totalNilai desc");
        return Excel::download(new LaporanLocationProject($data), 'laporan_lokasi_proyek.xlsx');
    }
}
