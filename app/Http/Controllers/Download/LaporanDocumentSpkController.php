<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanDocumentSpk;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Facades\Excel;


class LaporanDocumentSpkController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllDocumentSpk(Request $request)
    {
        $spk = DB::table('vw_laporan_document_spk_mitra')
        ->select('kode_partner', 'nama_partner','jenis_top','value_total','date','status', DB::raw('count(*) AS total_document'))
        ->groupBy('kode_partner', 'nama_partner','jenis_top','value_total','date','status')
        ->get();
        return Excel::download(new LaporanDocumentSpk($spk), 'laporan_spk.xlsx');
    }
}
