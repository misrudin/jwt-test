<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanDocumentTopKb;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


use DB;

class LaporanDocumentTopKbController extends Controller
{
    
    public function getAllTopKb(Request $request)
    {
        $topKb = DB::table("vw_laporan_document_top_kb")->get();
        $arr = [];

            foreach($topKb as $key => $dt){

                $arr[$key] = [
                    'project_code' => $dt->project_code,
                    'project_name' => $dt->project_name,
                    'segment_name' => $dt->segment_name,
                    'pm_name' => $dt->pm_name,
                    'project_value' => round($dt->project_value),
                    'payment_name' => $dt->payment_name,
                    'status' => $dt->status,
                    'expected_date' => $dt->expected_date,
                    'id_lop' => implode(json_decode($dt->id_lop)[0]->lop)
                ];
            }
        return Excel::download(new LaporanDocumentTopKb($arr), 'laporan_top_kb.xlsx');
    }
}
