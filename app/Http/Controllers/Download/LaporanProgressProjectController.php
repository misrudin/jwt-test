<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanProgressProject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use DB;

class LaporanProgressProjectController extends Controller
{
    
    public function getAllProgress(Request $request)
    {
        $statusProyek = ucfirst(strtolower($request->statusProyek));
        $statusProgressProyek = ucfirst(strtolower($request->statusProgressProyek));

        $data = DB::table('vw_dash_laporan_progress_achievment_proyek')
            ->where(DB::raw("charter_running_status = '$statusProyek' AND $statusProgressProyek='true'"))
            ->select(['id','proyek_name','charter_running_status','project_manager','deliverable','issue_open','issue_total','progress','value','updated_at'])
            ->get();
        return Excel::download(new LaporanProgressProject($data), 'laporan_progress_proyek.xlsx');
    }
}
