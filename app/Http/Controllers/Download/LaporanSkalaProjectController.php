<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanSkalaProject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use DB;

class LaporanSkalaProjectController extends Controller
{
    
    public function getAllSkalaProyek(Request $request)
    {
        $skala = ucfirst(strtoupper($request->skala));
        $data = DB::table('vw_dash_laporan_skala_proyek')
        ->where('skala', $skala)->select(['code_proyek','proyek_name','nilai_proyek','skala','project_manager','account_manager','status_proyek','progress','updated_at'])->get();
        return Excel::download(new LaporanSkalaProject($data),'laporan_skala_proyek.xlsx');
    }
}
