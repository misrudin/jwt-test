<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanDocumentTopKl;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use DB;

class LaporanDocumentTopKlController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllDocumentKl(Request $request)
    {
        $data = DB::table('vw_laporan_document_top_kl');
        $arr = [];
            foreach($data->get() as $key => $dt){

                $arr[$key] = [
                    'project_code' => $dt->project_code,
                    'project_name' => $dt->project_name,
                    'segment_name' => $dt->segment_name,
                    'payment_name' => $dt->payment_name,
                    'status' => $dt->status,
                    'no_pb' => $dt->no_pb,
                    'expected_date' => $dt->expected_date,
                    'id_lop' => implode(json_decode($dt->idlop)[0]->lop)
                ];
            }
        return Excel::download(new LaporanDocumentTopKl($arr), 'laporan_kl.xlsx');
    }
}
