<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanAnggaranProject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class LaporanAnggaranProjectController extends Controller
{
    
    public function getLaporanAnggaran(Request $request)
    {
        $running_status = ucfirst(strtolower($request->runningStatus));
        $data = DB::table('vw_dash_laporan_anggaran_proyek')
            ->whereRaw("charter_running_status='$running_status'");

        if (!empty($data)) {
            $result =  $data->select(['proyek_name','model_cost','value','budget_allocated','budget_spent','budget_gap','absorption_percentage','absorption_realization','absorption_difference','budget_status'])->get();
        } else {
            $result = 'Data Tidak Ditemukan';
        }
        return Excel::download(new LaporanAnggaranProject($result), 'laporan_anggaran.xlsx');
    }
}
