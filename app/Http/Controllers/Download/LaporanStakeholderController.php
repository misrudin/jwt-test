<?php

namespace App\Http\Controllers\Download;

use App\Exports\LaporanStakeholderCustomer;
use App\Exports\LaporanStakeholderPartner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use DB;

class LaporanStakeholderController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllStakeholder(Request $request)
    {
        $param = ucfirst(strtoupper($request->param));
        if ($param == 'MITRA' || $param == 'KLIEN') {
            $parameter = '';
            if ($param == 'MITRA') {
                $parameter = 'PARTNER';
            }
            if ($param == 'KLIEN') {
                $parameter = 'CUSTOMER';
            }
            if ($request->param != '') {
                $data = DB::table('vw_dash_laporan_partner_customer_proyek')->whereRaw(
                    "type='$parameter'"
                )->select(['stakeholder_name','type','total_payment','realisasi','remaining_payment','total_proyek','document'])->get();
            }
        } 
        if (!empty($data)) {
            $result = $data;
        } else {
            $result = 'Data Kosong';
        }
        if ($param == 'MITRA') {
            return Excel::download(new LaporanStakeholderPartner($result), 'laporan_partner.xlsx');
        } else {
            return Excel::download(new LaporanStakeholderCustomer($result), 'laporan_customer.xlsx');
        }
    }
}
