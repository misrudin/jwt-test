<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class VwLessonLearnedController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $data = DB::table('charters')
                ->whereBetween('charters.created_at', [$request->filterDateFrom, $request->filterDateTo]);
            }
        } else {
            $data = DB::table('charters');
        }
        $response = $data
        ->join('users', 'charters.created_by', '=', 'users.id')
        ->select('charters.name AS proyek_name','charters.descriptions','users.tenant_id','charters.lesson_learn','charters.planed_end_at','charters.created_at')
        ->where('users.tenant_id','!=', 'sda')
        ->whereNull('charters.deleted_at')->whereNotNull('charters.lesson_learn')->orderBy('charters.created_at', 'desc')->get();
        if(!empty($response)) {
            $arr = [];
            foreach ($response as $key => $value) {
                $arr[$key] = [
                    'proyek_name' => $value->proyek_name,
                    'descriptions' => $value->lesson_learn,
                    'created_at' => $value->created_at,
                ];
            }
            $result = array('status'=>true,'kode'=>1,'data'=>$arr,'pesan'=>'Data Ditemukan');
        } else {
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }
}
