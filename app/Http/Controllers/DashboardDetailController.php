<?php

namespace App\Http\Controllers;

use App\Models\Charter;
use App\Models\PlanValueDetail;
use App\Services\ScurvesService;
use DateTime;
use Doctrine\DBAL\Driver\Exception;
use Illuminate\Http\Request;
use App\Models\EarnValue;
use App\Models\EarnValueDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;


class DashboardDetailController extends Controller
{
    public function getSummary($projectId)
    {
        try {
            $dataSummary = DB::table('vw_dash_detail_summary')->where('id', $projectId)->get();
            $location = DB::select(DB::raw("select provinsi, city, geojson from vw_dash_detail_location where id='$projectId'"));
            $stakeholder = DB::table('vw_dash_detail_stakeholder')->where('id_proyek', $projectId)->get();
            $cost = DB::table('vw_dash_detail_cost')->where('id', $projectId)->get();

            $stakeholder_mapping = [];
            foreach ($stakeholder as $value) {
                $stakeholder_mapping[] = ['name' => $value->stackholder, 'type' => $value->stackholder_types];
            }

            $data = [
                'summary' => [
                    'progress' => $dataSummary[0]->progress,
                    'issue' => [
                        'count' => $dataSummary[0]->issue,
                        'status' => $dataSummary[0]->status_project
                    ],
                    'executionTime' => [
                        'timeRemaining' => $dataSummary[0]->time_remaining <=0 ? 0 : $dataSummary[0]->time_remaining,
                        'startDate' => $dataSummary[0]->planed_start_at,
                        'endDate' => $dataSummary[0]->planed_end_at
                    ],
                    'category' => [
                        'name' => $dataSummary[0]->category_name,
                        'value' => $dataSummary[0]->category_value
                    ],
                    'location' => [
                        'province' => $location[0]->provinsi,
                        'district' => $location[0]->city,
                        'geojson' => json_decode($location[0]->geojson)
                    ],
                    'stakeholder' => $stakeholder_mapping
                ],
                'cost' => [
                    'totalCost' => $cost[0]->total_anggaran,
                    'totalRealization' => $cost[0]->realisasi_anggaran,
                    'absorption' => ($cost[0]->penyerapan == null) ? "0.00" : $cost[0]->penyerapan,
                    'remainingBudget' => $cost[0]->sisa_anggaran
                ]
            ];

            $response = ['status' => true, 'kode' => 1, 'data' => $data, 'pesan' => 'Data Ditemukan'];
            return $response;
        } catch (\Throwable $th) {
            return $th;
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    public function getSCurves($projectId)
    {
        try {

            $data = DB::table('vw_dash_detail_scurves')->where('id', $projectId)->get();
            $tasks = json_decode($data[0]->tasks)[0];

            $response = [
                'id' => $data[0]->id,
                'name' => $data[0]->name,
                'code' => $data[0]->code,
                'budget_allocated' => $data[0]->budget_allocated,
                'budget_spent' => $data[0]->budget_spent,
                'running_stat' => $data[0]->running_stat,
                'deliv_stat' => $data[0]->deliv_stat,
                'additionals' => json_decode($data[0]->additionals),
                'tasks' => [
                    'data' => [[
                        'charter_id' => $tasks->charter_id,
                        'text' => $tasks->text,
                        'start_date' => date('Y-m-d', strtotime($tasks->start_date)),
                        'duration' => $tasks->duration,
                        'bobot' => $tasks->bobot,
                        'progress' => $tasks->progress,
                    ]],

                ]
            ];
            return ['status' => true, 'kode' => 1, 'data' => $response, 'pesan' => 'Data Ditemukan'];
        } catch (\Throwable $th) {
            return ['status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    public function getChartScurves(Request $request, ScurvesService $sCurvesService){

        $this->sCurvesService = $sCurvesService;

        try {
            $data = $request->only('charter_id', 'scale');
            $validator = Validator::make($data, [
                'charter_id' => 'required|uuid|exists:charters,id',
                'scale' => 'required|in:daily,weekly,monthly,annually'
            ], [
                'charter_id.required' => 'Charter ID tidak boleh kosong',
                'charter_id.uuid' => 'Charter ID tidak valid',
                'charter_id.exists' => 'Charter ID tidak ditemukan',
                'scale.required' => 'Scale tidak boleh kosong',
                'scale.in' => 'Scale tidak valid'
            ]);
            if($validator->fails()){
                return response()->json(['status' => false, 'kode' => 2, 'data' => [], 'pesan' => $validator->messages()], 400);
            }
            $gantt = $this->sCurvesService->sCurves($request);
            return ['status' => true, 'kode' => 1, 'data' => $gantt, 'pesan' => 'Data Ditemukan'];
        } catch (\Throwable $th) {
            return ['status' => false, 'kode' => 2, 'data' => [], 'pesan' => $th->getMessage()];
        }
    }

    public function filterScurvesChart(Request $request, ScurvesService $sCurvesService){

        try {
            $this->sCurvesService = $sCurvesService;
            $data = $request->only('charter_id');
            $validator = Validator::make($data, [
                'charter_id' => 'required|uuid|exists:charters,id',
            ], [
                'charter_id.required' => 'Charter ID tidak boleh kosong',
                'charter_id.uuid' => 'Charter ID tidak valid',
                'charter_id.exists' => 'Charter ID tidak ditemukan'
            ]);
            if($validator->fails()){
                return response()->json(['status' => false, 'kode' => 2, 'data' => [], 'pesan' => $validator->messages()], 400);
            }
            $gantt = $this->sCurvesService->filterScurves($request);
            return ['status' => true, 'kode' => 1, 'data' => $gantt, 'pesan' => 'Data Ditemukan'];
        } catch (\Throwable $th) {
            return ['status' => false, 'kode' => 2, 'data' => [], 'pesan' => $th->getMessage()];
        }
    }

    public function saveEarnValue(Request $request)
    {
        try {


            $req = ['data' => $request->all()];
            $validator = Validator::make($req, [
                'data.*.task_id' => 'required|uuid|exists:tasks,id',
                'data.*.budget_at_completion' => 'numeric',
                'data.*.target_periode_end' => 'numeric',
            ], $this->validation_message_earn_value());
            if ($validator->fails()) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
            }
            $result = [];
            //looping plan value detail jika ada nanti delete dulu baru insert
            for ($i = 0; $i < count($req['data']); $i++) {
                $task = DB::table('tasks')->select('planned_start', 'planned_end', 'bobot')->where('id', $req['data'][$i]['task_id'])->first();
                //                return $task;
                $date1 = new DateTime($task->planned_start);
                $date2 = new DateTime($task->planned_end);
                $duration = 0;
                $diff = 0;
                if ($task->planned_start == $task->planned_end) {
                    $duration = 1;
                } else {
                    $diff = $date2->diff($date1);
                    $duration = $diff->format("%a");
                }
                $target_per_day = $req['data'][$i]['target_periode_end'] / $duration;
                $budget_per_day = $req['data'][$i]['budget_at_completion'] / $duration;
                $id = $this->findEarnValueId($req['data'][$i]['task_id']);
                $data_post = [
                    'id' => !empty($id) ? $id : Str::uuid(),
                    'task_id' => $req['data'][$i]['task_id'],
                    'budget_at_completion' => $req['data'][$i]['budget_at_completion'],
                    'target_periode_end' => $req['data'][$i]['target_periode_end'],
                ];
                EarnValue::upsert(
                    $data_post,
                    ['id'],
                    ['budget_at_completion', 'target_periode_end']
                );


                //get_id
                $earn_value_id = $data_post['id'];

                //get_plan_value by earn_value_id
                $plan_value_data = DB::table('plan_value_detail')->where('earn_value_id', $earn_value_id)->get();

                if (count($plan_value_data) > 0) {
                    //delete plan value by earn_value_id
                    PlanValueDetail::where('earn_value_id', $earn_value_id)->delete();
                }

                $result[] = [
                    'id' => !empty($id) ? $id : Str::uuid(),
                    'task_id' => $req['data'][$i]['task_id'],
                    'budget_at_completion' => $req['data'][$i]['budget_at_completion'],
                    'target_periode_end' => $req['data'][$i]['target_periode_end'],
                    'plan_values' => []
                ];

                for ($j = 0; $j <= $duration; $j++) {
                    $plan_values_data[] = [
                        'earn_value_id' => $earn_value_id,
                        'id' => Str::uuid(),
                        'plan_value_percent' => $target_per_day * $j,
                        'plan_value_bobot' => ($target_per_day * $j) / 100 * $task->bobot,
                        'audit_date' => date('Y-m-d', strtotime($task->planned_start . $j . 'days')),
                    ];
                    $result[$i]['plan_values'][] = [
                        'earn_value_id' => $earn_value_id,
                        'id' => Str::uuid(),
                        'plan_value_percent' => $target_per_day * $j,
                        'plan_value_bobot' => ($target_per_day * $j) / 100 * $task->bobot,
                        'audit_date' => date('Y-m-d', strtotime($task->planned_start . $j . 'days')),
                        'plan_value_rupiah' => $budget_per_day * $j,
                    ];
                }
            }
            PlanValueDetail::insert($plan_values_data);
            return ['status' => true, 'kode' => 1, 'pesan' => 'Berhasil simpan data', 'data' => $result];
        } catch (\Throwable $th) {
            $errorCode = ($th->getCode() > 99 && $th->getCode() < 600) ? $th->getCode() : 500;
            throw new \Exception($th->getMessage(), $errorCode);
        }
    }

    public function updateEarnValue(Request $request)
    {
        try {
            $data = $request->only('budget_at_completion', 'target_periode_end', 'task_id');
            $validator = Validator::make($data, [
                'task_id' => 'required|uuid',
                'budget_at_completion' => 'numeric',
                'target_periode_end' => 'numeric',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
            }
            $task = DB::table('tasks')->select('planned_start', 'planned_end', 'bobot')->where('id', $data['task_id'])->first();
            if (empty($task)) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Task tidak ditemukan'], 404);
            }
            $date1 = new DateTime($task->planned_start);
            $date2 = new DateTime($task->planned_end);
            $duration = 0;
            $diff = 0;
            if ($task->planned_start == $task->planned_end) {
                $duration = 1;
            } else {
                $diff = $date2->diff($date1);
                $duration = $diff->format("%a");
            }
            $target_per_day = $data['target_periode_end'] / $duration;

            $earnValueId = EarnValue::where('task_id', $data['task_id'])->get();

            if (count($earnValueId) == 0) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Data tidak ditemukan'], 404);
            }

            $deletePlanValueDetail = PlanValueDetail::where('earn_value_id', $earnValueId[0]->id)->delete();

            $earn_value = EarnValue::where('task_id', $data['task_id'])->first();
            $earn_value->update([
                'budget_at_completion' => $request->budget_at_completion,
                'task_id' => $request->task_id,
                'target_periode_end' => $request->target_periode_end,

            ]);

            for ($i = 0; $i <= $duration; $i++) {
                $plan_values_data[] = [
                    'id' => Str::uuid(),
                    'earn_value_id' => $earn_value->id,
                    'plan_value_percent' => $target_per_day * $i,
                    'plan_value_bobot' => ($target_per_day * $i) / $data['target_periode_end'] * $task->bobot,
                    'audit_date' => date('Y-m-d', strtotime($task->planned_start . $i . 'days')),
                ];
            }
            $plan_values = PlanValueDetail::insert($plan_values_data);

            $get_plan_value_data = PlanValueDetail::select('audit_date')->pluck('audit_date')->toArray();
            $get_earn_value_detail_data = EarnValueDetail::select('audit_date')->get();
            $data_not_same = [];
            foreach ($get_earn_value_detail_data as $value) {
                if (!in_array($value->audit_date, $get_plan_value_data)) {
                    $data_not_same[] = $value->audit_date;
                }
            }

            EarnValueDetail::where('earn_value_id', $earnValueId[0]->id)->whereIn('audit_date', $data_not_same)->delete();

            return ['status' => true, 'kode' => 1, 'pesan' => 'Berhasil update data', 'data' => [
                'earn_value' => $earn_value,
                'plan_values' => $plan_values_data,
            ]];
        } catch (\Throwable $th) {
            $errorCode = ($th->getCode() > 99 && $th->getCode() < 600) ? $th->getCode() : 500;
            throw new \Exception($th->getMessage(), $errorCode);
        }
    }

    public function deleteEarnValue($task_id)
    {

        try {
            $earnValueId = DB::table('earn_value')->where('task_id', $task_id)->first();
            if ($earnValueId == null) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Data tidak ditemukan'], 404);
            } else {
                $earnValueId = $earnValueId->id;
            }
            PlanValueDetail::where('earn_value_id', $earnValueId)->delete();
            EarnValueDetail::where('earn_value_id', $earnValueId)->delete();
            DB::table('earn_value')->where('task_id', $task_id)->delete();
            return ['status' => true, 'kode' => 1, 'pesan' => 'Berhasil hapus data'];
        } catch (\Throwable $th) {
            $errorCode = ($th->getCode() > 99 && $th->getCode() < 600) ? $th->getCode() : 500;
            throw new \Exception($th->getMessage(), $errorCode);
        }
    }

    public function findEarnValueId($id)
    {
        $earnValueId = DB::table('earn_value')->where('task_id', $id)->first();
        return !empty($earnValueId) ? $earnValueId->id : null;
    }

    public function findEarnValueDetail($date, $earn_value_id)
    {
        $earnValueDetail = DB::table('earn_value_detail')->where('audit_date', $date)->where('earn_value_id', $earn_value_id)->first();
        return !empty($earnValueDetail) ? $earnValueDetail : null;
    }

    public function getEarnValueReal($taskId)
    {
        try {
            $earn_value_id = $this->findEarnValueId($taskId);
            if (!empty($earn_value_id)) {
                $data = DB::table('earn_value_detail')->where('earn_value_id', $earn_value_id)->get();
                if (!empty($data)) {
                    $dataReal = [];
                    foreach ($data as $value) {
                        $dataReal[] = [
                            'id'=> $value->id,
                            'audit_date' => date('Y-m-d', strtotime($value->audit_date)),
                            'earn_value_bobot' => $value->earn_value_bobot,
                            'earn_value_percent' => $value->earn_value_percent,
                            'actual_cost'=> $value->actual_cost,
                            'earn_value_bobot_accumulation'=> $value->earn_value_bobot_accumulation,
                            'earn_value_percent_accumulation'=> $value->earn_value_percent_accumulation,
                            'actual_cost_accumulation'=> $value->actual_cost_accumulation,
                        ];
                    }
                    return response()->json(['status' => false, 'kode' => 1, 'pesan' => "Data ditemukan", 'data'=> $dataReal], 200);
                } else {
                    return response()->json(['status' => false, 'kode' => 2, 'pesan' => "Belum ada data realisasi"], 404);
                }
            } else {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => "Task tidak ditemukan"], 404);
            }
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'kode' => 2, 'pesan' => $th->getMessage()], $th->getCode());
        }
    }

    public function saveEarnValueDetail(Request $request)
    {
        try {
            $data = $request->only('task_id', 'earn_values');
            $validator = Validator::make($data, [
                'task_id' => 'required|uuid|exists:earn_value,task_id',
                'earn_values' => 'required|array',
                'earn_values.*.audit_date' => 'required|date',
                'earn_value.*.earn_value_percent' => 'required|numeric',
                'earn_value.*.earn_value_bobot' => 'numeric'

            ], $this->validation_message_earn_value_detail());

            if ($validator->fails()) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
            }

            $earn_value = DB::table('earn_value')->where('task_id', $data['task_id'])->first();
            $earn_value_id = $earn_value->id;
            $target_period_end = $earn_value->target_periode_end;
            $budget_at_completion = $earn_value->budget_at_completion;
            $bobot_task = DB::table('tasks')->select('bobot')->where('id', $data['task_id'])->get()[0]->bobot;

            $earn_value_detail_data = [];
            $result_real = [];
            $earn_value_bobot_accumulation = 0;
            $earn_value_percent_accumulation = 0;
            $earn_value_rupiah_accumulation = 0;
            for ($i = 0; $i < count($data['earn_values']); $i++) {
                $evd_detail = $this->findEarnValueDetail($data['earn_values'][$i]['audit_date'], $earn_value_id);
                $earn_value_detail_data[] =
                    [
                        'id' => !empty($evd_detail) ? $evd_detail->id : Str::uuid(),
                        'earn_value_id' => $earn_value_id,
                        'audit_date' => $data['earn_values'][$i]['audit_date'],
                        'earn_value_percent' => $data['earn_values'][$i]['earn_value_percent'],
                        'earn_value_bobot' => empty($data['earn_values'][$i]['earn_value_bobot']) ? ($data['earn_values'][$i]['earn_value_percent'] / 100) * $bobot_task : $data['earn_values'][$i]['earn_value_bobot'],
                        'earn_value_percent_accumulation' => $earn_value_percent_accumulation += $data['earn_values'][$i]['earn_value_percent'],
                        'earn_value_bobot_accumulation' => $earn_value_bobot_accumulation += empty($data['earn_values'][$i]['earn_value_bobot']) ? ($data['earn_values'][$i]['earn_value_percent'] / 100) * $bobot_task : $data['earn_values'][$i]['earn_value_bobot'],
                        'actual_cost' => !empty($evd_detail) ? $evd_detail->actual_cost : 0,
                        'actual_cost_accumulation' => !empty($evd_detail) ? $evd_detail->actual_cost_accumulation : 0,
                    ];
                $result_real[] =
                    [
                        'id' => !empty($evd_detail) ? $evd_detail->id : Str::uuid(),
                        'earn_value_id' => $earn_value_id,
                        'audit_date' => $data['earn_values'][$i]['audit_date'],
                        'earn_value_percent' => $data['earn_values'][$i]['earn_value_percent'],
                        'earn_value_bobot' => empty($data['earn_values'][$i]['earn_value_bobot']) ? ($data['earn_values'][$i]['earn_value_percent'] / 100) * $bobot_task : $data['earn_values'][$i]['earn_value_bobot'],
                        'earn_value_percent_accumulation' => $earn_value_percent_accumulation,
                        'earn_value_bobot_accumulation' => $earn_value_bobot_accumulation,
                        'earn_value_rupiah' => $data['earn_values'][$i]['earn_value_percent'] / $target_period_end * $budget_at_completion,
                        'earn_value_rupiah_accumulation' => $earn_value_rupiah_accumulation += $data['earn_values'][$i]['earn_value_percent'] / $target_period_end * $budget_at_completion,
                        'actual_cost' => !empty($evd_detail) ? $evd_detail->actual_cost : 0,
                        'actual_cost_accumulation' => !empty($evd_detail) ? $evd_detail->actual_cost_accumulation : 0,
                    ];
            }

            $check_task_in_earn_value = DB::table('earn_value_detail')->where('earn_value_id', $earn_value_id)->first();
            if (!empty($check_task_in_earn_value)) {
                DB::table('earn_value_detail')->where('earn_value_id', $earn_value_id)->delete();
            }

            EarnValueDetail::upsert(
                $earn_value_detail_data,
                ['id'],
                ['earn_value_percent', 'earn_value_bobot', 'earn_value_id']
            );

            return ['status' => true, 'kode' => 1, 'pesan' => 'Berhasil simpan data', 'data' => $result_real];
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'kode' => 2, 'pesan' => $th->getMessage()], $th->getCode());
        }
    }

    public function saveActualCost(Request $request)
    {
        try {
            $data = $request->only('task_id', 'earn_values');
            $validator = Validator::make($data, [
                'task_id' => 'required|uuid|exists:earn_value,task_id',
                'earn_values' => 'required|array',
                'earn_values.*.actual_cost' => 'required|numeric',
                'earn_values.*.audit_date' => 'required|date',
            ], $this->validation_message_actual_cost());

            if ($validator->fails()) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
            }

            $earn_value_id = DB::table('earn_value')->select('id')->where('task_id', $data['task_id'])->get()[0]->id;

            $earn_value_detail_data = [];
            $tempActualCost = 0;
            for ($i = 0; $i < count($data['earn_values']); $i++) {
                $evd_detail = $this->findEarnValueDetail($data['earn_values'][$i]['audit_date'], $earn_value_id);
                $earn_value_detail_data[] =
                    [
                        'id' => !empty($evd_detail) ? $evd_detail->id : Str::uuid(),
                        'earn_value_id' => $earn_value_id,
                        'audit_date' => $data['earn_values'][$i]['audit_date'],
                        'actual_cost' => $data['earn_values'][$i]['actual_cost'],
                        'actual_cost_accumulation' => $tempActualCost += $data['earn_values'][$i]['actual_cost'],
                        'earn_value_percent' => !empty($evd_detail) ? $evd_detail->earn_value_percent : 0,
                        'earn_value_bobot' => !empty($evd_detail) ? $evd_detail->earn_value_bobot : 0,
                        'earn_value_percent_accumulation' => !empty($evd_detail) ? $evd_detail->earn_value_percent_accumulation : 0,
                        'earn_value_bobot_accumulation' => !empty($evd_detail) ? $evd_detail->earn_value_bobot_accumulation : 0,
                    ];
            }

            $check_task_in_earn_value = DB::table('earn_value_detail')->where('earn_value_id', $earn_value_id)->first();
            if (!empty($check_task_in_earn_value)) {
                DB::table('earn_value_detail')->where('earn_value_id', $earn_value_id)->delete();
            }

            EarnValueDetail::upsert(
                $earn_value_detail_data,
                ['id'],
                ['actual_cost', 'actual_cost_accumulation']
            );

            return ['status' => true, 'kode' => 1, 'pesan' => 'Berhasil simpan data', 'data' => $earn_value_detail_data];
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'kode' => 2, 'pesan' => $th->getMessage()], $th->getCode());
        }
    }

    public function getEarnValueAnalysis($projectId)
    {

        try {
            $charter = DB::table('charters')->select('budget_allocated', 'budget_spent')->where('id', $projectId)->first();
            if (empty($charter)) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Data tidak ditemukan'], 404);
            }
            $plan_value = DB::table('vw_dash_plan_value_detail')->where('charter_id', $projectId)->get();
            $earn_value = DB::table('vw_dash_earn_value_detail')->where('charter_id', $projectId)->get();
            $evm = DB::table('vw_earn_value_detail')->where('charter_id', $projectId)->first();

            //plan value
            $plan_value_rupiah = array_sum($plan_value->pluck('plan_value_rupiah')->toArray());
            $plan_value_bobot = array_sum($plan_value->pluck('plan_value_bobot')->toArray());
            $plan_value_percent = count($plan_value) != 0 ? array_sum($plan_value->pluck('plan_value_percent')->toArray()) / count($plan_value) : 0;

            //earn value
            $earn_value_rupiah = array_sum($earn_value->pluck('earn_value_rupiah')->toArray());
            $earn_value_bobot = array_sum($earn_value->pluck('earn_value_bobot')->toArray());
            $earn_value_percent = count($earn_value) != 0 ? array_sum($earn_value->pluck('earn_value_percent')->toArray()) / count($earn_value) : 0;

            $sv_rupiah = $earn_value_rupiah - $plan_value_rupiah;
            $sv_bobot = $earn_value_bobot - $plan_value_bobot;
            $sv_percent = $earn_value_percent - $plan_value_percent;
            $actual_cost = array_sum($earn_value->pluck('actual_cost')->toArray());

            $cv_value = $earn_value_percent - $actual_cost;
            $cpi = $plan_value_rupiah != 0 ? $earn_value_rupiah / $plan_value_rupiah : 0;
            $spi = $actual_cost != 0 ? $earn_value_rupiah / $actual_cost : 0;

            $eac = $cpi != 0 ? $evm->bac / $cpi : 0;
            $etc = $eac - $actual_cost;
            $earn_value = [
                'earn_value_rupiah' => $earn_value_rupiah,
                'earn_value_bobot' => $earn_value_bobot,
                'earn_value_percent' => $earn_value_percent
            ];

            $plan_value = [
                'plan_value_rupiah' => $plan_value_rupiah,
                'plan_value_bobot' => $plan_value_bobot,
                'plan_value_percent' => $plan_value_percent
            ];

            $sv_value = [
                'sv_value_rupiah' => $sv_rupiah,
                'sv_value_bobot' => $sv_bobot,
                'sv_value_percent' => $sv_percent
            ];

            $response = [
                'project_overview' => [
                    'total_budget' => (float)$charter->budget_allocated,
                    'actual_cost' => (float)$charter->budget_spent,
                    'earned_value' => $earn_value,
                    'planned_value' => $plan_value
                ],
                'cost' => [
                    'cost_performance_index' => $cpi,
                    'cost_variance' => $cv_value,
                    'conclusion' => $cpi >= 1 ? 'Within Budget' : 'Exceed Budget'
                ],
                'schedule' => [
                    'schedule_performance_index' => $spi,
                    'schedule_variance' => $sv_value,
                    'conclusion' => $spi > 1 ? 'Ahead Schedule' : ($spi == 1 ? 'On Shedule' : 'Behind Schedule')
                ],
                'predictions' => [
                    'estimate_at_completion' => $eac,
                    'estimate_to_completion' => $etc
                ]

            ];
            return ['status' => true, 'kode' => 1, 'data' => $response, 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $exception) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    public function customArray($array)
    {
        return $array['planned_start'];
    }

    public function getEarnValueChart(Request $request)
    {
        try {
            if (empty($request->projectId)) return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Project Id tidak boleh kosong'], 403);
            if (!empty($request->filter)) {
                $filter = strtolower($request->filter);
                $projectId = $request->projectId;


                if ($filter == 'day') {
                    $earn_value_day = DB::table('vw_earn_value_chart_day')->where('charter_id', $projectId)->get();
                    $plan_value_day = DB::table('vw_plan_value_chart_day')->where('charter_id', $projectId)->get();

                    $realisasi_percent = [];
                    $plan_percent = [];
                    $realisasi_bobot = [];
                    $plan_bobot = [];
                    $realisasi_rupiah = [];
                    $plan_rupiah = [];
                    $actual_cost = [];
                    $labels = [];

                    foreach ($plan_value_day as $value) {
                        $plan_percent[] = (float)$value->plan_value_percent;
                        $plan_bobot[] = (float)$value->plan_value_bobot;
                        $plan_rupiah[] = (float)$value->plan_value_rupiah;
                        $labels[] = date('d M Y', strtotime($value->audit_date));
                    }
                    foreach ($earn_value_day as $value) {
                        $realisasi_percent[] = (float)$value->earn_value_percent;
                        $realisasi_bobot[] = (float)$value->earn_value_bobot;
                        $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                        $actual_cost[] = (float)$value->actual_cost;
                    }
                    $response = [
                        'realisasi_percent' => $realisasi_percent,
                        'plan_percent' => $plan_percent,
                        'realisasi_bobot' => $realisasi_bobot,
                        'plan_bobot' => $plan_bobot,
                        'realisasi_rupiah' => $realisasi_rupiah,
                        'plan_rupiah' => $plan_rupiah,
                        'actual_cost' => $actual_cost,
                        'labels' => $labels
                    ];
                }
                if ($filter == 'week') {
                    $earn_value_week = DB::table('vw_earn_value_chart_week')->where('charter_id', $projectId)->get();
                    $plan_value_week = DB::table('vw_plan_value_chart_week')->where('charter_id', $projectId)->get();

                    $realisasi_percent = [];
                    $plan_percent = [];
                    $realisasi_bobot = [];
                    $plan_bobot = [];
                    $realisasi_rupiah = [];
                    $plan_rupiah = [];
                    $actual_cost = [];
                    $labels = [];

                    foreach ($plan_value_week as $value) {
                        $plan_percent[] = (float)$value->plan_value_percent;
                        $plan_bobot[] = (float)$value->plan_value_bobot;
                        $plan_rupiah[] = (float)$value->plan_value_rupiah;
                        $labels[] = "Week #" . $value->week_num;
                    }
                    foreach ($earn_value_week as $value) {
                        $realisasi_percent[] = (float)$value->earn_value_percent;
                        $realisasi_bobot[] = (float)$value->earn_value_bobot;
                        $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                        $actual_cost[] = (float)$value->actual_cost;
                    }
                    $response = [
                        'realisasi_percent' => $realisasi_percent,
                        'plan_percent' => $plan_percent,
                        'realisasi_bobot' => $realisasi_bobot,
                        'plan_bobot' => $plan_bobot,
                        'realisasi_rupiah' => $realisasi_rupiah,
                        'plan_rupiah' => $plan_rupiah,
                        'actual_cost' => $actual_cost,
                        'labels' => $labels
                    ];
                }

                if ($filter == 'month') {
                    $earn_value_month = DB::table('vw_earn_value_chart_month')->where('charter_id', $projectId)->get();
                    $plan_value_month = DB::table('vw_plan_value_chart_month')->where('charter_id', $projectId)->get();

                    $realisasi_percent = [];
                    $plan_percent = [];
                    $realisasi_bobot = [];
                    $plan_bobot = [];
                    $realisasi_rupiah = [];
                    $plan_rupiah = [];
                    $actual_cost = [];
                    $labels = [];

                    foreach ($plan_value_month as $value) {
                        $plan_percent[] = (float)$value->plan_value_percent;
                        $plan_bobot[] = (float)$value->plan_value_bobot;
                        $plan_rupiah[] = (float)$value->plan_value_rupiah;
                        $labels[] = date('M Y', strtotime($value->audit_date));
                    }
                    foreach ($earn_value_month as $value) {
                        $realisasi_percent[] = (float)$value->earn_value_percent;
                        $realisasi_bobot[] = (float)$value->earn_value_bobot;
                        $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                        $actual_cost[] = (float)$value->actual_cost;
                    }
                    $response = [
                        'realisasi_percent' => $realisasi_percent,
                        'plan_percent' => $plan_percent,
                        'realisasi_bobot' => $realisasi_bobot,
                        'plan_bobot' => $plan_bobot,
                        'realisasi_rupiah' => $realisasi_rupiah,
                        'plan_rupiah' => $plan_rupiah,
                        'actual_cost' => $actual_cost,
                        'labels' => $labels
                    ];
                }
                if ($filter == 'year') {
                    $earn_value_year = DB::table('vw_earn_value_chart_year')->where('charter_id', $projectId)->get();
                    $plan_value_year = DB::table('vw_plan_value_chart_year')->where('charter_id', $projectId)->get();

                    $realisasi_percent = [];
                    $plan_percent = [];
                    $realisasi_bobot = [];
                    $plan_bobot = [];
                    $realisasi_rupiah = [];
                    $plan_rupiah = [];
                    $actual_cost = [];
                    $labels = [];

                    foreach ($plan_value_year as $value) {
                        $plan_percent[] = (float)$value->plan_value_percent;
                        $plan_bobot[] = (float)$value->plan_value_bobot;
                        $plan_rupiah[] = (float)$value->plan_value_rupiah;
                        $labels[] = date('Y', strtotime($value->audit_date));
                    }
                    foreach ($earn_value_year as $value) {
                        $realisasi_percent[] = (float)$value->earn_value_percent;
                        $realisasi_bobot[] = (float)$value->earn_value_bobot;
                        $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                        $actual_cost[] = (float)$value->actual_cost;
                    }
                    $response = [
                        'realisasi_percent' => $realisasi_percent,
                        'plan_percent' => $plan_percent,
                        'realisasi_bobot' => $realisasi_bobot,
                        'plan_bobot' => $plan_bobot,
                        'realisasi_rupiah' => $realisasi_rupiah,
                        'plan_rupiah' => $plan_rupiah,
                        'actual_cost' => $actual_cost,
                        'labels' => $labels
                    ];
                }
            } else {
                $projectId = $request->projectId;
                $earn_value_month = DB::table('vw_earn_value_chart_month')->where('charter_id', $projectId)->get();
                $plan_value_month = DB::table('vw_plan_value_chart_month')->where('charter_id', $projectId)->get();

                $realisasi_percent = [];
                $plan_percent = [];
                $realisasi_bobot = [];
                $plan_bobot = [];
                $realisasi_rupiah = [];
                $plan_rupiah = [];
                $actual_cost = [];
                $labels = [];

                foreach ($plan_value_month as $value) {
                    $plan_percent[] = (float)$value->plan_value_percent;
                    $plan_bobot[] = (float)$value->plan_value_bobot;
                    $plan_rupiah[] = (float)$value->plan_value_rupiah;
                    $labels[] = date('M Y', strtotime($value->audit_date));
                }
                foreach ($earn_value_month as $value) {
                    $realisasi_percent[] = (float)$value->earn_value_percent;
                    $realisasi_bobot[] = (float)$value->earn_value_bobot;
                    $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                    $actual_cost[] = (float)$value->actual_cost;
                }
                $response = [
                    'realisasi_percent' => $realisasi_percent,
                    'plan_percent' => $plan_percent,
                    'realisasi_bobot' => $realisasi_bobot,
                    'plan_bobot' => $plan_bobot,
                    'realisasi_rupiah' => $realisasi_rupiah,
                    'plan_rupiah' => $plan_rupiah,
                    'actual_cost' => $actual_cost,
                    'labels' => $labels
                ];
            }

            return ['status' => true, 'kode' => 1, 'data' => $response, 'pesan' => 'Data Ditemukan'];
        } catch (\Throwable $th) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    public function getEarnValueChartPost(Request $request)
    {
        try {
            $data = $request->only('projectId', 'filter');
            $validator = Validator::make($data, [
                'projectId' => 'required|uuid'
            ], $this->validation_message_chart());

            if ($validator->fails()) {
                return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
            }

            if (empty($data['projectId'])) return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Project Id tidak boleh kosong'], 403);
            if (!empty($data['filter'])) {
                $filter = strtolower($data['filter']);
                $projectId = $data['projectId'];

                if ($filter == 'day') {
                    $earn_value_day = DB::table('vw_earn_value_chart_day')->where('charter_id', $projectId)->get();
                    $plan_value_day = DB::table('vw_plan_value_chart_day')->where('charter_id', $projectId)->get();

                    $realisasi_percent = [];
                    $plan_percent = [];
                    $realisasi_bobot = [];
                    $plan_bobot = [];
                    $realisasi_rupiah = [];
                    $plan_rupiah = [];
                    $actual_cost = [];
                    $labels = [];

                    foreach ($plan_value_day as $value) {
                        $plan_percent[] = (float)$value->plan_value_percent;
                        $plan_bobot[] = (float)$value->plan_value_bobot;
                        $plan_rupiah[] = (float)$value->plan_value_rupiah;
                        $labels[] = date('d M Y', strtotime($value->audit_date));
                    }
                    foreach ($earn_value_day as $value) {
                        $realisasi_percent[] = (float)$value->earn_value_percent;
                        $realisasi_bobot[] = (float)$value->earn_value_bobot;
                        $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                        $actual_cost[] = (float)$value->actual_cost;
                    }
                    $response = [
                        'realisasi_percent' => $realisasi_percent,
                        'plan_percent' => $plan_percent,
                        'realisasi_bobot' => $realisasi_bobot,
                        'plan_bobot' => $plan_bobot,
                        'realisasi_rupiah' => $realisasi_rupiah,
                        'plan_rupiah' => $plan_rupiah,
                        'actual_cost' => $actual_cost,
                        'label' => $labels
                    ];
                }
                if ($filter == 'week') {
                    $earn_value_week = DB::table('vw_earn_value_chart_week')->where('charter_id', $projectId)->get();
                    $plan_value_week = DB::table('vw_plan_value_chart_week')->where('charter_id', $projectId)->get();

                    $realisasi_percent = [];
                    $plan_percent = [];
                    $realisasi_bobot = [];
                    $plan_bobot = [];
                    $realisasi_rupiah = [];
                    $plan_rupiah = [];
                    $actual_cost = [];
                    $labels = [];

                    foreach ($plan_value_week as $value) {
                        $plan_percent[] = (float)$value->plan_value_percent;
                        $plan_bobot[] = (float)$value->plan_value_bobot;
                        $plan_rupiah[] = (float)$value->plan_value_rupiah;
                        $labels[] = "Week #" . $value->week_num;
                    }
                    foreach ($earn_value_week as $value) {
                        $realisasi_percent[] = (float)$value->earn_value_percent;
                        $realisasi_bobot[] = (float)$value->earn_value_bobot;
                        $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                        $actual_cost[] = (float)$value->actual_cost;
                    }
                    $response = [
                        'realisasi_percent' => $realisasi_percent,
                        'plan_percent' => $plan_percent,
                        'realisasi_bobot' => $realisasi_bobot,
                        'plan_bobot' => $plan_bobot,
                        'realisasi_rupiah' => $realisasi_rupiah,
                        'plan_rupiah' => $plan_rupiah,
                        'actual_cost' => $actual_cost,
                        'label' => $labels
                    ];
                }

                if ($filter == 'month') {
                    $earn_value_month = DB::table('vw_earn_value_chart_month')->where('charter_id', $projectId)->get();
                    $plan_value_month = DB::table('vw_plan_value_chart_month')->where('charter_id', $projectId)->get();

                    $realisasi_percent = [];
                    $plan_percent = [];
                    $realisasi_bobot = [];
                    $plan_bobot = [];
                    $realisasi_rupiah = [];
                    $plan_rupiah = [];
                    $actual_cost = [];
                    $labels = [];

                    foreach ($plan_value_month as $value) {
                        $plan_percent[] = (float)$value->plan_value_percent;
                        $plan_bobot[] = (float)$value->plan_value_bobot;
                        $plan_rupiah[] = (float)$value->plan_value_rupiah;
                        $labels[] = date('M Y', strtotime($value->audit_date));
                    }
                    foreach ($earn_value_month as $value) {
                        $realisasi_percent[] = (float)$value->earn_value_percent;
                        $realisasi_bobot[] = (float)$value->earn_value_bobot;
                        $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                        $actual_cost[] = (float)$value->actual_cost;
                    }
                    $response = [
                        'realisasi_percent' => $realisasi_percent,
                        'plan_percent' => $plan_percent,
                        'realisasi_bobot' => $realisasi_bobot,
                        'plan_bobot' => $plan_bobot,
                        'realisasi_rupiah' => $realisasi_rupiah,
                        'plan_rupiah' => $plan_rupiah,
                        'actual_cost' => $actual_cost,
                        'label' => $labels
                    ];
                }
                if ($filter == 'year') {
                    $earn_value_year = DB::table('vw_earn_value_chart_year')->where('charter_id', $projectId)->get();
                    $plan_value_year = DB::table('vw_plan_value_chart_year')->where('charter_id', $projectId)->get();

                    $realisasi_percent = [];
                    $plan_percent = [];
                    $realisasi_bobot = [];
                    $plan_bobot = [];
                    $realisasi_rupiah = [];
                    $plan_rupiah = [];
                    $actual_cost = [];
                    $labels = [];

                    foreach ($plan_value_year as $value) {
                        $plan_percent[] = (float)$value->plan_value_percent;
                        $plan_bobot[] = (float)$value->plan_value_bobot;
                        $plan_rupiah[] = (float)$value->plan_value_rupiah;
                        $labels[] = date('Y', strtotime($value->audit_date));
                    }
                    foreach ($earn_value_year as $value) {
                        $realisasi_percent[] = (float)$value->earn_value_percent;
                        $realisasi_bobot[] = (float)$value->earn_value_bobot;
                        $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                        $actual_cost[] = (float)$value->actual_cost;
                    }
                    $response = [
                        'realisasi_percent' => $realisasi_percent,
                        'plan_percent' => $plan_percent,
                        'realisasi_bobot' => $realisasi_bobot,
                        'plan_bobot' => $plan_bobot,
                        'realisasi_rupiah' => $realisasi_rupiah,
                        'plan_rupiah' => $plan_rupiah,
                        'actual_cost' => $actual_cost,
                        'label' => $labels
                    ];
                }
            } else {
                $projectId = $data['projectId'];
                $earn_value_month = DB::table('vw_earn_value_chart_month')->where('charter_id', $projectId)->get();
                $plan_value_month = DB::table('vw_plan_value_chart_month')->where('charter_id', $projectId)->get();

                $realisasi_percent = [];
                $plan_percent = [];
                $realisasi_bobot = [];
                $plan_bobot = [];
                $realisasi_rupiah = [];
                $plan_rupiah = [];
                $actual_cost = [];
                $labels = [];

                foreach ($plan_value_month as $value) {
                    $plan_percent[] = (float)$value->plan_value_percent;
                    $plan_bobot[] = (float)$value->plan_value_bobot;
                    $plan_rupiah[] = (float)$value->plan_value_rupiah;
                    $labels[] = date('M Y', strtotime($value->audit_date));
                }
                foreach ($earn_value_month as $value) {
                    $realisasi_percent[] = (float)$value->earn_value_percent;
                    $realisasi_bobot[] = (float)$value->earn_value_bobot;
                    $realisasi_rupiah[] = (float)$value->earn_value_rupiah;
                    $actual_cost[] = (float)$value->actual_cost;
                }
                $response = [
                    'realisasi_percent' => $realisasi_percent,
                    'plan_percent' => $plan_percent,
                    'realisasi_bobot' => $realisasi_bobot,
                    'plan_bobot' => $plan_bobot,
                    'realisasi_rupiah' => $realisasi_rupiah,
                    'plan_rupiah' => $plan_rupiah,
                    'actual_cost' => $actual_cost,
                    'label' => $labels
                ];
            }
            $dataFormatting[] = [
                'tasks' => $response
            ];
            return ['status' => true, 'kode' => 1, 'data' => $dataFormatting, 'pesan' => 'Data Ditemukan'];
        } catch (\Throwable $th) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    private function validation_message_earn_value()
    {
        return [
            'data.*.task_id.required' => 'Data tidak boleh kosong',
            'data.*.task_id.exists' => 'Task ID tidak cocok dengan data yang ada',
            'data.*.task_id.uuid' => 'Data harus bertipe UUID',
            'data.*.budget_at_completion.numeric' => 'Tipe data harus numeric',
            'data.*.target_periode_end.numeric' => 'Tipe data harus numeric',
        ];
    }

    private function validation_message_earn_value_detail()
    {
        return [
            'task_id.required' => 'Data tidak boleh kosong',
            'task_id.uuid' => 'Data harus bertipe UUID',
            'actual_cost.numeric' => 'Tipe data harus numeric',
            'earn_values' => 'Tipe data harus array',
            'earn_values.*.earn_value_percent.numeric' => 'Tipe data harus numeric',
            'earn_values.*.earn_value_percent.required' => 'Data tidak boleh kosong',
            'earn_values.*.earn_value_bobot.numeric' => 'Tipe data harus numeric'
        ];
    }

    private function validation_message_actual_cost()
    {
        return [
            'task_id.required' => 'Data tidak boleh kosong',
            'task_id.uuid' => 'Data harus bertipe UUID',
            'earn_values' => 'Tipe data harus array',
            'earn_values.*.actual_cost.numeric' => 'Tipe data harus numeric',
            'earn_values.*.actual_cost.required' => 'Data tidak boleh kosong',
            'earn_values.*.audit_date.required' => 'Data tidak boleh kosong',
            'earn_values.*.audit_date.date' => 'Data harus bertipe tanggal',
        ];
    }

    private function validation_message_chart()
    {
        return [
            'projectId.required' => 'Data tidak boleh kosong',
            'projectId.uuid' => 'Data harus bertipe UUID'
        ];
    }

    private function _formatDate($date)
    {
        $time = strtotime($date);
        $newDate = date('Y-m-d', $time);
        return $newDate;
    }
}
