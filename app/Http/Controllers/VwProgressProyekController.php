<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_progress_proyek;
use App\Models\V_dash_grafik_progress_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwProgressProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }


     /**
     * @OA\GET(
     *     path="/api/v1/progressProyek/listProgressProyekStatus",
     *     tags={"Progress Proyek"},
     *     operationId="listProgressProyekStatus",
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function listProgressProyekStatus(){
        $data = DB::table('charter_running_statuses')->get();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }


            /**
     * @OA\GET(
     *     path="/api/v1/progressProyek/getCountAllProgressProyek",
     *     tags={"Progress Proyek"},
     *     operationId="getCountAllProgressProyek",
        *      @OA\Parameter(
        *          name="status",
        *          description="status (Draft,Kandidat, Aktif, Tech Closed, Closed, Batal,  )",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getCountAllProgressProyek(Request $request){  
        if (empty($request->has('status'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter status tidak boleh kosong');
            return $result;
        }
        $status=ucfirst(strtolower($request->status));

        $table = new V_dash_progress_proyek();
        // return  array('status'=>true,'kode'=>1,'data'=>$request->status,'pesan'=>'Data Ditemukan');

        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $lead =  DB::select(DB::raw("SELECT * FROM vw_dash_progress_proyek WHERE 
                (lead = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (lead = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lead = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lead = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));                
                
                $lag =  DB::select(DB::raw("SELECT * FROM vw_dash_progress_proyek WHERE 
                (lag = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (lag = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lag = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lag = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));

                $delay =  DB::select(DB::raw("SELECT * FROM vw_dash_progress_proyek WHERE 
                (delay = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (delay = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (delay = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (delay = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                "));
                // return  count($lag);
                
            }
        }else{
            $lead =  DB::select(DB::raw("SELECT * FROM vw_dash_progress_proyek WHERE lead = true AND charter_running_status = '$status'"));
            $lag =  DB::select(DB::raw("SELECT * FROM vw_dash_progress_proyek WHERE lag = true AND charter_running_status = '$status'"));
            $delay =  DB::select(DB::raw("SELECT * FROM vw_dash_progress_proyek WHERE delay = true AND charter_running_status = '$status'"));
        }
 
            $data = [
                // "lead" => count($lead),
                // "lag" => count($lag),
                // "delay" => count($delay),
                count($lead),
                count($lag),
                count($delay),
            ];
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
            return $result;
  
    }
   


    
            /**
     * @OA\GET(
     *     path="/api/v1/progressProyek/getGrafikAllProgressProyek",
     *     tags={"Progress Proyek"},
     *     operationId="getGrafikAllProgressProyek",
        *      @OA\Parameter(
        *          name="status",
        *          description="status (Draft,Kandidat, Aktif, Tech Closed, Closed, Batal,  )",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getGrafikAllProgressProyek(Request $request){  
        // if (empty($request->has('status'))){
        //     $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter status tidak boleh kosong');
        //     return $result;
        // }
        // $status=ucfirst(strtolower($request->status));

        $table = new V_dash_grafik_progress_proyek();
        $result = array('status'=>true,'kode'=>1,'data'=>$table->get(),'pesan'=>'Data Ditemukan');
        // return $result;
        // return  array('status'=>true,'kode'=>1,'data'=>$request->status,'pesan'=>'Data Ditemukan');

        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $lead =  DB::select(DB::raw("SELECT * FROM vw_dash_grafik_progress_proyek WHERE 
                (lead = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (lead = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lead = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lead = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));                
                
                $lag =  DB::select(DB::raw("SELECT * FROM vw_dash_grafik_progress_proyek WHERE 
                (lag = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (lag = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lag = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lag = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));

                $delay =  DB::select(DB::raw("SELECT * FROM vw_dash_grafik_progress_proyek WHERE 
                (delay = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (delay = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (delay = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (delay = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                "));
                
            }
        }else{
            $lead =  DB::select(DB::raw("SELECT * FROM vw_dash_grafik_progress_proyek WHERE lead = true"));
            $lag =  DB::select(DB::raw("SELECT * FROM vw_dash_grafik_progress_proyek WHERE lag = true"));
            $delay =  DB::select(DB::raw("SELECT * FROM vw_dash_grafik_progress_proyek WHERE delay = true"));
        $lead = $table
                ->where('lead',true)
                ->get();
        $lag =  $table
                ->where('lag',true)
                ->get();
        $delay = $table
                ->where('delay',true)
                ->get();
        }

            $data = array();
            $dataLead = array();
            $dataLag = array();
            $dataDelay = array();
            // $maxX =0;
            // $maxY =0;
            foreach ($lead as $q) {
                // if((int)$q->nilai_proyek > $maxX){
                //     $maxX =(int)$q->nilai_proyek;
                // }
                // if((int)$q->progress > $maxY){
                //     $maxY =(int)$q->progress;
                // }
                $dataLead[] = ['x' =>(int)$q->nilai_proyek, 'y' =>(int)$q->progress  , 'company' =>$q->company, 'project' => $q->project,'pm' => $q->stakeholder];
            }
            foreach ($lag as $q) {
                // if((int)$q->nilai_proyek > $maxX){
                //     $maxX =(int)$q->nilai_proyek;
                // }
                // if((int)$q->progress > $maxY){
                //     $maxY =(int)$q->progress;
                // }
                $dataLag[] = ['x' =>(int)$q->nilai_proyek, 'y' =>(int)$q->progress, 'company' =>$q->company, 'project' => $q->project,'pm' => $q->stakeholder];
            }
            foreach ($delay as $q) {
                // if((int)$q->nilai_proyek > $maxX){
                //     $maxX =(int)$q->nilai_proyek;
                // }
                // if((int)$q->progress > $maxY){
                //     $maxY =(int)$q->progress;
                // }
                $dataDelay[] = ['x' =>(int)$q->nilai_proyek,'y' =>(int)$q->progress, 'company' =>$q->company, 'project' => $q->project,'pm' => $q->stakeholder];
            }
            $data[] = ['name' => 'lead',  'data' => $dataLead];
            $data[] = ['name' => 'lag', 'data' => $dataLag];
            $data[] = ['name' => 'delay', 'data' => $dataDelay];
           
             
            // $result = array('status'=>true,'kode'=>1,'maxX'=>$maxX,'maxY'=>$maxY,'data'=>$data,'pesan'=>'Data Ditemukan');
            $result = array('status'=>true,'kode'=>1,'totalData'=>count($dataLead)+count($dataLag)+count($dataDelay), 'data'=>$data, 'pesan'=>'Data Ditemukan');
     
        return $result;
    }
   

}