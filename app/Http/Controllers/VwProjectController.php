<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_laporan_project_segment;
use App\Models\V_laporan_project_report_cost;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwProjectController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function listSegment(){
        $data = V_laporan_project_segment::All();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function searchListSegment(Request $request){
        $data = new V_laporan_project_segment();
        $data = V_laporan_project_segment::Select('laporan_vw_project_segment.*');
        if($request->has('filterSegmentName')){
            if($request->filterSegmentName != ''){
                $data->where("laporan_vw_project_segment.segment_name","like","%{$request->filterSegmentName}%");
            }
        }
        $data=$data->get();
        if(!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }

        return $result;
    }

    public function listRepostCost(){
        $data = V_laporan_project_report_cost::All();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function searchListRepostCost(Request $request){
        $data = new V_laporan_project_report_cost();
        $data = V_laporan_project_report_cost::Select('vw_project_repost_cost.*');
        if($request->has('filterProjectDeskripsi')){
            if($request->filterProjectDeskripsi != ''){
                $data->where("vw_project_repost_cost.project_deskripsi","like","%{$request->filterProjectDeskripsi}%");
            }
        }
        $data=$data->get();
        if(!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }

        return $result;
    }

}