<?php

namespace App\Http\Controllers;

use App\Models\VwIssueProyekGeneral;
use Illuminate\Http\Request;
use DB;

class VwIssueProyekGeneralController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * @OA\GET(
     *     path="/api/v1/lokasiProyek/getAllIssueProyekGeneral",
     *     tags={"Term Of Payment"},
     *     operationId="getAllIssueProyekGeneral",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getAllIssueProyekGeneral(Request $request){
        $table = new VwIssueProyekGeneral();
        $kategori = DB::table('risk_categories')->get();
        // return  $kategori;
        if (!empty($kategori)){
            $data = array();
        // $dataArray['data'] = array();
            foreach ($kategori as $p) {
                if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                    if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                        $daystosum = '1';
                        $datesum =  date('d-m-Y', strtotime($request->filterDateTo.' + '.$daystosum.' days'));
                        $response =  $table
                        ->where('issue_category',$p->name)
                        ->whereBetween('created_at', [$request->filterDateFrom, $datesum])
                        ->get();
                    }
                }else{
                    $response =  $table
                    ->where('issue_category',$p->name)
                    ->get();
                }
                $data[] = ['isuKategori' => $p->name,'totalProyek' => count($response)];
                 }
            $data = collect($data);
            $sorted = $data->sortBy('totalProyek')->reverse();
            $maxIssue =$sorted->values()->toArray();
            $result = array('status'=>true,'maxIssue'=>$maxIssue[0]['totalProyek'],'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
                 
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }
}
