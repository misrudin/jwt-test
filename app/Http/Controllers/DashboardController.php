<?php

namespace App\Http\Controllers;

use App\Models\V_dash_charter_all;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_status_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

                /**
     * @OA\GET(
     *     path="/api/v1/dashboard/getStatusProyek/",
     *     tags={"Status Proyek"},
     *     operationId="getStatusProyek",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */


    public function getStatusProyek(Request $request){  

        $table = new V_dash_status_proyek();

        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){

                $closed =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_all WHERE 
                (status = 'Closed' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (status = 'Closed' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Closed' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Closed' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));

                $aktif =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_all WHERE 
                (status = 'Aktif' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (status = 'Aktif' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Aktif' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Aktif' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                "));

                $kandidat =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_all WHERE 
                (status = 'Kandidat' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (status = 'Kandidat' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Kandidat' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (status = 'Kandidat' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                "));

                // $closed =  $table->where('status','Closed')->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])->get();
                // $aktif =  $table->where('status','Aktif')->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])->get();
                // $kandidat =  $table->where('status','Kandidat')->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo])->get();
            }
        }else{
            $closed =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_all WHERE status = 'Closed'"));
            $aktif =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_all WHERE status = 'Aktif'"));
            $kandidat =  DB::select(DB::raw("SELECT * FROM vw_dash_status_proyek_all WHERE status = 'Kandidat'"));
        }

       
            $data = [
                count($kandidat),
                count($aktif),
                count($closed),
               
               
            ];
            $result = array('status'=>true,'kode'=>1,
            'totalProyek'=>count($kandidat)+  count($aktif)+  count($closed),
            'data'=>$data,
            'pesan'=>'Data Ditemukan');
      
        return $result;
    }



    
                /**
     * @OA\GET(
     *     path="/api/v1/dashboard/getDetailStatusProyek/",
     *     tags={"Status Proyek"},
     *     operationId="getDetailStatusProyek",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    // public function getDetailStatusProyek(Request $request){
    //     $data =  new V_dash_status_proyek();
    //     $data = V_dash_status_proyek::Select('vw_dash_status_proyek_all.*');
    //     if ($request->has('filterDateFrom')){
    //         if($request->filterDateFrom != ''){
    //             $data->where("vw_dash_status_proyek_all.date_proyek",">=",$request->filterDateFrom);
    //         }
    //     }
    //     if ($request->has('filterDateTo')){
    //         if($request->filterDateTo != ''){
    //             $data->where("vw_dash_status_proyek_all.date_proyek","<=",$request->filterDateTo);
    //         }
    //     }
    //     return  $data->paginate(10);
    // }

    

    
    public function getDetailStatusProyek(Request $request){  

        $table = new V_dash_status_proyek();
        $table = V_dash_status_proyek::Select('vw_dash_status_proyek_all.*');
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $data =  $table
                ->whereBetween('planed_start_at', [$request->filterDateFrom, $request->filterDateTo]);
            }
            return  $data->paginate(10)->appends(['filterDateFrom'=>$request->filterDateFrom, 'filterDateTo' => $request->filterDateTo]);
        }else{
        $data = $table;
        return $data->paginate(10);
        }

        // if (!empty($data)){
          
        //     $result = $data->paginate(10);
        // }else{
        //     $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        // }
        // return $result;
    }

    public function test() {
        return auth()->user()->tenant_id;
    }
}