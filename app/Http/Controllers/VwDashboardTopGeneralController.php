<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use DB;

class VwDashboardTopGeneralController extends Controller
{

    public function getAllTopProyekGeneral(Request $request){
        if (empty($request->has('tipe_partner'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter tipe_partner tidak boleh kosong');
            return $result;
        }
        // if (empty($request->has('status'))){
        //     $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter status tidak boleh kosong');
        //     return $result;
        // }
        $tipe_partner=strtoupper($request->tipe_partner);
        $status=strtoupper($request->status);
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                

                $getData =  DB::select(DB::raw("SELECT * FROM vw_dashboard_top_generals WHERE 
                (tipe_partner = '$tipe_partner' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));
               
            }
        }else{
            $getData =  DB::select(DB::raw("SELECT * FROM vw_dashboard_top_generals WHERE tipe_partner = '$tipe_partner'"));

        }
        if (!empty($getData) ){
            $totalProyek=0;
            $totalPembayaran=0;
            $sudahDibayarkan=0;
            $belumDibayarkan=0;
            $lastValue='';
            $response = array();
            foreach ($getData as $p) {
                // return (date('Y-m-d', strtotime($p->due_date. ' - 30 days')) <= date('Y-m-d', strtotime($request->filterDateFrom)));
                if($status == 'BELUMBAYAR' ){
                    if($p->nama_pertner != $lastValue){
                 
                        $totalProyek=$totalProyek+ $p->total_proyek;
                        $totalPembayaran= $totalPembayaran+ $p->value_total;
                        $belumDibayarkan= $belumDibayarkan+ $p->value_total;
                        $lastValue=$p->nama_pertner;
                    }
                    if($p->top_status == 'PAID'){
                        $sudahDibayarkan= $sudahDibayarkan+ $p->value;
                        $belumDibayarkan= $belumDibayarkan- $p->value;
                    }           
        
                }else if ($status == 'MENDEKATIDEADLINE'){
                    //duedate under 30 days
                     // return date('Y-m-d', strtotime('2022-08-20'. ' - 7 days')).'=='. date('Y-m-d', strtotime(now()));
                    if(date('Y-m-d', strtotime($p->due_date. ' - 7 days')) <= date('Y-m-d', strtotime(now()))){
                        if($p->nama_pertner != $lastValue){
                            $totalProyek = $totalProyek+ $p->total_proyek;
                            $totalPembayaran= $totalPembayaran+ $p->value_total;
                            $belumDibayarkan= $belumDibayarkan+ $p->value_total;
                            $lastValue=$p->nama_pertner;
                        }
                        if($p->top_status == 'PAID'){
                            $sudahDibayarkan= $sudahDibayarkan+ $p->value;
                            $belumDibayarkan= $belumDibayarkan - $p->value;
                        }        
                    }
                }              
                
                 }
                 $response[] = ['value' =>  $totalProyek,'type' =>  'totalProyek'];
                 $response[] = ['value' =>  $totalPembayaran,'type' =>  'totalPembayaran'];
                 $response[] = ['value' =>  $sudahDibayarkan,'type' =>  'sudahDibayarkan'];
                 $response[] = ['value' =>  $belumDibayarkan,'type' =>  'belumDibayarkan'];
            $result = array('status'=>true,'kode'=>1,'data'=>$response,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function getDetailPembayaranGeneral(Request $request)
    {
        if (empty($request->has('tipe_partner'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter tipe_partner tidak boleh kosong');
            return $result;
        }
        if (empty($request->has('status'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter status tidak boleh kosong');
            return $result;
        }
        $tipe_partner=strtoupper($request->tipe_partner);
        $status=strtoupper($request->status);
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $getData =  DB::select(DB::raw("SELECT * FROM vw_detail_pembayaran_general WHERE 
                (tipe_partner = '$tipe_partner' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (tipe_partner = '$tipe_partner' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));
               
            }
        }else{
        $getData =  DB::select(DB::raw("SELECT * FROM vw_detail_pembayaran_general WHERE tipe_partner = '$tipe_partner'"));
        }
        if (!empty($getData) ){
            $partnerName= '';
            $typePayment= '';
            $startDate= '';
            $endDate='';
            $value='';
            $totalPayment=0;
            $lastValue='';
            $proyekName = '';
            $proyekCode = '';
            $response = array();
            foreach ($getData as $p) {
                // return (date('Y-m-d', strtotime($p->due_date. ' - 30 days')) <= date('Y-m-d', strtotime($request->filterDateFrom)));
                if($status == 'BELUMBAYAR' ){
                    if($p->nama_pertner != $lastValue){
                        // get name project
                           $proyekName = $p->name_project;
                           $proyekCode = $p->code_project;
                           $partnerName = $p->nama_pertner;
                           $typePayment = $p->jenis_top;
                           $totalPayment = $p->value_total;
                           $startDate = $p->planed_start_at;
                           $endDate  =$p->planed_end_at;
                           $value = $p->value;
                    }
                    
                }else if ($status == 'MENDEKATIDEADLINE'){
                    if(date('Y-m-d', strtotime($p->planed_end_at. ' - 7 days')) <= date('Y-m-d', strtotime(now()))){
                        $proyekName = $p->name_project;
                        $proyekCode = $p->code_project;
                        $partnerName = $p->nama_pertner;
                        $typePayment = $p->jenis_top;  
                        $totalPayment = $p->value_total; 
                        $startDate = $p->planed_start_at;
                        $value = $p->value;
                        $endDate  =$p->planed_end_at;
                    } 
                   
                }              
                $response[] = [
                    'proyekName' => $proyekName,
                    'proyekCode' => $proyekCode,
                    'partnerName' =>  $partnerName,
                    'typePayment' =>  $typePayment,
                    'totalValue' =>  $totalPayment,
                    'value' => $value,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                ];
            }
            $result = array('status'=>true,'kode'=>1,'data'=>$response,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }
}
