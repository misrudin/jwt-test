<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_charter_all;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwKategoriProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

            /**
      * @OA\GET(
     *     path="/api/v1/kategoriProyek/getAllKatergoriProyek",
     *     tags={"Kategori Proyek"},
     *     operationId="getAllKatergoriProyek",
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getAllKatergoriProyek(Request $request){
        $table = new V_dash_charter_all();
        $kategori = DB::table('charter_categories')->get();
        if (!empty($kategori)){
            $data = array();
        // $dataArray['data'] = array();
            foreach ($kategori as $p) {
                if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                    if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_charter_all WHERE 
                        (category = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                        (category = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (category = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (category = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                         "));
                       
                    }
                }else{
                    $response =  DB::select(DB::raw("SELECT * FROM vw_dash_charter_all WHERE category = '$p->name'"));
                }
                $totalNilai=0;
                foreach ($response as $q) {
                    $totalNilai = $totalNilai+(int)$q->nilai_proyek;
                }
                $data[] = ['Kategori' => $p->name,'Total_Proyek' => count($response), 'Total_Nilai' => $totalNilai];
                 }
            $data = collect($data);
            $sorted = $data->sortBy('Total_Nilai')->reverse();
            $result = array('status'=>true,'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
                 
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

}