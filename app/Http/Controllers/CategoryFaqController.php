<?php

namespace App\Http\Controllers;

use App\Models\CategoryFaq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryFaqController extends Controller
{
    public function getAllFaqs()
    {
        $data = CategoryFaq::with('faqs:category_faq_id,id,question,answer')->orderBy('order')->get();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function index()
    {
        $data = CategoryFaq::orderBy('order')->get();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = $this->validation($data);
        if($validator->fails()){
            return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
        }
        try {
            $contact = CategoryFaq::create($data);
            return ['status' => true, 'kode' => 1, 'data' => $contact, 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    
    public function show($id)
    {
        try {
            $contact = CategoryFaq::findOrFail($id);
            return ['status' => true, 'kode' => 1, 'data' => $contact, 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    public function update(Request $request,$id)
    {
        $contact = CategoryFaq::find($id);
        $data = $request->all();
        $validator = $this->validation($data);
        if($validator->fails()){
            return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
        }
        try {
            $contact->update($data);
            return ['status' => true, 'kode' => 1, 'data' => $contact, 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    public function updateAll(Request $request)
    {   
        try {
            $categories = CategoryFaq::all();
            foreach ($categories as $category) {
                $category->timestamps = false;
                $id = $category->id;
                foreach ($request->categories as $ct) {
                    if ($ct['id'] == $id) {
                        $category->update(['order' => $ct['order']]);
                    }
                }
            }
            return ['status' => true, 'kode' => 1, 'data' => $categories, 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    
    public function destroy($id)
    {
        // delete the contact
        $contact = CategoryFaq::find($id);
        try {
            $contact->delete();
            return ['status' => true, 'kode' => 1, 'data' => 'Berhasil Hapus Data', 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    private function validation($data)
    {
        return Validator::make($data, [
            'faq_name' => 'required',
        ]);
    }
}
