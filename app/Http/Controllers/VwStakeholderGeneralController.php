<?php

namespace App\Http\Controllers;

use App\Models\VwStakeholderGeneral;
use Illuminate\Http\Request;
use DB;

class VwStakeholderGeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllStakeHolderGeneral(Request $request){
        if (empty($request->has('type'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter type tidak boleh kosong');
            return $result;
        }
        $type=strtolower($request->type);

        if ($type == 'project-manager'){
            //project manager
            $stakeholder_table = DB::table('stackholders')
            ->leftjoin('charter_stackholders', 'stackholders.code', '=', 'charter_stackholders.stackholder_code')
            ->leftjoin('stackholder_types', 'stackholders.stackholder_type_code', '=', 'stackholder_types.code')
            ->leftjoin('charters', 'charter_stackholders.charter_code', '=', 'charters.code')
            ->leftjoin('users','charters.created_by','=','users.id')
            ->select('stackholders.name as  stakeholder_name','users.tenant_id','stackholders.code as  stakeholder_code', 'stackholder_types.name as stackholder_type', 'charters.progress as progress')
            ->where('stackholder_types.name','Project Manager')
            ->where('users.tenant_id','!=','sda')
            ->whereNull('charters.deleted_at')
            ->get();
            if (!empty($stakeholder_table)){
            $data = array();
            foreach ($stakeholder_table as $p) {
                // return $p->stackholder_code;
                if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                    if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stake_holder WHERE 
                        (stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                        (stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                        (stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                        "));
                    }
                }else{
                    $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stake_holder WHERE  stackholder_types = 'Project Manager' AND stackholder_code = '$p->stakeholder_code'"));
                }
            $nilai=0;
            $proyekActive=0;
                foreach ( $response as $q) {
                    $nilai = $nilai+(int)$q->nilai_proyek;
                    $proyekActive = $proyekActive+(int)$q->total_proyek;
                }
                $data[] = [
                    'dataname' => $p->stakeholder_name,
                    'proyek' => $proyekActive, 
                    'progress' => $p->progress == null ? 0 :  $p->progress, 
                    'nilai' => $nilai,
                    'tenant_id' => $p->tenant_id,
                ];
                }
                $data = collect($data);
                $sorted = $data->sortBy('nilai')->reverse();
            $result = array('status'=>true,'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
                
            }else{
                $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
            }
            return $result;
        }elseif ($type == 'partner') {
                    if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                        if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                            $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stakeholder_general_customer WHERE 
                            (type = 'PARTNER' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                            (type = 'PARTNER' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                            (type = 'PARTNER' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                            (type = 'PARTNER' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                             "));
                        }
                    }else{
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stakeholder_general_customer WHERE type = 'PARTNER'"));
                    }
                    $arr = [];
                    foreach ($response as $value) {
                        $arr[] = [
                            'dataname' => $value->dataname,
                            'proyek' => $value->proyek,
                            'realisasi' => round($value->realisasi),
                            'nilai_pembayaran' => round($value->nilai_pembayaran)
                        ];
                    }
                $data = collect($arr);
                $sorted = $data->sortBy('realisasi')->reverse();

            $result = array('status'=>true,'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
            return $result;

        }elseif ($type == 'customer') {
                    if($request->has('filterDateFrom') && $request->has('filterDateTo')){
                        if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                            $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stakeholder_general_customer WHERE 
                            (type = 'CUSTOMER' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                            (type = 'CUSTOMER' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                            (type = 'CUSTOMER' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                            (type = 'CUSTOMER' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                             "));
                        }
                    }else{
                        $response =  DB::select(DB::raw("SELECT * FROM vw_dash_stakeholder_general_customer WHERE type = 'CUSTOMER'"));
                    }
                    $arr = [];
                    foreach ($response as $value) {
                        $arr[] = [
                            'dataname' => $value->dataname,
                            'proyek' => $value->proyek,
                            'progress' => $value->progress,
                            'penyerapan' => round($value->budget_spent) == 0 ? 0 : $value->budget_spent / $value->budget_allocated *100
                        ];
                    }
                $data = collect($arr);
                $sorted = $data->sortBy('realisasi')->reverse();

            $result = array('status'=>true,'kode'=>1,'data'=>$sorted->values()->toArray(),'pesan'=>'Data Ditemukan');
            return $result;
        }else{
           return $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
    }
}
