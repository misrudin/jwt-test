<?php

namespace App\Http\Controllers;

use App\Models\VwMainProyekGeneral;
use Illuminate\Http\Request;
use DB;

class VwMainProyekGeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $data =  DB::select(DB::raw("SELECT * FROM vw_main_proyek_generals WHERE 
                (planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));
            }
        }
        $arr = [];
        foreach ($data as $value) {
            $issue = DB::table("vw_dash_laporan_issue_proyek")->get();
            $totalIssue = [];
            foreach ($issue as $key) {
                if($value->proyek_name == $key->name_proyek ) {
                    $totalIssue[] = [
                        'issue_name' => $key->issue_name,
                        'code_proyek' => $key->code_proyek,
                        'name_proyek' => $key->name_proyek,
                        'issue_category' => $key->issue_category,
                        'issue_status' => $key->issue_status,
                        'impacts' => $key->impacts,
                    ];
                }
            }
            $arr[] = [
                'proyek_name' => $value->proyek_name,
                'total_proyek' => $value->total_proyek,
                'kandidat' => $value->kandidat,
                'aktif' => $value->aktif,
                'closed' => $value->closed,
                'draft' => $value->draft,
                'tech_closed' => $value->tech_closed,
                'progress' => round($value->budget_spent) == 0 ? 0 : $value->budget_spent/$value->budget_allocated * 100,
                'budget_spent' => $value->budget_spent,
                'budget_allocated' => $value->budget_allocated,
                'issue' => $totalIssue
            ];
        }
        
        
        $result = [
            'status'=>true,
            'kode'=>1,
            'data'=> $arr,
            'pesan'=>'Data Ditemukan'
        ];
        return $result;
        
    }

    
}
