<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class VwResikoGeneralController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = DB::table("vw_dash_resiko_general")->get();
        if(!empty($data)) {
            foreach ($data as $value) {
                $arr[] = [
                    'name' => $value->resiko,
                    'value' => $value->total
                ];
            }
            
            $result = array('status'=>true,'kode'=>1,'data'=>$arr,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }
}
