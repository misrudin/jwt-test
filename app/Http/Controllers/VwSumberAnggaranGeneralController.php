<?php

namespace App\Http\Controllers;

use App\Models\VwSumberAnggaranGeneral;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DB;

class VwSumberAnggaranGeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSumberAnggaranGeneral(Request $request)
    {
        $itemsPerPage = 3;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        
        $itemsPerPage = $request->itemsPerPage;
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $response =  DB::select(DB::raw("SELECT * FROM vw_sumber_anggaran_generals WHERE 
                (planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo')) ORDER BY budget_allocated DESC"));
            }
        }else{
            $response =  DB::select(DB::raw("SELECT * FROM vw_sumber_anggaran_generals ORDER BY budget_allocated DESC"));
        }
            $arr = [];
            foreach ($response as $value) {
                $arr[] = [
                    'proyek_name' => $value->name,
                    'progress' => round($value->budget_spent) == 0 ? 0 : $value->budget_spent/$value->budget_allocated * 100,
                    'budget_allocated' => round($value->budget_allocated),
                    'budget_spent' => round($value->budget_spent),
                ];
            }
            $result = [
                'status'=>true,
                'kode'=>1,
                'data'=>$this->_paginationArray($arr, $itemsPerPage),
                'pesan'=>'Data Ditemukan'
            ];
        return $result;
    }

    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }

}
