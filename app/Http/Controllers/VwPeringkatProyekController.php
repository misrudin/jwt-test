<?php

namespace App\Http\Controllers;

use App\Models\VwPeringkatProyek;
use Illuminate\Http\Request;
use DB;
use DateTime;
use Carbon\Carbon;

class VwPeringkatProyekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param = ucfirst(strtoupper($request->filter));
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $data = DB::table('vw_peringkat_proyeks')
                ->whereBetween('created_at', [$request->filterDateFrom, $request->filterDateTo]);
            }
        } else {
            $data = DB::table('vw_peringkat_proyeks');
        }
        
        // return $data->get();
        if($request->orderBy == 'ASC') {
            $data->orderBy('budget_allocated','asc');
        } else {
            $data->orderBy('budget_allocated','desc');
        }
            if($param == 'BUDGET') {
                $arr = [];
                foreach ($data->get() as $value) {
                    $arr[] = [
                        'proyek_name' => $value->proyek_name,
                        'progress' => round($value->budget_spent) == 0 ? 0 : $value->budget_spent / $value->budget_allocated * 100,
                        'budget_allocated' => round($value->budget_allocated),
                    ];
                };
            }
            if($param == 'PROGRESS') {
                $arr = [];
                foreach ($data->get() as $value) {
                    $arr[] = [
                        'proyek_name' => $value->proyek_name,
                        'progress' =>  round($value->budget_spent) == 0 ? 0 : $value->budget_spent / $value->budget_allocated * 100,
                        'total_deliveriable' => $value->deliverable,
                    ];
                };
            }
            if($param == 'DURATION') {
                $arr = [];
                $status = '';
                foreach ($data->get() as $value) {
                    if($value->delay == true) {
                        $status = 'delay';
                    } elseif($value->lag == true) {
                        $status = 'lag';
                    } elseif($value->lead == true) {
                        $status = 'lead';
                    } 
                    $arr[] = [
                        'proyek_name' => $value->proyek_name,
                        'target' => $value->duration,
                        'realization' => Carbon::parse($value->planed_start_at)->diffInDays(Carbon::parse($value->planed_end_at)),
                        'status'=> $status
                    ];
                };
            }
            $result = [
                'status'=>true,
                'kode'=>1,
                'data'=>$arr,
                'pesan'=>'Data Ditemukan'
            ];
            return $result;
        
    }

}
