<?php

namespace App\Http\Controllers;

use App\Models\VideoTutorial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class VideoTutorialController extends Controller
{
    public function index()
    {
        $data = VideoTutorial::all();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = $this->validation($data);
        if($validator->fails()){
            return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
        }
        try {
            $contact = VideoTutorial::create($data);
            return ['status' => true, 'kode' => 1, 'data' => $contact, 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    
    public function show($id)
    {
        try {
            $contact = VideoTutorial::findOrFail($id);
            return ['status' => true, 'kode' => 1, 'data' => $contact, 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    public function update(Request $request)
    {
        $contact = VideoTutorial::find($request->id);
        $data = $request->all();
        $validator = $this->validation($data);
        if($validator->fails()){
            return response()->json(['status' => false, 'kode' => 2, 'pesan' => 'Terjadi Error', 'data' => $validator->messages()], 400);
        }
        try {
            $contact->update($data);
            return ['status' => true, 'kode' => 1, 'data' => $contact, 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    
    public function destroy($id)
    {
        // delete the contact
        $contact = VideoTutorial::find($id);
        try {
            $contact->delete();
            return ['status' => true, 'kode' => 1, 'data' => 'Berhasil Hapus Data', 'pesan' => 'Data Ditemukan'];
        } catch (\Exception $e) {
            return ['status' => false, 'kode' => 2, 'pesan' => 'Data Tidak Ditemukan'];
        }
    }

    private function validation($data)
    {
        return Validator::make($data, [
            'title' => 'required',
            'embed_link' => 'required',
        ]);
    }
}
