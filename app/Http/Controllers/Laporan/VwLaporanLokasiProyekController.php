<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_anggaran_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class VwLaporanLokasiProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getLaporanAllLokasiProyek(Request $request)
    {
        // $table = new V_dash_lokasi_proyek();
        // $cities = DB::table('provinces')->select('name')->get();
        // return $cities;
        // if (!empty($cities)) {

        //     $dataFinal = array();
        //     $features = array();

        //     // $dataArray['data'] = array();
        //     foreach ($cities as $p) {
        //         $data = array();

        //         if ($request->has('filterDateFrom') && $request->has('filterDateTo')) {
        //             if ($request->filterDateFrom != '' && $request->filterDateTo != '') {
        //                 $response =  DB::select(DB::raw("SELECT * FROM vw_dash_lokasi_proyek WHERE 
        //                 (provinsi = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
        //                 (provinsi = '$p->name' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
        //                 (provinsi = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
        //                 (provinsi = '$p->name' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
        //                  "));
        //             }
        //         } else {
        //             $response =  DB::select(DB::raw("SELECT * FROM vw_dash_lokasi_proyek WHERE provinsi = '$p->name'"));
        //         }
        //         $totalNilai = 0;
        //         $issue = 0;
        //         $provinceName = '';
        //         foreach ($response as $q) {
        //             $totalNilai = $totalNilai + (int)$q->nilai_proyek;
        //             $issue = $issue + (int)$q->issue;
        //             $provinceName = $q->provinsi;
        //         }
        //         $features[] = ['province' => $p->name,  'totalProyek' => count($response), 'totalNilai' => $totalNilai, 'isu' => $issue];
        //     }


        //     $features = collect($features);
        //     $sorting = $features->sortBy('totalNilai')->reverse();
        //     $sorted = $sorting->values()->toArray();


        //     $result = array('status' => true, 'kode' => 1, 'data' => $sorted, 'pesan' => 'Data Ditemukan');
        // } else {
        //     $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        // }
    try {
        if (empty($request->itemsPerPage)) {
            return  array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'param itemsPerPage tidak boleh kosong');
        }
        if (empty($request->page)) {
            return  array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'param page tidak boleh kosong');
        }

        if (!empty($request->filterDateFrom) && !empty($request->filterDateTo)) {

            $data = DB::select("select * from fn_dash_lokasi_project('$request->filterDateFrom', '$request->filterDateTo') l WHERE lower(province) LIKE '%$request->search%' OR l.totalproyek::text like '%$request->search%'");
        } else {
            $data = DB::select("select * from fn_dash_lokasi_project() l WHERE lower(province) LIKE '%$request->search%' OR l.totalproyek::text like '%$request->search%' order by l.totalNilai desc");
        }
        $arr = [];
        foreach ($data as $key => $value) {
            if($value->totalproyek > 0) {
                $arr[$key] = [
                    'province' => $value->province,
                    'totalproyek' => $value->totalproyek,
                    'totalnilai' => $value->totalnilai,
                    'isu' => $value->isu
                ];
            }
        }
        $q = $this->_paginationArray($arr, $request->itemsPerPage);
        return array('status' => true, 'kode' => 1, 'data' => $q, 'pesan' => 'Data Ditemukan');
    } catch(\Exception $exception){
        return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
    }

    }

//    public function searchLokasiProyek(Request $request)
//    {
//        if (empty($request->filterDateFrom) || empty($request->filterDateTo)) return  array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'param filterDateFrom atau FilterDateTo tidak boleh kosong');
//
//        $data = DB::select("select * from fn_dash_lokasi_proyek('$request->filterDateFrom', '$request->filterDateTo') l WHERE lower(province) LIKE '%$request->search%' OR l.totalproyek::text like '%$request->search%'");
//
//
//        $q = $this->_paginationArray($data, $request->perPage);
//
//        if (!empty($data)) {
//            return array('status' => true, 'kode' => 1, 'data' => $q, 'pesan' => 'Data Ditemukan');
//        } else {
//            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
//        }
//    }

    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }
}
