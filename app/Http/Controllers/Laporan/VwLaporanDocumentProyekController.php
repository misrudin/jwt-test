<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use App\Models\rc;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Carbon\CarbonPeriod;

use DB;

class VwLaporanDocumentProyekController extends Controller
{
     //     /**
    //  * @OA\GET(
    //  *     path="/api/v1/dokument/documentLopWin/",
    //  *     tags={"List Direktory"},
    //  *     operationId="documentLopWin",
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */

    public function documentLopWin(Request $request)
    {
        $itemsPerPage = 5;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        
        $itemsPerPage = $request->itemsPerPage;
        if ($request->has('segmentType')) {
            $arraysegmentType = $request->segmentType;
            
            foreach ($arraysegmentType as $r) {
                $formatArraysegmentType = ucwords(strtolower($r));
                if($formatArraysegmentType == 'All') {
                    $segment = DB::table('charter_segments')->select('id','name')->get();
                    $segmentType = array();
                    foreach ($segment as $s) {
                        $segmentType[] = "segment_name='".$s->name."'";
                    };
                }
                $segmentType[] = "segment_name='" . $formatArraysegmentType . "'";
            }
            //generate query string
            $segmentType = implode(" OR ", $segmentType);
        }
        
        $data = DB::table('vw_laporan_document_lop_win')->whereRaw($segmentType);
        if (!empty($request->search)) {
            $data->orWhereRaw('LOWER(project_code) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(segment_name) LIKE ?', ['%' . $request->search . '%']);
        }
        if(!empty($data)) {
            $arr = [];
            foreach($data->get() as $key => $dt){
                $arr[$key] = [
                    'project_code' => $dt->project_code,
                    'project_name' => $dt->project_name,
                    'segment_name' => $dt->segment_name,
                    'am' => $dt->am,
                    'no_quote' => empty(json_decode($dt->no_quote)[0]->quote) ? '-' : implode(json_decode($dt->no_quote)[0]->quote),
                    'no_so' => $dt->no_so,
                    'spk' => $dt->spk
                ];
            }
            $result = array('status'=>false,'kode'=>2,'data'=> $this->_paginationArray($arr, $itemsPerPage),'pesan'=>'Data Ditemukan');
        } else {
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }


    //     /**
    //  * @OA\GET(
    //  *     path="/api/v1/dokument/documentSpkMitra/",
    //  *     tags={"List Direktory"},
    //  *     operationId="documentSpkMitra",
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */
    public function documentSpkMitra(Request $request)
    {
        $itemsPerPage = 5;
        $itemsPerPage = $request->itemsPerPage;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }

        if ($request->has('paymentType')) {
            $arrayPaymentType = $request->paymentType;
            $paymentTypePagination = array();
            $paymentType = array();
            $paymentNotType = array();


            foreach ($arrayPaymentType as $r) {
                $formatArrayPaymentType = ucwords(strtolower($r));
                //handle All
                if ($formatArrayPaymentType == 'All') {
                    $paymentType = array();
                    $paymentType[] = "jenis_top='One Time Charge (OTC)'"; // AND paymentType= Over Budget, On Budget, All
                    $paymentType[] = "jenis_top='Location Based'";
                    $paymentType[] = "jenis_top='Termin Based'";
                    $paymentType[] = "jenis_top='Progress Based'";
                    $paymentType[] = "jenis_top='Monthly Recurring'";

                    $paymentNotType[] = "jenis_top='One Time Charge (OTC)'"; // AND paymentNotType= Over Budget, On Budget, All
                    $paymentNotType[] = "jenis_top='Location Based'";
                    $paymentNotType[] = "jenis_top='Termin Based'";
                    $paymentNotType[] = "jenis_top='Progress Based'";
                    $paymentNotType[] = "jenis_top='Monthly Recurring'";
                    break;
                }
                //add modelCost
                $paymentType[] = "jenis_top='" . $formatArrayPaymentType . "'";
            }
            //generate query string
            $paymentType = implode(" AND ", $paymentType);
            $paymentNotType = implode(" OR ", $paymentNotType);
        }
        
        // return $paymentType;
        $data = DB::table('vw_laporan_document_spk_mitra')->whereRaw($paymentNotType)
        ->select('kode_partner', 'nama_partner','jenis_top','value_total','date','status', DB::raw('count(*) AS total_document'))
        ->groupBy('kode_partner', 'nama_partner','jenis_top','value_total','date','status');
        
        if ($request->has('filterDateFrom') && $request->has('filterDateTo')) {
            if ($request->filterDateFrom != '' && $request->filterDateTo != '') {
                $data = DB::table('vw_laporan_document_spk_mitra')
                ->whereBetween('date', [$request->filterDateFrom, $request->filterDateTo])
                ->orWhereRaw($paymentType)
                ->select('kode_partner', 'nama_partner','jenis_top','value_total','date','status', DB::raw('count(*) AS total_document'))
                ->groupBy('kode_partner', 'nama_partner','jenis_top','value_total','date','status');
            }
        } 

       

        if (!empty($request->search)) {
            $data = DB::table('vw_laporan_document_spk_mitra')
                ->select('kode_partner', 'nama_partner','jenis_top','value_total','date','status', DB::raw('count(*) AS total_document'))
                ->groupBy('kode_partner', 'nama_partner','jenis_top','value_total','date','status')
                ->whereRaw('LOWER(kode_partner) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(nama_partner) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(jenis_top) LIKE ?', ['%' . $request->search . '%']);
        } 

        if(!empty($data)) {
            $result = array('status'=>false,'kode'=>2,'data'=>$data->paginate($itemsPerPage),'pesan'=>'Data Ditemukan');
        } else {
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    


    //     /**
    //  * @OA\GET(
    //  *     path="/api/v1/dokument/documentTopKb/",
    //  *     tags={"List Direktory"},
    //  *     operationId="documentTopKb",
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */


    public function documentTopKb(Request $request)
    {
        $itemsPerPage = 5;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }

        $itemsPerPage = $request->itemsPerPage;

        

        if ($request->has('statusType')) {
            $arraystatusType = $request->statusType;
            $statusType = array();
            foreach ($arraystatusType as $r) {
                $formatArraystatusType = ucwords(strtolower($r));
                //handle All
                if ($formatArraystatusType == 'All') {
                    $statusType = array();
                    $statusType[] = "status='Kandidat'"; // AND statusType= Over Budget, On Budget, All
                    $statusType[] = "status='Aktif'";
                    $statusType[] = "status='Tech Closed'";
                    $statusType[] = "status='Closed'";
                    $statusType[] = "status='Batal'";
                    $statusType[] = "status='Draft'";
                    break;
                }
                //add modelCost
                $statusType[] = "status='" . $formatArraystatusType . "'";
            }
            $statusType = implode(" OR ", $statusType);
        }

        if ($request->has('paymentType')) {
            $arrayPaymentType = $request->paymentType;
            $paymentType = array();


            foreach ($arrayPaymentType as $r) {
                $formatArrayPaymentType = ucwords(strtolower($r));
                //handle 
                
                if($request->has('statusType')) {
                    if ($formatArrayPaymentType == 'All') {
                        $paymentType = array();
                        $paymentType[] = "payment_name='One Time Charge (OTC)' AND $statusType";
                        $paymentType[] = "payment_name='Location Based' AND $statusType";
                        $paymentType[] = "payment_name='Termin Based' AND $statusType";
                        $paymentType[] = "payment_name='Progress Based' AND $statusType";
                        $paymentType[] = "payment_name='Monthly Recurring' AND $statusType";
                        break;
                    }
                    //add modelCost
                    $paymentType[] = "payment_name='" . $formatArrayPaymentType . "' AND $statusType";
                }  else {
                    if ($formatArrayPaymentType == 'All') {
                        $paymentType = array();
                        $paymentType[] = "payment_name='One Time Charge (OTC)'";
                        $paymentType[] = "payment_name='Location Based'";
                        $paymentType[] = "payment_name='Termin Based'";
                        $paymentType[] = "payment_name='Progress Based'";
                        $paymentType[] = "payment_name='Monthly Recurring'";
                        break;
                    }
                    //add modelCost
                    $paymentType[] = "payment_name='" . $formatArrayPaymentType . "'";
                }
            }
            //generate query string
            $paymentType = implode(" OR ", $paymentType);
        }

        if ($request->has('segmentType')) {
            $arraysegmentType = $request->segmentType;
            
            foreach ($arraysegmentType as $r) {
                $formatArraysegmentType = ucwords(strtolower($r));
                $segment = DB::table('charter_segments')->select('id','name')->get();
                $segmentType = array();
                if($request->has('paymentType')) {
                    if($formatArraysegmentType == 'All') {
                        foreach ($segment as $s) {
                            $segmentType[] = "segment_name='".$s->name."' AND $paymentType";
                        };
                    }
                    $segmentType[] = "segment_name='" . $formatArraysegmentType . "' AND $paymentType";
                } else {
                    if($formatArraysegmentType == 'All') {
                        foreach ($segment as $s) {
                            $segmentType[] = "segment_name='".$s->name."'";
                        };
                    }
                    $segmentType[] = "segment_name='" . $formatArraysegmentType . "'";
                }
            }
            //generate query string
            $segmentType = implode(" AND ", $segmentType);
        }


        $data = DB::table('vw_laporan_document_top_kb')
                ->whereRaw($segmentType);
        if (!empty($request->search)) {
            $data->orWhereRaw('LOWER(project_code) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(segment_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(pm_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(payment_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(status) LIKE ?', ['%' . $request->search . '%']);
        }

        if(!empty($data)) {
            $arr = [];

            foreach($data->get() as $key => $dt){

                $arr[$key] = [
                    'project_code' => $dt->project_code,
                    'project_name' => $dt->project_name,
                    'segment_name' => $dt->segment_name,
                    'pm_name' => $dt->pm_name,
                    'project_value' => round($dt->project_value),
                    'payment_name' => $dt->payment_name,
                    'status' => $dt->status,
                    'expected_date' => $dt->expected_date,
                    'id_lop' => implode(json_decode($dt->id_lop)[0]->lop)
                ];
            }
            $result = array('status'=>false,'kode'=>2,'data'=>$this->_paginationArray($arr, $itemsPerPage),'pesan'=>'Data Ditemukan');
            
        } else {
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }


    public function documentTopKl(Request $request)
    {
        $itemsPerPage = 5;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }

        $itemsPerPage = $request->itemsPerPage;

        if ($request->has('statusType')) {
            $arraystatusType = $request->statusType;
            $statusType = array();
            foreach ($arraystatusType as $r) {
                $formatArraystatusType = ucwords(strtolower($r));
                //handle All
                if ($formatArraystatusType == 'All') {
                    $statusType = array();
                    $statusType[] = "status='Kandidat'"; // AND statusType= Over Budget, On Budget, All
                    $statusType[] = "status='Aktif'";
                    $statusType[] = "status='Tech Closed'";
                    $statusType[] = "status='Closed'";
                    $statusType[] = "status='Batal'";
                    $statusType[] = "status='Draft'";
                    break;
                }
                //add modelCost
                $statusType[] = "status='" . $formatArraystatusType . "'";
            }
            $statusType = implode(" OR ", $statusType);
        }

        if ($request->has('paymentType')) {
            $arrayPaymentType = $request->paymentType;
            $paymentType = array();


            foreach ($arrayPaymentType as $r) {
                $formatArrayPaymentType = ucwords(strtolower($r));
                //handle 
                
                if($request->has('statusType')) {
                    if ($formatArrayPaymentType == 'All') {
                        $paymentType = array();
                        $paymentType[] = "payment_name='One Time Charge (OTC)' AND $statusType";
                        $paymentType[] = "payment_name='Location Based' AND $statusType";
                        $paymentType[] = "payment_name='Termin Based' AND $statusType";
                        $paymentType[] = "payment_name='Progress Based' AND $statusType";
                        $paymentType[] = "payment_name='Monthly Recurring' AND $statusType";
                        break;
                    }
                    //add modelCost
                    $paymentType[] = "payment_name='" . $formatArrayPaymentType . "' AND $statusType";
                }  else {
                    if ($formatArrayPaymentType == 'All') {
                        $paymentType = array();
                        $paymentType[] = "payment_name='One Time Charge (OTC)'";
                        $paymentType[] = "payment_name='Location Based'";
                        $paymentType[] = "payment_name='Termin Based'";
                        $paymentType[] = "payment_name='Progress Based'";
                        $paymentType[] = "payment_name='Monthly Recurring'";
                        break;
                    }
                    //add modelCost
                    $paymentType[] = "payment_name='" . $formatArrayPaymentType . "'";
                }
            }
            //generate query string
            $paymentType = implode(" OR ", $paymentType);
        }

        if ($request->has('segmentType')) {
            $arraysegmentType = $request->segmentType;
            
            foreach ($arraysegmentType as $r) {
                $formatArraysegmentType = ucwords(strtolower($r));
                $segment = DB::table('charter_segments')->select('id','name')->get();
                $segmentType = array();
                if($request->has('paymentType')) {
                    if($formatArraysegmentType == 'All') {
                        foreach ($segment as $s) {
                            $segmentType[] = "segment_name='".$s->name."' AND $paymentType";
                        };
                    }
                    $segmentType[] = "segment_name='" . $formatArraysegmentType . "' AND $paymentType";
                } else {
                    if($formatArraysegmentType == 'All') {
                        foreach ($segment as $s) {
                            $segmentType[] = "segment_name='".$s->name."'";
                        };
                    }
                    $segmentType[] = "segment_name='" . $formatArraysegmentType . "'";
                }
            }
            //generate query string
            $segmentType = implode(" AND ", $segmentType);
        }

        $data = DB::table('vw_laporan_document_top_kl')->whereRaw($segmentType);

        if (!empty($request->search)) {
            $data->orWhereRaw('LOWER(project_code) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(segment_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(payment_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(status) LIKE ?', ['%' . $request->search . '%']);
        }

        if(!empty($data)) {
            $arr = [];

            foreach($data->get() as $key => $dt){

                $arr[$key] = [
                    'project_code' => $dt->project_code,
                    'project_name' => $dt->project_name,
                    'segment_name' => $dt->segment_name,
                    'payment_name' => $dt->payment_name,
                    'status' => $dt->status,
                    'no_pb' => $dt->no_pb,
                    'expected_date' => $dt->expected_date,
                    'id_lop' => implode(json_decode($dt->idlop)[0]->lop)
                ];
            }
            $result = array('status'=>false,'kode'=>2,'data'=>$this->_paginationArray($arr, $itemsPerPage),'pesan'=>'Data Ditemukan');
            
        } else {
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }


    public function getAllSegment(Request $request)
    {
        $data = DB::table('charter_segments')->select('id','name')->get();
        if(!empty($data)) {
            $result = array('status'=>false,'kode'=>2,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else {
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    
    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }
}
