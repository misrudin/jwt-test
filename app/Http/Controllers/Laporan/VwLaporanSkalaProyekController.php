<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_anggaran_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DB;

class VwLaporanSkalaProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * @OA\GET(
     *     path="/api/v1/laporan/SkalaProyek/getLaporanAllSkalaProyek",
     *     tags={"Progress Pencapaian Proyek"},
     *     operationId="getLaporanAllSkalaProyek",
     *      @OA\Parameter(
     *          name="status",
     *          description="status (Draft,Kandidat, Aktif, Tech Closed, Closed, Batal,  )",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filterDateFrom",
     *          description="format date",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filterDateTo",
     *          description="format date",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getLaporanAllSkalaProyek(Request $request)
    { 
        if (empty($request->has('codeSkala'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Parameter skala tidak boleh kosong');
            return $result;
        }
        $skala = ucfirst(strtoupper($request->codeSkala));
        $itemsPerPage = 5;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        $itemsPerPage = $request->itemsPerPage;

        if ($request->has('filterDateFrom') && $request->has('filterDateTo')) {
            if ($request->filterDateFrom != '' && $request->filterDateTo != '') {
                $data = DB::table('vw_dash_laporan_skala_proyek')->where(DB::raw("(code_skala = '$skala'  AND planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                 (code_skala = '$skala'  AND planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                 (code_skala = '$skala'  AND planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                 (code_skala = '$skala'  AND planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))"));
            }
        } else {
            $data = DB::table('vw_dash_laporan_skala_proyek')
                ->where('code_skala', $skala);
        }

        if ($request->has('status')) {
            if($request->status != ['All']) {
                $data->whereIn('status_proyek',$request->status);
            } 
        }

        if (!empty($request->search)) {
            $data->whereRaw('LOWER(skala) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(proyek_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(code_proyek) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_manager) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(account_manager) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(status_proyek) LIKE ?', ['%' . $request->search . '%']);
        }
        // return $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');

        if (!empty($data)) {
            
            $arr = [];
            foreach ($data->get() as $value) {
                $arr[] = [
                    "nilai_proyek" => round($value->nilai_proyek),
                    "proyek_name"=> $value->proyek_name,
                    "skala"=> $value->skala,
                    "code_skala"=> $value->code_skala,
                    "code_proyek"=> $value->code_proyek,
                    "project_manager"=> $value->project_manager,
                    "account_manager"=> null,
                    "status_proyek"=> $value->status_proyek,
                    "progress"=> $value->progress,
                    "updated_at"=> $value->updated_at,
                    "planed_start_at"=> $value->planed_start_at,
                    "planed_end_at"=> $value->planed_end_at
                ];
            }
            // return $arr;
            // $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('skala', $skala);
            $result =  array('status' => true, 'kode' => 1, 'data' => $this->_paginationArray($arr, $itemsPerPage), 'pesan' => 'Data Ditemukan');
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function searchLaporanSkalaProyek(Request $request)
    {
        $data = DB::table('vw_dash_laporan_skala_proyek');
        if (!empty($request->search)) {
            $data->whereRaw('LOWER(skala) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(proyek_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(code_proyek) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_manager) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(account_manager) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(status_proyek) LIKE ?', ['%' . $request->search . '%']);
        }
        if (!empty($data)) {
            return array('status' => true, 'kode' => 1, 'data' => $data->simplePaginate($request->perPage), 'pesan' => 'Data Ditemukan');
        } else {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }

    public function getValueSkala(Request $request)
    {
        $data = DB::table('charter_value_categories')->whereNull('deleted_at')->select('code','name');
        $arr = [];
        foreach ($data->get() as $key => $value) {
            $arr[$key] = [
                'label' => $value->name,
                'value' => $value->code
            ];
        }
        if(!empty($arr)) {
            $result = array('status' => true, 'kode' => 1, 'data' => $arr, 'pesan' => 'Data Ditemukan');
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }
    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }
}
