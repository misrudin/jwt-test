<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DB;

class VwLaporanRegionalController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllRegional(Request $request)
    {
        $itemsPerPage = 5;
        $itemsPerPage = $request->itemsPerPage;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        
        $query = DB::table('vw_laporan_regional');
        
        $query->when($request->input('status'), function($q) use($request) {
            if($request->status != ["All"]) {
                return $q->whereIn('status_proyek', $request->input('status'));
            }
        });

        $query->when($request->input('regional'), function($q) use($request) {
            if($request->regional != ["All"]) { 
                return $q->whereIn('name_regional', $request->input('regional'));
            }
        });

        $query->when($request->input('picType'), function($q) use($request) {
            if($request->picType != ["All"]) {
                return $q->whereIn('pic', $request->input('picType'));
            }
        });

        $query->when($request->input('progressFrom'), function($q) use($request) {
            return $q->whereBetween('progress', [$request->progressFrom, $request->progressTo]);
        });

        $query->when($request->input('search'), function($q) use($request) {
            return $q->where(DB::raw('lower(proyek_name)'), 'like', '%' . strtolower($request->search) . '%');
        });

        if (!empty($query)) {
            $arr =[];
            foreach ($query->get() as $key => $value) {
                $arr[$key] = [
                    'id_lop' => empty(json_decode($value->id_lop)[0]->lop) ? '-' : implode(json_decode($value->id_lop)[0]->lop),
                    'proyek_code' => $value->proyek_code,
                    'proyek_name' => $value->proyek_name,
                    'regional' => $value->name_regional,
                    'pic' => $value->pic,
                    'email' => $value->email,
                    'progress' => $value->progress,
                    'status_proyek' => $value->status_proyek,
                    'segment_name' => $value->segment_name,
                ];
            }
            $result = array('status' => true, 'kode' => 1, 'data' => $this->_paginationArray($arr, $itemsPerPage), 'pesan' => 'Data Ditemukan');
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function getRegionalType(Request $request)
    {
        $regionalData = DB::table('charter_regionals')->select('name')->get();
        if(!empty($regionalData)) {
                $result = array('status' => true, 'kode' => 1, 'data' => $regionalData, 'pesan' => 'Data Ditemukan');
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function getPicType(Request $request)
    {
        $picData = DB::select(DB::raw("SELECT name FROM users WHERE is_active = true"));
        if(!empty($picData)) {
                $result = array('status' => true, 'kode' => 1, 'data' => $picData, 'pesan' => 'Data Ditemukan');
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }
    
    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }
}
