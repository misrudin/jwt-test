<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_anggaran_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DB;

class VwLaporanTermOfPaymentProyekController extends Controller
{


    public function getLaporanAllTermOfPaymentProyek(Request $request)
    {

        if (empty($request->has('param'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Param tidak boleh kosong');
            return $result;
        }
        $param = ucfirst(strtoupper($request->param));

        $itemsPerPage = 5;

        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }

        $itemsPerPage = $request->itemsPerPage;
        if ($param == 'MITRA' || $param == 'KLIEN') {
            $parameter = '';
            if ($param == 'MITRA') {
                $parameter = 'PARTNER';
            }
            if ($param == 'KLIEN') {
                $parameter = 'CUSTOMER';
            }
            if ($request->param != '') {
                $data = DB::table('vw_dash_laporan_list_top_charter_proyek')
                    ->where('type_partner', $parameter)->paginate($itemsPerPage);
            }
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Value param tidak Valid!!');
            return $result;
        }

        if (!empty($data)) {

            //formating data
            $dataFormatting = array();
            foreach ($data as $q) {

                if ($request->has('paymentType')) {
                    $arrayPaymentType = $request->paymentType;
                    $paymentTypePagination = array();
                    $paymentType = array();


                    foreach ($arrayPaymentType as $r) {
                        $formatArrayPaymentType = ucwords(strtolower($r));
                        //handle All
                        if ($formatArrayPaymentType == 'All') {
                            $paymentType = array();
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='One Time Charge (OTC)'"; // AND paymentType= Over Budget, On Budget, All
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='Location Based'";
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='Termin Based'";
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='Progress Based'";
                            $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='Monthly Recurring'";
                            $paymentTypePagination = array();
                            $paymentTypePagination[] = 'One Time Charge (OTC)';
                            $paymentTypePagination[] = 'Location Based';
                            $paymentTypePagination[] = 'Termin Based';
                            $paymentTypePagination[] = 'Progress Based';
                            $paymentTypePagination[] = 'Monthly Recurring';
                            break;
                        }
                        //add modelCost
                        $paymentType[] = "id_partner='" . $q->id_partner . "' AND payment_type='" . $formatArrayPaymentType . "'";
                    }
                    //generate query string
                    $paymentType = implode(" OR ", $paymentType);
                }
                // return $paymentType;
                

                
                if(!empty($request->has('search'))) {
                    $dataTmp =  DB::table('vw_dash_laporan_term_of_payment_proyek')
                    // ->where('id_partner',$q->id_partner)
                    ->whereRaw('LOWER(proyek_name) LIKE ?', ['%' . $request->search . '%'])
                    ->orWhereRaw('LOWER(proyek_code) LIKE ?', ['%' . $request->search . '%'])
                    ->orWhereRaw('LOWER(name_partner) LIKE ?', ['%' . $request->search . '%'])
                    ->orWhereRaw('LOWER(payment_type) LIKE ?', ['%' . $request->search . '%'])
                    ->get();
                } else {
                    $dataTmp =  DB::table('vw_dash_laporan_term_of_payment_proyek')
                    // ->where('id_partner',$q->id_partner)
                    ->whereRaw($paymentType)
                    ->get();
                }
                
                if (!empty($dataTmp)) {
                    $paid = 0;
                    $unpaid = 0;
                    foreach ($dataTmp as $payment) {
                        if($payment->top_status === 'UNPAID') {
                            $unpaid += 1;
                        } else {
                            $paid += 1;
                        }
                    };

                    if (!count($dataTmp) == 0) {
                        $dataFormatting[] = [
                            'proyek_name' => $dataTmp[0]->proyek_name,
                            'proyek_code' => $dataTmp[0]->proyek_code,
                            'id_partner' => $q->id_partner,
                            'partner_name' => $dataTmp[0]->name_partner,
                            'payment_type' => $dataTmp[0]->payment_type,
                            'total_value' => $dataTmp[0]->total_value,
                            'paid_payment' => $paid,
                            'unpaid_payment' => $unpaid,
                            'detail_payment' => $dataTmp
                        ];
                    }
                }
            }





            if ($request->has('param')) {
                $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('param', $param);
            } else {
                $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('param', $param);
            }

            $result = array('status' => false, 'kode' => 2, 'data' => $this->_paginationArray($dataFormatting ,$itemsPerPage), 'pesan' => 'Data  Ditemukan');
            
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }


    public function getLaporanFilterSkalaProyek(Request $request)
    {
        if (empty($request->has('skala'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Parameter skala tidak boleh kosong');
            return $result;
        }
        $skala = ucfirst(strtoupper($request->skala));

        if ($request->has('status')) {
            $arrayStatus = $request->status;
            $statusPagination = array();
            $status = array();

            foreach ($arrayStatus as $q) {
                //handle All
                if ($q == 'All') {
                    $status = array();
                    $status[] = "(skala ='$skala' AND status_proyek='lead')";
                    $status[] = "(skala ='$skala' AND status_proyek='lag')";
                    $status[] = "(skala ='$skala' AND status_proyek='delay')";
                    $statusPagination = array();
                    $statusPagination[] = 'lead';
                    $statusPagination[] = 'lag';
                    $statusPagination[] = 'delay';

                    break;
                }
                //add filter
                $status[] = "(skala ='$skala' AND status_proyek='" . $q . " ')";
            }
            //generate query string
            $status = implode(" OR ", $status);
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Filter status tidak boleh kosong');
            return $result;
        }



        $itemsPerPage = 5;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        $itemsPerPage = $request->itemsPerPage;
        if ($request->has('status')) {
            if ($request->status != '') {
                $data = DB::table('vw_dash_laporan_skala_proyek')
                    ->whereRaw($status)
                    ->paginate($itemsPerPage);
            }
        } else {
            $data = DB::table('vw_dash_laporan_skala_proyek')
                ->whereRaw("skala = '$skala'")
                ->paginate($itemsPerPage);
        }

        if (!empty($data)) {
            if ($request->has('status')) {
                $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('skala', $skala)->appends('status', $statusPagination);
            } else {
                $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('skala', $skala);
            }


            return $data;
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }

    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }

}

