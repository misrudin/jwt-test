<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_anggaran_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;

class VwLaporanPartnerCustomerProyekController extends Controller
{



    public function getLaporanAllPartnerCustomerProyek(Request $request)
    {

        if (empty($request->has('param'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Param tidak boleh kosong');
            return $result;
        }

        $param = ucfirst(strtoupper($request->param));
        $itemsPerPage = 5;

        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }

        $itemsPerPage = $request->itemsPerPage;

        if ($param == 'MITRA' || $param == 'KLIEN') {
            $parameter = '';
            if ($param == 'MITRA') {
                $parameter = 'PARTNER';
            }
            if ($param == 'KLIEN') {
                $parameter = 'CUSTOMER';
            }

            $search = strtolower($request->search);
            if ($request->param != '') {
                $data = DB::table('vw_dash_laporan_partner_customer_proyek')->whereRaw(
                    "type='$parameter' AND lower(stakeholder_name) LIKE '%$search%'"
                );

                // ->orWhere('stakeholder_id', 'like', '%' . $request->search . '%')
                // ->orWhereRaw('LOWER(stakeholder_name) LIKE ?', ['%' . $request->search . '%']);
            }
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Value param tidak Valid!!');
            return $result;
        }

        if (!empty($data)) {
            // if ($request->has('param')) {
            //     $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('param', $param);
            // } else {
            //     $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('param', $param);
            // }



            // return "disini";
            return array('status' => true, 'kode' => 1, 'data' => $data->paginate($itemsPerPage), 'pesan' => 'Data Ditemukan');;
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }
}
