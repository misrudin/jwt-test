<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_anggaran_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DB;

class VwLaporanCategorySegmentProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * @OA\GET(
     *     path="/api/v1/laporan/CategorySegmentProyek/getLaporanAllCategorySegmentProyek",
     *     tags={"Progress Pencapaian Proyek"},
     *     operationId="getLaporanAllCategorySegmentProyek",
     *      @OA\Parameter(
     *          name="status",
     *          description="status (Draft,Kandidat, Aktif, Tech Closed, Closed, Batal,  )",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filterDateFrom",
     *          description="format date",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filterDateTo",
     *          description="format date",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getLaporanAllCategorySegmentProyek(Request $request)
    {
        if (empty($request->has('param'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Parameter tidak boleh kosong');
            return $result;
        }
        $param = ucfirst(strtoupper($request->param));

        $itemsPerPage = 5;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        $itemsPerPage = $request->itemsPerPage;

        if ($param == "CATEGORY") {

            $data = DB::table('vw_dash_laporan_category_proyek');
            if (!empty($request->search)) {
                $data->whereRaw('LOWER(category) LIKE ?', ['%' . $request->search . '%']);
            }
            if (!empty($data)) {
                // $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('param', $param);
                // return $data;
                $result = array('status' => true, 'kode' => 1, 'data' => $data->simplePaginate($request->perPage), 'pesan' => 'Data Ditemukan');
            } else {
                $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
            }
            return $result;
        }
        if ($param == "SEGMENT") {

            $data = DB::table('laporan_vw_project_segment');
            if (!empty($request->search)) {
                $data->whereRaw('LOWER(segment_code) LIKE ?', ['%' . $request->search . '%'])
                    ->orwhereRaw('LOWER(segment_name) LIKE ?', ['%' . $request->search . '%']);
            }
            if (!empty($data)) {
                // $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('param', $param);
                $arr = [];
                foreach ($data->get() as $key => $value) {
                    $arr[$key] = [
                        'code_segment' => $value->segment_code,
                        'segment' => $value->segment_name,
                        'total_project' => $value->total,
                        'total_value' => $value->nilai,
                    ];
                }
                $result = array('status' => true, 'kode' => 1, 'data' => $this->_paginationArray($arr, $itemsPerPage), 'pesan' => 'Data Ditemukan');
            } else {
                $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
            }
            return $result;
        } else {

            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Parameter salah!');
            return $result;
        }
    }

    public function searchLaporanCategorySegmentProyek(Request $request)
    {
        $data = DB::table('vw_dash_laporan_category_proyek');
        if (!empty($request->search)) {
            $data->whereRaw('LOWER(category) LIKE ?', ['%' . $request->search . '%']);
        }
        if (!empty($data)) {
            return array('status' => true, 'kode' => 1, 'data' => $data->simplePaginate($request->perPage), 'pesan' => 'Data Ditemukan');
        } else {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }

    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }
}
