<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_anggaran_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DB;

class VwLaporanProgressPencapaianController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * @OA\GET(
     *     path="/api/v1/laporan/progressPencapaian/getLaporanAllProgressPencapaian",
     *     tags={"Progress Pencapaian Proyek"},
     *     operationId="getLaporanAllProgressPencapaian",
     *      @OA\Parameter(
     *          name="status",
     *          description="status (Draft,Kandidat, Aktif, Tech Closed, Closed, Batal,  )",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filterDateFrom",
     *          description="format date",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filterDateTo",
     *          description="format date",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getLaporanAllProgressPencapaian(Request $request)
    {
        if (empty($request->has('statusProyek'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Parameter statusProyek tidak boleh kosong');
            return $result;
        }
        $statusProyek = ucwords(strtolower($request->statusProyek));

        if (empty($request->has('statusProgressProyek'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Parameter statusProgressProyek tidak boleh kosong');
            return $result;
        }
        $statusProgressProyek = ucfirst(strtolower($request->statusProgressProyek));

        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        $itemsPerPage = $request->itemsPerPage;


        if ($request->has('filterDateFrom') && $request->has('filterDateTo')) {
            if ($request->filterDateFrom != '' && $request->filterDateTo != '') {
                $data = DB::table('vw_dash_laporan_progress_achievment_proyek')
                    ->where(DB::raw("(charter_running_status = '$statusProyek' AND $statusProgressProyek='true' AND updated_at BETWEEN '$request->filterDateFrom' AND '$request->filterDateTo')"));
            }
        } else {

            $data = DB::table('vw_dash_laporan_progress_achievment_proyek')
                ->where(DB::raw("charter_running_status = '$statusProyek' AND $statusProgressProyek='true'"));
        }
        if (!empty($request->search)) {
            $data->whereRaw('LOWER(charter_running_status) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(proyek_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_manager) LIKE ?', ['%' . $request->search . '%'])
                ->orWhere('id', 'LIKE', ['%' . $request->search . '%']);
        }
        // return $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');


        if (!empty($data)) {
            // $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('statusProyek', $statusProyek)->appends('statusProgressProyek', $statusProgressProyek);
            $arr = [];
            foreach ($data->get() as $value) {
                $arr[] = [
                    'lead' => $value->lead,
                    'lag' => $value->lag,
                    'delay' => $value->delay,
                    'charter_running_status' => $value->charter_running_status,
                    'proyek_name' => $value->proyek_name,
                    'id' => $value->id,
                    'project_manager' => $value->project_manager,
                    'pm_username' => $value->pm_username,
                    'deliverable' => $value->deliverable,
                    'issue_open' => $value->issue_open,
                    'issue_total' => $value->issue_total,
                    'progress' => $value->progress,
                    'value' => $value->value,
                    'updated_at' => $value->updated_at,
                    'planed_start_at' => $value->planed_start_at,
                    'planed_end_at' => $value->planed_end_at,
                ];
            }
            $result =  array('status' => true, 'kode' => 1, 'data' => $this->_paginationArray($arr, $itemsPerPage), 'pesan' => 'Data Ditemukan');
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }
    public function searchLaporanProgressPencapaian(Request $request)
    {
        $data = DB::table('vw_dash_laporan_progress_achievment_proyek');
        if (!empty($request->search)) {
            $data->whereRaw('LOWER(charter_running_status) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(proyek_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_manager) LIKE ?', ['%' . $request->search . '%'])
                ->orWhere('id', 'LIKE', ['%' . $request->search . '%']);
        }
        if (!empty($data)) {
            return array('status' => true, 'kode' => 1, 'data' => $data->simplePaginate($request->perPage), 'pesan' => 'Data Ditemukan');
        } else {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }

    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }
}
