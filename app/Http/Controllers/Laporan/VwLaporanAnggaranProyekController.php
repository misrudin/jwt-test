<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_anggaran_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DB;

class VwLaporanAnggaranProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getLaporanAllAnggaranProyek(Request $request)
    {
          
        if (empty($request->has('runningStatus'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Parameter runningStatus tidak boleh kosong');
            return $result;
        }
        $running_status = ucwords(strtolower($request->runningStatus));

        $itemsPerPage = 5;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        $itemsPerPage = $request->itemsPerPage;

        $data = DB::table('table_wv_report_budget_project')
            ->whereRaw("charter_running_status='$running_status'");

        if($request->has('modelCost')) {
            if($request->modelCost != ['All']) {
                $data->whereIn('model_cost', $request->modelCost);
            }
        }
        if (!empty($request->search)) {
            $data->whereRaw('LOWER(charter_running_status) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(proyek_name) LIKE ?', ['%' . $request->search . '%']);
        }
        if (!empty($data)) {
        $arr = [];
        foreach ($data->get() as $key => $value) {
            $progress = 0;
            $jumlah = json_decode($value->anggaran);
            $bottomUp = 0;
            $khs = 0;
            $hasil = 0;
            foreach ($jumlah as $j) {
                $progress += $j->progress;
                $bottomUp += $j->bu;
                $khs += $j->khs;
            }
            if($value->model_cost == 'bottom up') {
               $hasil = $bottomUp;
            } elseif($value->model_cost == 'khs') {
                $hasil = $khs;
            }
            $arr[$key] = [
                // "progress" => $progress,
                "model_cost" => $value->model_cost,
                "charter_running_status" => $value->charter_running_status,
                "proyek_name" => $value->proyek_name,
                "value" => $value->value,
                "budget_allocated" => $value->budget_allocated,
                "budget_spent" => $value->budget_spent,
                "budget_gap" => $value->budget_gap,
                "budget_status" => null,
                "rencana_anggaran" => $value->budget_allocated,
                "berlangsung" => $progress > 0 ? $hasil : 0,
                "belum_berlangsung" => $progress == 0 ? $hasil : 0,
            ];
        } 
            // $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('running_status', $running_status);
            // return $data;
            $result =  array('status' => true, 'kode' => 1, 'data' => $this->_paginationArray($arr,$itemsPerPage), 'pesan' => 'Data Ditemukan');
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function getLaporanFilterAnggaranProyek(Request $request)
    {
        if (empty($request->has('runningStatus'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Parameter runningStatus tidak boleh kosong');
            return $result;
        }
        $running_status = ucfirst(strtolower($request->runningStatus));


        if ($request->has('statusCost')) {
            $arrayStatusCost = $request->statusCost;
            $statusCostPagination = array();
            $statusCost = array();

            foreach ($arrayStatusCost as $q) {
                $formatArrayStatusCost = ucwords(strtolower($q));
                //handle All
                if ($formatArrayStatusCost == 'All') {
                    $statusCost = array();
                    $statusCost[] = "budget_status='Over Budget'"; // AND statuscost= Over Budget, On Budget, All
                    $statusCost[] = "budget_status='On Budget'";
                    $statusCost[] = "budget_status='All'";
                    $statusCostPagination = array();
                    $statusCostPagination[] = 'Over Budget';
                    $statusCostPagination[] = 'On Budget';
                    $statusCostPagination[] = 'All';

                    break;
                }
                //add modelCost
                $statusCost[] = "budget_status='" . $formatArrayStatusCost . "'";
            }
            //generate query string
            $statusCost = implode(" OR ", $statusCost);
        }

        if ($request->has('modelCost')) {
            $arrayModelCost = $request->modelCost;
            $modelCostPagination = array();
            $modelCost = array();

            foreach ($arrayModelCost as $q) {
                $formatArrayModelCost = ucwords(strtolower($q));

                //handle modelCost All
                if ($request->has('statusCost')) {
                    if ($formatArrayModelCost == 'All') {
                        $modelCost = array();
                        $modelCost[] = "(charter_running_status='$running_status' AND model_cost='Bottom Up' AND $statusCost)"; // AND modelcost= Bottom Up, Kontrak Harga Satuan, All
                        $modelCost[] = "(charter_running_status='$running_status' AND model_cost='Kontrak Harga Satuan' AND $statusCost)";
                        $modelCost[] = "(charter_running_status='$running_status' AND model_cost='All' AND $statusCost)";
                        $modelCostPagination = array();
                        $modelCostPagination[] = 'lead';
                        $modelCostPagination[] = 'lag';
                        $modelCostPagination[] = 'delay';

                        break;
                    }

                    //add modelCost
                    $modelCost[] = "(charter_running_status='$running_status' AND status_proyek='" . $formatArrayModelCost . "' AND '$statusCost')";
                } else {
                    if ($formatArrayModelCost == 'All') {
                        $modelCost = array();
                        $modelCost[] = "(charter_running_status='$running_status' AND model_cost='Bottom Up' )"; // AND modelcost= Bottom Up, Kontrak Harga Satuan, All
                        $modelCost[] = "(charter_running_status='$running_status' AND model_cost='Kontrak Harga Satuan' )";
                        $modelCost[] = "(charter_running_status='$running_status' AND model_cost='All' )";
                        $modelCostPagination = array();
                        $modelCostPagination[] = 'lead';
                        $modelCostPagination[] = 'lag';
                        $modelCostPagination[] = 'delay';

                        break;
                    }

                    //add modelCost
                    $modelCost[] = "(charter_running_status='$running_status' AND status_proyek='" . $formatArrayModelCost . "')";
                }
            }
            //generate query string
            $modelCost = implode(" OR ", $modelCost);
        }
        // return $modelCost;



        $itemsPerPage = 5;
        if (empty($request->has('itemsPerPage'))) {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'itemsPerPage tidak boleh kosong');
            return $result;
        }
        $itemsPerPage = $request->itemsPerPage;
        if ($request->has('modelCost')) {
            if ($request->modelCost != '') {
                $data = DB::table('vw_dash_laporan_anggaran_proyek')
                    ->whereRaw($modelCost)
                    ->paginate($itemsPerPage);
            }
        } else {
            $data = DB::table('vw_dash_laporan_anggaran_proyek')
                ->whereRaw("charter_running_status='$running_status'")
                ->paginate($itemsPerPage);
        }

        if (!empty($data)) {
            if ($request->has('modelCost')) {
                $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('running_status', $running_status)->appends('modelCost', $modelCostPagination)->appends('statusCost', $statusCostPagination);
            } else {
                $data = $data->appends('itemsPerPage', $itemsPerPage)->appends('running_status', $running_status);
            }


            return $data;
        } else {
            $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function searchLaporanAnggaranProyek(Request $request)
    {
        $data = DB::table('vw_dash_laporan_anggaran_proyek');
        if (!empty($request->search)) {
            $data->whereRaw('LOWER(charter_running_status) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(proyek_name) LIKE ?', ['%' . $request->search . '%']);
        }
        if (!empty($data)) {
            return array('status' => true, 'kode' => 1, 'data' => $data->simplePaginate($request->perPage), 'pesan' => 'Data Ditemukan');
        } else {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }

    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }

    
}
