<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_dash_anggaran_proyek;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class VwLaporanIssueProyekController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function getLaporanAllIssueProyek(Request $request)
    {
        // $kategori = DB::table('risk_categories')->get();
        // // return  $kategori;
        // if (!empty($kategori)) {
        //     $data = array();
        //     // $dataArray['data'] = array();
        //     foreach ($kategori as $p) {
        //         if ($request->has('filterDateFrom') && $request->has('filterDateTo')) {
        //             if ($request->filterDateFrom != '' && $request->filterDateTo != '') {
        //                 $daystosum = '1';
        //                 $datesum =  date('d-m-Y', strtotime($request->filterDateTo . ' + ' . $daystosum . ' days'));
        //                 $response =   DB::table('vw_dash_laporan_issue_proyek')
        //                     ->where('issue_category', $p->name)
        //                     ->whereBetween('created_at', [$request->filterDateFrom, $datesum])
        //                     ->get();
        //             }
        //         } else {
        //             $response =  DB::table('vw_dash_laporan_issue_proyek')
        //                 ->where('issue_category', $p->name)
        //                 ->get();
        //         }
        //         $data[] = ['IssueCategory' => $p->name, 'totalProject' => count($response), 'dataProject' => $response];
        //     }
        //     $data = collect($data);
        //     $sorted = $data->sortBy('totalProyek')->reverse();
        //     $maxIssue = $sorted->values()->toArray();
        //     $result = array('status' => true, 'maxIssue' => $maxIssue[0]['totalProject'], 'kode' => 1, 'data' => $sorted->values()->toArray(), 'pesan' => 'Data Ditemukan');
        // } else {
        //     $result = array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        // }
        // return $result;
        if (empty($request->itemsPerPage)) {
            return  array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'param itemsPerPage tidak boleh kosong');
        }
        if (empty($request->page)) {
            return  array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'param page tidak boleh kosong');
        }

        if (!empty($request->filterDateFrom) && !empty($request->filterDateTo)) {

            $data = DB::select("select * from fn_dash_issue_project('$request->dateFrom', '$request->dateTo', '$request->search') l where lower(l.issuecategory) like '%$request->search%' or totalproject like '%$request->search%'");
        } else {
            $data = DB::select("select * from fn_dash_issue_project(null, null, '$request->search') l where lower(l.issuecategory) like '%$request->search%' or totalproject like '%$request->search%'");
        }
        $response = [];
        foreach ($data as $d) {
            if($d->totalproject > 0) {
                $response[] = [
                    'IssueCategory' => $d->issuecategory,
                    'totalProject' => $d->totalproject,
                    'dataProject' => json_decode($d->dataproject)
                ];
            }
        }

        $q = $this->_paginationArray($response, $request->itemsPerPage);

        if (!empty($q)) {
            return array('status' => true, 'kode' => 1, 'data' => $q, 'pesan' => 'Data Ditemukan');
        } else {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }

    public function searchIssueProyek(Request $request)
    {
        $data = DB::select("select * from issue_list('$request->dateFrom', '$request->dateTo', '$request->search') l where lower(l.issuecategory) like '%$request->search%' or totalproject like '%$request->search%'");

        $response = [];
        foreach ($data as $d) {
            $response[] = [
                'IssueCategory' => $d->issuecategory,
                'totalProject' => $d->totalproject,
                'dataProject' => json_decode($d->dataproject)
            ];
            //            return $d;
        }
        $q = $this->_paginationArray($response, $request->perPage);

        return $q;
    }

    private function _paginationArray($data, $perPage)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);

        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::defaultSimpleView($perPage),
        ]);
    }
}
