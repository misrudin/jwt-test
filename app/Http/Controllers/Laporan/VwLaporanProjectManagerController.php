<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\V_laporan_project_manager;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class VwLaporanProjectManagerController extends Controller
{
    public function index(Request $request, V_laporan_project_manager $v_laporan_project_manager)
    {
        if ($request->search) {

            $search = strtolower($request->search);
            $data = $v_laporan_project_manager->whereRaw('LOWER(name) LIKE ? OR LOWER(code) LIKE ?', ['%' . $search . '%', '%' . $search . '%'])->simplePaginate();
        } else {
            $data = $v_laporan_project_manager->simplePaginate();
        }

        $user = $data->appends(['level' => null]);
        if ($data->isEmpty()) {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        } else {
            return array('status' => true, 'kode' => 1, 'data' => $user, 'pesan' => 'Data Ditemukan');
        }
    }

    public function getProfileProjectManager(Request $request)
    {
        if (empty($request->project_manager_code))  return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Param tidak boleh kosong');
        $project = DB::table('vw_dash_laporan_project_manager_profile')->where('code', $request->project_manager_code)->get();
        $profile = DB::table('vw_dash_laporan_project_manager_project')->where('code', $request->project_manager_code)->get();

        if (empty($profile[0]) || empty($project[0])) {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        } else {
            return array('status' => true, 'kode' => 1, 'data' => ['profile' => $profile[0], 'project' => $project[0]], 'pesan' => 'Data Ditemukan');
        }
    }

    public function getProjectDetailProjectManager(Request $request)
    {
        if (empty($request->project_manager_code) || empty($request->type)) return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Param tidak boleh kosong');
        $type = strtolower($request->type);
        if (empty($type)) {
            $project_detail = DB::table('vw_dash_laporan_project_manager_detail_project')->where(['charter_type_group_name' => 'APPLICATION', 'stakeholder_code' => $request->project_manager_code])->get();
        } else if (strtolower($type) == "connectivity") {
            $project_detail = DB::table('vw_dash_laporan_project_manager_detail_project')->where(['charter_type_group_name' => 'CONNECTIVITY', 'stakeholder_code' => $request->project_manager_code])->get();
        } else if (strtolower($type) == "cpe&other") {
            $project_detail = DB::table('vw_dash_laporan_project_manager_detail_project')->where(['charter_type_group_name' => 'CPE&OTHER', 'stakeholder_code' => $request->project_manager_code])->get();
        } else if (strtolower($type) == "smart_building") {
            $project_detail = DB::table('vw_dash_laporan_project_manager_detail_project')->where(['charter_type_group_name' => 'SMART_BUILDING', 'stakeholder_code' => $request->project_manager_code])->get();
        }

        if (empty($project_detail)) {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        } else {
            return array('status' => true, 'kode' => 1, 'data' => $project_detail, 'pesan' => 'Data Ditemukan');
        }
    }

    public function getCreditPoint(Request  $request)
    {
        $data = [
            'level' => 108,
            'credit_point' => 108,
            'credit_point_this_week' => 22,
            'cummulative_point' => [
                'update_issue' => 20,
                'update_action_plan' => 20,
                'update_deliverables' => 10,
                'add_document' => 20,
                'update_project_information' => 20,
                'update_top' => 20
            ],
            'credit_point_history' => [
                'date_event' => date('Y-m-d'),
                'task_activity' => 'aaa',
                'point' => 1
            ]
        ];
        return array('status' => true, 'kode' => 1, 'data' => $data, 'pesan' => 'Data Ditemukan');
    }

    public function searchProjectManager(Request $request)
    {
        $data = DB::table('vw_dash_laporan_project_manager');
        if (!empty($request->search)) {
            $data->whereRaw('LOWER(name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(code) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(jabatan) LIKE ?', ['%' . $request->search . '%'])
                ->orWhere('level', 'LIKE', ['%' . $request->search . '%']);
        }
        if (!empty($data)) {
            return array('status' => true, 'kode' => 1, 'data' => $data->simplePaginate($request->perPage), 'pesan' => 'Data Ditemukan');
        } else {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }

    public function searchProjectManagerProfile(Request $request)
    {
        $project = DB::table('vw_dash_laporan_project_manager_profile');
        $profile = DB::table('vw_dash_laporan_project_manager_project');
        if (!empty($request->search)) {
            $profile->whereRaw('LOWER(jabatan) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(code) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(username) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(email) LIKE ?', ['%' . $request->search . '%'])
                ->orWhere('mobile', 'LIKE', ['%' . $request->search . '%'])
                ->orWhere('nik', 'LIKE', ['%' . $request->search . '%'])
                ->orWhere('level', 'LIKE', ['%' . $request->search . '%']);
            $project->whereRaw('LOWER(code) LIKE ?', ['%' . $request->search . '%']);
        }

        if (!empty($profile) || !empty($project)) {
            return array('status' => true, 'kode' => 1, 'data' => ['profile' => $profile->get(), 'project' => $project->get()], 'pesan' => 'Data Ditemukan');
        } else {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }

    public function searchProjectManagerProjectDetail(Request $request)
    {

        $data = DB::table('vw_dash_laporan_project_manager_detail_project');
        if (!empty($request->search)) {
            $data->whereRaw('LOWER(stakeholder_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(stakeholder_code) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(charter_type_group_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(project_code) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(partner_name) LIKE ?', ['%' . $request->search . '%'])
                ->orWhereRaw('LOWER(type_partner) LIKE ?', ['%' . $request->search . '%']);
        }
        if (!empty($data)) {
            return array('status' => true, 'kode' => 1, 'data' => $data->simplePaginate($request->perPage), 'pesan' => 'Data Ditemukan');
        } else {
            return array('status' => false, 'kode' => 2, 'data' => [], 'pesan' => 'Data Tidak Ditemukan');
        }
    }
}
