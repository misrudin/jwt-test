<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class VwAnggaranProyekGeneralController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }


            /**
     * @OA\GET(
     *     path="/api/v1/anggaranProyek/getCountAllAnggaranProyekGeneral",
     *     tags={"Anggaran Proyek"},
     *     operationId="getCountAllAnggaranProyekGeneral",
        *      @OA\Parameter(
        *          name="status",
        *          description="status (Draft,Kandidat, Aktif, Tech Closed, Closed, Batal,  )",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="filterDateFrom",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
                *      @OA\Parameter(
        *          name="filterDateTo",
        *          description="format date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function getCountAllAnggaranProyekGeneral(Request $request){  
        if (empty($request->has('status'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter status tidak boleh kosong');
            return $result;
        }
        $status=ucfirst(strtolower($request->status));
        // return  array('status'=>true,'kode'=>1,'data'=>$request->status,'pesan'=>'Data Ditemukan');

        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $data =  DB::select(DB::raw("SELECT * FROM vw_anggaran_proyek_generals WHERE 
                (charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));
            }
        }else{
            $data =  DB::select(DB::raw("SELECT * FROM vw_anggaran_proyek_generals WHERE charter_running_status = '$status'"));
        }

        // return $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');


        if (!empty($data) ){
            
            $totalNilaiProyek=0;
            $penyerapanAnggaran=0;
            $rencanaAnggaran=0;
            $realisasiAnggaran=0;
            $sisaAnggaran=0;
            foreach ($data as $p) {
               $totalNilaiProyek = $totalNilaiProyek+(int)$p->nilai_proyek;
               $rencanaAnggaran = $rencanaAnggaran+(int)$p->rencana_anggaran;
               $realisasiAnggaran = $realisasiAnggaran+(int)$p->realisasi_anggaran;
               $sisaAnggaran = $sisaAnggaran+(int)$p->sisa_anggaran;
                }
                if($realisasiAnggaran !== 0 && $rencanaAnggaran !== 0 ){
                    $penyerapanAnggaran= ( $realisasiAnggaran *100 /$rencanaAnggaran);
                }
            // $penyerapanAnggaran= ( $realisasiAnggaran *100 /$rencanaAnggaran);

            // $response = [
            //     "totalNilaiProyek" => $totalNilaiProyek,
            //     "rencanaAnggaran " => $rencanaAnggaran,
            //     "penyerapanAnggaran" => $penyerapanAnggaran,
            //     "realisasiAnggaran" => $realisasiAnggaran
            // ];
            $response = array();
            $response[] = ['value' =>  $totalNilaiProyek,'type' =>  'totalNilaiProyek'];
            $response[] = ['value' => $rencanaAnggaran,'type' =>  'rencanaAnggaran'];
            $response[] = ['value' =>  $penyerapanAnggaran,'type' =>  'penyerapanAnggaran'];
            $response[] = ['value' =>  $realisasiAnggaran,'type' =>  'realisasiAnggaran'];
            $response[] = ['value' =>  $sisaAnggaran,'type' =>  'sisaAnggaran'];
            $result = array('status'=>true,'kode'=>1,'data'=>$response,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }
}
