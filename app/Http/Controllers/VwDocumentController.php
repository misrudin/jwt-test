<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\V_laporan_direktori_document;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DB;
/**
 * @OA\Info(title="Tomps Api documentation", version="0.1")
 */
class VwDocumentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }
//    /**
//      * @OA\GET(
//      *     path="/api/v1/dokument/listDirektoriDocument/",
//      *     tags={"List Direktory"},
//      *     operationId="listDirektoriDocument",
//      *     @OA\Response(
//      *         response="default",
//      *         description="successful operation"
//      *     )
//      * )
//      */
        public function listDirektoriDocument(){
            $data = V_laporan_direktori_document::All();
            if (!empty($data)){
                $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
            }else{
                $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
            }
            return $result;
        }


    //    /**
    //  * @OA\GET(
    //  *     path="/api/v1/dokument/listDirektoriDocumentByHirarki/",
    //  *     tags={"List Direktory"},
    //  *     operationId="listDirektoriDocumentByHirarki",
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */
    
    public function listDirektoriDocumentByHirarki(Request $request){
        $data = V_laporan_direktori_document::Select('*')->get();
        // $data = V_laporan_direktori_document::select('*');
        // $data = $data->paginate();
        foreach($data as $value){
            $getPath[]=$value->Path.'/'.$value->filename.','.$value->file_code;
        }
        $getHirarki=$this->parseArrayToHirarki($getPath);
        // return json_encode($getHirarki);
        return array("info"=>"Succes","data_hirarki"=>json_encode($getHirarki));
    
    }

    private function parseArrayToHirarki($paths){
        sort($paths);
        $array = array();
        foreach ($paths as $path)
        {
        $path = trim($path, '/');
        $list = explode('/', $path);
        $n = count($list);
        $arrayRef = &$array; // start from the root
            for ($i = 0; $i < $n; $i++)
            {
                $key = $list[$i];
                $arrayRef = &$arrayRef[$key]; // index into the next level
            }
        }
        // return $array;
        $dataArray = array();
        $dataArray['data'] = array();
        $dataArray['data'] = $this->buildHirarki($array, '');
        return $dataArray;
    }

    private function buildHirarki($array,$prefix){
        $finalArray= array();
        foreach ($array as $key => $value){
            $levelArray = array();
            $path_parts = pathinfo($key);
            if (!empty($path_parts['extension']) && $path_parts['extension'] != ''){
                $extension = $path_parts['extension'];
            }
            else{
                if (empty($value))
                    {
                        $extension = "";
                    }
                else if (is_array($value))
                    {
                        $extension = 'folder';
                    }
            }

            if (is_array($value)){ //its a folder
                $levelArray['data'] = $key;
            }
            else
            { //its a file
                $keyParsing=explode(',',$key);
                $levelArray['data']['title'] = $keyParsing[0];
                $levelArray['data']['code'] = $keyParsing[1];
                $getKeyParsing=V_laporan_direktori_document::where('file_code',$keyParsing[1])->first();
                if (Isset($getKeyParsing->url_document)){
                    $levelArray['data']['url'] = $getKeyParsing->url_document;
                }else{
                    $levelArray['data']['url'] ="";
                }
                if (Isset($getKeyParsing->tanggal_upload)){
                    $levelArray['data']['tanggal_upload'] = $getKeyParsing->tanggal_upload;
                }else{
                    $levelArray['data']['tanggal_upload'] = "";
                }       
            }

        // if the value is another array, recursively build the list$key
        if (is_array($value))
        {
            $levelArray['children'] = $this->buildHirarki($value, $prefix . $key . "/");
        }

      $finalArray[] = $levelArray;
    } //end foreach

    return $finalArray;
    }


    //     /**
    //  * @OA\GET(
    //  *     path="/api/v1/dokument/searchListDirektoriDocument/",
    //  *     tags={"List Direktory"},
    //  *     operationId="searchListDirektoriDocument",
    //  *     @OA\Response(
    //  *         response="default",
    //  *         description="successful operation"
    //  *     )
    //  * )
    //  */
    public function searchListDirektoriDocument(Request $request){
        $project_file = new V_laporan_direktori_document();
        $project_file = V_laporan_direktori_document::Select('vw_laporan_document_direktori.*');
        if($request->has('filterFileName')){
            if($request->filterFileName != ''){
                $project_file->where("vw_laporan_document_direktori.filename","like","%{$request->filterFileName}%");
            }
        }
        if($request->has('date_from')){
            if($request->date_from != ''){
                $project_file->where("vw_laporan_document_direktori.tanggal_upload",">=","%{$request->date_from}%");
            }
        }
        if($request->has('date_to')){
            if($request->date_to != ''){
                $project_file->where("vw_laporan_document_direktori.tanggal_upload","<","%{$request->date_to}%");
            }
        }
        // return $project_file=$project_file->toSql();
        
        $project_file=$project_file->get();

        if(!empty($project_file)){
            $result = array('status'=>true,'kode'=>1,'data'=>$project_file,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }

        return $result;
    }


   

}