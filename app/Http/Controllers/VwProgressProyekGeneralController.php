<?php

namespace App\Http\Controllers;

use App\Models\VwProgressProyekGeneral;
use Illuminate\Http\Request;
use DB;

class VwProgressProyekGeneralController extends Controller
{
 
    public function listProgressProyekStatusGeneral(){
        $data = DB::table('charter_running_statuses')->get();
        if (!empty($data)){
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
        }else{
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Data Tidak Ditemukan');
        }
        return $result;
    }

    public function getCountAllProgressGeneral(Request $request){  
        if (empty($request->has('status'))){
            $result = array('status'=>false,'kode'=>2,'data'=>[],'pesan'=>'Parameter status tidak boleh kosong');
            return $result;
        }
        $status=ucfirst(strtolower($request->status));
        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $lead =  DB::select(DB::raw("SELECT * FROM vw_progress_proyek_generals WHERE 
                (lead = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (lead = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lead = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lead = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));                
                
                $lag =  DB::select(DB::raw("SELECT * FROM vw_progress_proyek_generals WHERE 
                (lag = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (lag = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lag = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lag = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));

                $delay =  DB::select(DB::raw("SELECT * FROM vw_progress_proyek_generals WHERE 
                (delay = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (delay = true AND charter_running_status = '$status' AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (delay = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (delay = true AND charter_running_status = '$status' AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                "));
                // return  count($lag);
                
            }
        }else{
            $lead =  DB::select(DB::raw("SELECT * FROM vw_progress_proyek_generals WHERE lead = true AND charter_running_status = '$status'"));
            $lag =  DB::select(DB::raw("SELECT * FROM vw_progress_proyek_generals WHERE lag = true AND charter_running_status = '$status'"));
            $delay =  DB::select(DB::raw("SELECT * FROM vw_progress_proyek_generals WHERE delay = true AND charter_running_status = '$status'"));
        }
 
            $data = [
                count($lead),
                count($lag),
                count($delay),
            ];
            $result = array('status'=>true,'kode'=>1,'data'=>$data,'pesan'=>'Data Ditemukan');
            return $result;
  
    }

    public function getGrafikAllProgressProyekGeneral(Request $request){  
        $table = new VwProgressProyekGeneral();
        $result = array('status'=>true,'kode'=>1,'data'=>$table->get(),'pesan'=>'Data Ditemukan');

        if($request->has('filterDateFrom') && $request->has('filterDateTo')){
            if($request->filterDateFrom != '' && $request->filterDateTo != '' ){
                $lead =  DB::select(DB::raw("SELECT * FROM vw_dash_grafikk_progress_general WHERE 
                (lead = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (lead = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lead = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lead = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));                
                
                $lag =  DB::select(DB::raw("SELECT * FROM vw_dash_grafikk_progress_general WHERE 
                (lag = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (lag = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lag = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (lag = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                 "));

                $delay =  DB::select(DB::raw("SELECT * FROM vw_dash_grafikk_progress_general WHERE 
                (delay = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateTo')) OR
                (delay = true AND  planed_start_at <= Date('$request->filterDateFrom') AND planed_end_at >= Date('$request->filterDateFrom')AND planed_end_at <= Date('$request->filterDateTo')) OR
                (delay = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at <= Date('$request->filterDateTo')) OR
                (delay = true AND  planed_start_at >= Date('$request->filterDateFrom') AND  planed_start_at <= Date('$request->filterDateTo') AND planed_end_at >= Date('$request->filterDateTo'))
                "));
                
            }
        }else{
            $lead =  DB::select(DB::raw("SELECT * FROM vw_dash_grafikk_progress_general WHERE lead = true"));
            $lag =  DB::select(DB::raw("SELECT * FROM vw_dash_grafikk_progress_general WHERE lag = true"));
            $delay =  DB::select(DB::raw("SELECT * FROM vw_dash_grafikk_progress_general WHERE delay = true"));
        $lead = $table
                ->where('lead',true)
                ->get();
        $lag =  $table
                ->where('lag',true)
                ->get();
        $delay = $table
                ->where('delay',true)
                ->get();
        }

            $data = array();
            $dataLead = array();
            $dataLag = array();
            $dataDelay = array();
            foreach ($lead as $q) {
                $dataLead[] = ['x' =>(int)$q->nilai_proyek, 'y' =>(int)$q->progress  , 'company' =>$q->company, 'project' => $q->project,'pm' => $q->stakeholder];
            }
            foreach ($lag as $q) {
                $dataLag[] = ['x' =>(int)$q->nilai_proyek, 'y' =>(int)$q->progress, 'company' =>$q->company, 'project' => $q->project,'pm' => $q->stakeholder];
            }
            foreach ($delay as $q) {
                $dataDelay[] = ['x' =>(int)$q->nilai_proyek,'y' =>(int)$q->progress, 'company' =>$q->company, 'project' => $q->project,'pm' => $q->stakeholder];
            }
            $data[] = ['name' => 'lead',  'data' => $dataLead];
            $data[] = ['name' => 'lag', 'data' => $dataLag];
            $data[] = ['name' => 'delay', 'data' => $dataDelay];
            $result = array('status'=>true,'kode'=>1,'totalData'=>count($dataLead)+count($dataLag)+count($dataDelay), 'data'=>$data, 'pesan'=>'Data Ditemukan');
     
        return $result;
    }

}
