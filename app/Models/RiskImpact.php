<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiskImpact extends Model
{
    use HasFactory, softDeletes;
    protected $fillable = [];
    protected $keyType = 'string';
    public $incrementing = false;
}
