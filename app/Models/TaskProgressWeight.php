<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskProgressWeight extends Model
{
    protected $fillable = ['id', 'type', 'charter_id', 'task_id', 'start_date', 'end_date', 'value', 'updated_by', 'actual_cost'];
    protected $keyType = 'string';
    public $incrementing = false;

    public function user() {
        return $this->belongsTo(UserTest::class, "updated_by")->select("id", "name");
    }
}
