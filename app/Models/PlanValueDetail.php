<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanValueDetail extends Model
{
    use HasFactory;
    public $table = 'plan_value_detail';
    protected $fillable = ['id', 'plan_value_id', 'audit_date', 'plan_value_percent', 'plan_value_bobot', 'earn_value_id'];
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

}
