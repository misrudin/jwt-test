<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = ['id', 'source', 'target', 'type', 'project_id'];
    public $incrementing = false;
}
