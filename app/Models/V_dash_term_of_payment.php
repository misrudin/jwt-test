<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class V_dash_term_of_payment extends Model
{
    use HasFactory;
    protected $table = 'vw_dash_term_of_payment';
    protected $keyType = 'string';
    public $incrementing = false;

}