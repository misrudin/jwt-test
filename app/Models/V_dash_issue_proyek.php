<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class V_dash_issue_proyek extends Model
{
    use HasFactory;
    protected $table = 'vw_dash_issue_proyek';
    protected $keyType = 'string';
    public $incrementing = false;

}