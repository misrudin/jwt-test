<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DashboardGeneral extends Model
{
    use HasFactory;
    protected $table = 'vw_dash_status_proyek_general';
    protected $keyType = 'string';
    public $incrementing = false;
}
