<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VwAnggaranProyekGeneral extends Model
{
    use HasFactory;
    protected $table = 'vw_anggaran_proyek_generals';
    protected $keyType = 'string';
    public $incrementing = false;
}
