<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VwDashboardTopGeneral extends Model
{
    use HasFactory;
    protected $table = 'vw_dashboard_top_generals';
    protected $keyType = 'string';
    public $incrementing = false;
}
