<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class V_dash_charter_all extends Model
{
    use HasFactory;
    protected $table = 'vw_dash_charter_all';
    protected $keyType = 'string';
    public $incrementing = false;

}