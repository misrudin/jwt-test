<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class V_laporan_project_segment extends Model
{
    use HasFactory;
    protected $table = 'laporan_vw_project_segment';
    protected $keyType = 'string';
    public $incrementing = false;

}