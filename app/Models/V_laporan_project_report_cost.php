<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class V_laporan_project_report_cost extends Model
{
    use HasFactory;
    protected $table = 'vw_project_repost_cost';
    protected $keyType = 'string';
    public $incrementing = false;

}