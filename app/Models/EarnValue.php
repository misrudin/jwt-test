<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EarnValue extends Model
{


    use HasFactory;
    protected $casts = [
        'id' => 'string'
    ];
    protected $table = 'earn_value';
    protected $fillable = ['id', 'task_id', 'budget_at_completion', 'target_periode_end'];
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';
}
