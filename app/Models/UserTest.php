<?php

namespace App\Models;


use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTest extends Model
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $connection = "core";
    protected $fillable = [];
    protected $hidden = [ 'password', 'remember_token' ];
    protected $keyType = 'string';

    public $incrementing = false;

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }
}
