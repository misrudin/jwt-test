<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class V_laporan_direktori_document extends Model
{
    use HasFactory;
    protected $table = 'vw_laporan_document_direktori';
    protected $keyType = 'string';
    protected $fillable = ['file_id','file_code','Path','filename','format','tanggal_upload','url_document','Category'];
    public $incrementing = false;

}