<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VwIssueProyekGeneral extends Model
{
    use HasFactory;
    protected $table = 'vw_issue_proyek_generals';
    protected $keyType = 'string';
    public $incrementing = false;
}
