<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // protected $connection = "tenant";
    protected $fillable = [
        'id', 'gantt_id', 'start_date', 'end_date', 'type', 'duration',
        'order_num', 'text', 'progress', 'parent', 'planned_start',
        'charter_id', 'planned_end', 'owner', 'bobot', 'customs',
        'is_approval', 'isNotify', 'notify_start_date', 'notify_end_date',
        'notify_remarks', 'notify_status'
    ];
    protected $keyType = 'string';
    protected $appends = ["tot_hst_weight", "tot_hst_progress", "lst_hst_weight_date", "lst_hst_progress_date", "p_start", "p_end"];
    public $incrementing = false;

    public function approval() {
        return $this->hasOne(Approval::class, "correlation_id", "id")
            ->where("service", "TASKS");
    }

    public function histories() {
        return $this->hasMany(HistoryTask::class, "task_id", "id")
            ->orderBy("updated_at", "desc");
    }

    public function owner() {
        return $this->belongsTo(Partner::class, 'owner', "code");
    }

    public function owner2() {
        return $this->belongsTo(Partner::class, 'owner', "code");
    }

    public function files() {
        return $this->hasMany(ProjectFile::class, "DeliverablesID", "id");
    }

    public function charterCost() {
        return $this->hasMany(CharterCost::class, "task_id", "id");
    }

    public function charterCostKhs() {
        return $this->hasMany(CharterCostKhsTrans::class, "task_id", "id");
    }

    public function hst_progress() {
        return $this->hasMany(TaskProgressWeight::class)
            ->select("id", "type", "value", "task_id", "start_date", "end_date", "updated_at", "duration")
            ->where("type", "real")
            ->orderBy("end_date", "desc");
    }

    public function hst_weights() {
        return $this->hasMany(TaskProgressWeight::class)
            ->select("id", "type", "value", "task_id", "start_date", "end_date", "updated_at", "duration")
            ->where("type", "plan")
            ->orderBy("end_date", "desc");
    }

    public function getTotHstProgressAttribute() {
        $total = TaskProgressWeight::select(\DB::raw("SUM(value)"))
            ->where(["task_id" => $this->id, "type" => "real"])
            ->groupBy("task_id")
            ->first();

        return $total ? $total->sum : 0;
    }

    public function getTotHstWeightAttribute() {
        $total = TaskProgressWeight::select(\DB::raw("SUM(value)"))
            ->where(["task_id" => $this->id, "type" => "plan"])
            ->groupBy("task_id")
            ->first();

        return $total ? $total->sum : 0;
    }

    public function getLstHstWeightDateAttribute() {
        $lastData = $this->hst_weights->first();

        return $lastData ? $lastData->end_date : null;
    }

    public function getLstHstProgressDateAttribute() {
        $lastData = $this->hst_progress->first();

        return $lastData ? $lastData->end_date : null;
    }

    public function getProgressAttribute($val) {
        return $val / 100;
    }

    public function getPStartAttribute() {
        return $this->planned_start;
    }

    public function getPEndAttribute() {
        return $this->planned_end;
    }

    public function getCustomsAttribute($val) {
        $customs = json_decode($val)[0];

        $nData = [];
        foreach ($customs as $key => $value) {
            $nData[] = [
                "name" => $key,
                "value" => $value,
            ];
        }

        return $nData;
    }
}
