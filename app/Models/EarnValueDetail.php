<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class EarnValueDetail extends Model
{
    use Uuids;
    use HasFactory;
    protected $table = 'earn_value_detail';
    protected $fillable = ['id', 'earn_value_id', 'earn_value_percent', 'earn_value_bobot', 'audit_date', 'actual_cost'];
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid()->toString();
            }
        });
    }
}
