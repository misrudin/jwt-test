<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VwSumberAnggaranGeneral extends Model
{
    use HasFactory;
    protected $table = 'vw_sumber_anggaran_generals';
    protected $keyType = 'string';
    public $incrementing = false;
}
