<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class V_dash_partner_payment_charters extends Model
{
    use HasFactory;
    protected $table = 'vw_dash_partner_payments_charters';
    protected $keyType = 'string';
    public $incrementing = false;

}