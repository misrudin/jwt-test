<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiskRegister extends Model
{
    use HasFactory, softDeletes;
    protected $fillable = [];
    protected $keyType = 'string';
    public $incrementing = false;

//    public function category() {
//        return $this->belongsTo(RiskCategory::class, "risk_category_code", "code")->select("code", "name");
//    }

    public function impact() {
        return $this->belongsTo(RiskImpact::class, "risk_impact_code", "code")->select("code", "name");
    }

}
