<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryVideo extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function video()
    {
        return $this->hasMany(VideoTutorial::class,'category_video_id','id');
    }
}
