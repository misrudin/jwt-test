<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class V_laporan_project_manager extends Model
{
    use HasFactory;
    protected $table = "vw_dash_laporan_project_manager";
    public $increment = false;
}
