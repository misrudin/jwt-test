<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class V_dash_stakeholder_partner_customer extends Model
{
    use HasFactory;
    protected $table = 'vw_dash_stakeholder_partner_customer';
    protected $keyType = 'string';
    public $incrementing = false;

}