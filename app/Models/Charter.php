<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ {
    Factories\HasFactory,
    Model,
    SoftDeletes
};

class Charter extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [];
    protected $keyType = 'string';
    public $incrementing = false;

    public function tasks() {
        return $this->hasMany(Task::class, "charter_id", "id")
            ->select("id", "start_date", "duration", "text", "progress", "charter_id", "bobot")
            ->orderBy("start_date");
    }
}
