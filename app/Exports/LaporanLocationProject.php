<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LaporanLocationProject implements FromCollection, WithHeadings, ShouldAutoSize
{
   
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    
    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'Provinsi',
            'Total Proyek',
            'Total Nilai',
            'Isu'
        ];
    }
}
