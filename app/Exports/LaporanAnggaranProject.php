<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LaporanAnggaranProject implements FromCollection,WithHeadings,ShouldAutoSize
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    
    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'Nama Proyek',
            'Model Cost',
            'Total Nilai Proyek',
            'Rencana Anggaran',
            'Tahapan Berlangsung',
            'Tahapan Belum Berlangsung',
            'Persentase',
            'Realisasi Anggaran',
            'Selisih Anggaran',
            'Status Anggaran'
        ];
    }
}
