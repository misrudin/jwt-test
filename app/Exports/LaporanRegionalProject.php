<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LaporanRegionalProject implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    
    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'ID LOP',
            'Kode Proyek',
            'Nama Proyek',
            'Regional',
            'PIC',
            'Progress',
            'Status Proyek'
        ];
    }
}
