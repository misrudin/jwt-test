<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LaporanStakeholderPartner implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    
    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'Stakeholder Name',
            'Type',
            'Total Project',
            'Total Realisasi',
            'Sisa Pembayaran',
            'Total Proyek',
            'Dokumen'
        ];
    }
}
