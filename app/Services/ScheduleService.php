<?php

namespace App\Services;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\ {
  Task,
  Link,
};

class ScheduleService
{
  public function show($p_id) {
    try {
      $data = [
          "data" => Task::where('project_id', $p_id)->get(),
          "links" => Link::where('project_id', $p_id)->get()
      ];

      return $data;
        
    } catch (\Throwable $th) {
      throw new \Exception($th->getMessage());
    }
  }

  public function store($p_id, Request $request) {
    try {
      # remap Task Data
      $newTask = collect($request->data)
          ->filter(function($task) {
              return empty($task['!nativeeditor_status']) || $task['!nativeeditor_status'] != 'deleted';
          })->values()
          ->map(function($task) use($p_id) {
              return [
                  # check if task is new and add project id at the first
                  "id" => (count(explode("-", $task['id'])) == 1) ? "T$p_id-".$task['id'] : $task['id'],
                  "start_date" => Carbon::parse($task['start_date'])->timezone("Asia/Jakarta"),
                  "end_date" => Carbon::parse($task['end_date'])->timezone("Asia/Jakarta"),
                  "type" => $task['type'],
                  "duration" => $task['duration'],
                  "text" => $task['text'],
                  "progress" => $task['progress'],
                  # check if parent is new task and add project id at the first
                  "parent" => ($task['parent'] != 0 && count(explode("-", $task['parent'])) == 1) ? "T$p_id-".$task['parent'] : $task['parent'],
                  "project_id" => $p_id,
                  "planned_start" => !empty($task['planned_start']) ? Carbon::parse($task['planned_start'])->timezone("Asia/Jakarta") : null,
                  "planned_end" => !empty($task['planned_end']) ? Carbon::parse($task['planned_end'])->timezone("Asia/Jakarta") : null,
                  "owner" => $task['owner'] ?? null,
                  "created_at" => Carbon::now(),
                  "updated_at" => Carbon::now()
              ];
          })->toArray();

      # remap link data
      $newLink = collect($request->links)
          ->filter(function($link) {
              return empty($link['!nativeeditor_status']) || $link['!nativeeditor_status'] != 'deleted';
          })->values()
          ->map(function($link) use($p_id) {
              return [
                  # check if link is new and add project id at the first
                  "id" => (count(explode("-", $link['id'])) == 1) ? "L$p_id-".$link['id'] : $link['id'],
                  # check if source is new task and add project id at the first
                  "source" => (count(explode("-", $link['source'])) == 1) ? "T$p_id-".$link['source'] : $link['source'],
                  # check if target is new task and add project id at the first
                  "target" => (count(explode("-", $link['target'])) == 1) ? "T$p_id-".$link['target'] : $link['target'],
                  "type" => $link['type'],
                  "project_id" => $p_id,
                  "created_at" => \Carbon\Carbon::now(),
                  "updated_at" => \Carbon\Carbon::now()
              ];
          })->toArray();

      # delete task & link
      Task::where('project_id', $p_id)->delete();
      Link::where('project_id', $p_id)->delete();

      # insert new task & link
      Task::insert($newTask);
      Link::insert($newLink);

      return null;

    } catch (\Throwable $th) {
      throw new \Exception($th->getMessage());
    }
  }
}
