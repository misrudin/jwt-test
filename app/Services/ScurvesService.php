<?php

namespace App\Services;

use App\Models\Charter;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ScurvesService
{
    private $labelOpt = [
        "daily" => "Y-m-d",
        "weekly" => "#W Y",
        "monthly" => "F Y",
        "annually" => "Y"
    ];
    private $dateFormat, $prefix = null;

    public function sCurves(Request $request)
    {

        try {

            $cek_data = DB::table('charters')->where('id', $request->charter_id)->first();

            $finData = [];

            $this->dateFormat = $this->labelOpt[$request->input("scale")];
            $this->prefix = $this->dateFormat === "#W Y" ? "Week " : "";

            $response = DB::table('vw_scurves_chart')->where('id', $request->charter_id)->get();
            $data = $response[0];
            $data->tasks = json_decode($data->tasks);

            $tempTask["real"] = [];
            $tempTask["plan"] = [];

            foreach ($data->tasks as $kTask => $task) {
                $tempTask["real"] = collect($tempTask["real"])->merge($task->hst_progress)->sortBy("start_date")->values()->toArray();
                $tempTask["plan"] = collect($tempTask["plan"])->merge($task->hst_weights)->sortBy("start_date")->values()->toArray();
            }

            $data->tasks = $tempTask;

            foreach ($data->tasks as $kType => $type) {
                $currMaxVal = 0;

                foreach ($type as $kHistory => $history) {
                    $currMaxVal += (int)$history->value;

                    $data->tasks[$kType][$kHistory]->calculated_value = $currMaxVal;
                }
            }

            $finData["real"] = [];
            $finData["plan"] = [];
            $finData["label"] = [];

            // merge, group dan sort
            $additionals = json_decode($data->additionals, true)[0];
            $pembobotan = $additionals->pembobotan ?? null;

            if ($pembobotan) {
                $pembobotanTaskReal = collect($data->tasks["real"])->map(function ($task) use ($pembobotan) {
                    $nData = $task;
                    $nData->value = ($pembobotan / 100) * $task->value;

                    return $nData;
                })->toArray();

                $pembobotanTaskPlan = collect($data->tasks["plan"])->map(function ($task) use ($pembobotan) {
                    $nData = $task;
                    $nData->value = ($pembobotan / 100) * $task->value;

                    return $nData;
                })->toArray();

                $finData["real"] = collect($finData["real"])->merge($pembobotanTaskReal)->toArray();
                $finData["plan"] = collect($finData["plan"])->merge($pembobotanTaskPlan)->toArray();
            } else {
                $finData["real"] = collect($finData["real"])->merge($data->tasks["real"])->toArray();
                $finData["plan"] = collect($finData["plan"])->merge($data->tasks["plan"])->toArray();
            }

            $finData["real"] = collect($finData["real"])->sortBy("start_date")->values()->toArray();
            $finData["plan"] = collect($finData["plan"])->sortBy("start_date")->values()->toArray();

            foreach ($finData as $kType => $type) {
                $currMaxVal = 0;

                foreach ($type as $kHistory => $history) {
                    $currMaxVal += $history->value;

                    $finData[$kType][$kHistory]->calculated_value = $currMaxVal;
                }
            }

            // data projek
            $mergeAllData = collect($finData["plan"])->merge($finData["real"]);

            // check if there's any data history
            if ($mergeAllData->count() > 0) {
                // create raw kurva s data
                foreach ($finData as $tKey => $type) {
                    $finData[$tKey] = $this->getComulativeValue(true, $type);
                }
                // create label data
                $minDate = Carbon::createMidnightDate($mergeAllData->sortBy("start_date")->first()->start_date);
                $maxDate = Carbon::createMidnightDate($mergeAllData->sortByDesc("end_date")->first()->end_date);
                $totalLabel = $minDate->diffInDays($maxDate);


//                return $totalYear;

                for ($i = 0; $i < $totalLabel + 1; $i++) {
                    $newLabel = $this->prefix . Carbon::create($minDate)->addDays($i)->format($this->dateFormat);

                    if ($i > 0) {
                        if (!collect($finData["label"])->first(function ($lbl) use ($newLabel) {
                            return $lbl === $newLabel;
                        })) {
                            $finData["label"][] = $newLabel;
                        }
                    } else {
                        $finData["label"][] = $newLabel;
                    }
                }

                // maping kurva data based on label
                $lIterate = 0;
                $nPlan = collect([]);
                $nReal = collect([]);
                $totalPlan = count($finData["plan"]);
                $totalReal = count($finData["real"]);
                $planLastValue = 0;
                $realLastvalue = 0;

                foreach ($finData["label"] as $lKey => $label) {
                    $lIterate++;

                    if (isset($finData["plan"][$label])) {
                        $planLastValue = $finData["plan"][$label];
                        $nPlan->push($planLastValue);
                    } else if ($lIterate <= $totalPlan) {
                        $nPlan->push($planLastValue);
                    }

                    if (isset($finData["real"][$label])) {
                        $realLastvalue = $finData["real"][$label];
                        $nReal->push($realLastvalue);
                    } else if ($lIterate <= $totalReal) {
                        $nReal->push($realLastvalue);
                    }
                }

                $finData["plan"] = $nPlan;
                $finData["real"] = $nReal;

                // filter unused label
                $longestData = count($nPlan) > count($nReal) ? count($nPlan) : count($nReal);
                if ($longestData < count($finData["label"])) {
                    $finData["label"] = collect($finData["label"])->filter(function ($lbl, $key) use ($longestData) {
                        return $key < $longestData;
                    });
                }
            }

            return [[
                'tasks' => $finData
            ]];

        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage());
        }
    }


    private function getComulativeValue($isNotSinergi, $data)
    {
        // data histories
        $nType = [];
        $cType = [];

        foreach ($data as $key => $history) {
            if ($history->duration < 2) {
                $currDate = $this->prefix . Carbon::create($history->end_date)->format($this->dateFormat);
                if (isset($nType[$history->id][$currDate])) {
                    $nType[$history->id][$currDate] += $history->value;
                } else {
                    $nType[$history->id][$currDate] = $history->value;
                }
            } else {
                $progComulative = $history->value / $history->duration;

                for ($i = 1; $i <= $history->duration; $i++) {
                    $currDate = $this->prefix . Carbon::create($history->start_date)->addDays($i)->format($this->dateFormat);
                    if (isset($nType[$history->id][$currDate])) {
                        $nType[$history->id][$currDate] += $progComulative;
                    } else {
                        $nType[$history->id][$currDate] = $progComulative;
                    }
                }
            }
        }

        // sum progress
        foreach ($nType as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $cType[$key2] = isset($cType[$key2]) ? $cType[$key2] + $value2 : $value2;
            }
        }

        // create comulative
        $lastValue = 0;
        foreach ($cType as $key => $value) {
            if ($isNotSinergi) {
                $lastValue += $value;
            } else {
                $lastValue += $value;
            }

            $cType[$key] = $lastValue;
        }

        return $cType;
    }

    public function filterScurves(Request $request)
    {
        try {


            $finData = [];


            $response = DB::table('vw_scurves_chart')->where('id', $request->charter_id)->get();

            $data = $response[0];
            $data->tasks = json_decode($data->tasks);

            $tempTask["real"] = [];
            $tempTask["plan"] = [];

            foreach ($data->tasks as $kTask => $task) {
                $tempTask["real"] = collect($tempTask["real"])->merge($task->hst_progress)->sortBy("start_date")->values()->toArray();
                $tempTask["plan"] = collect($tempTask["plan"])->merge($task->hst_weights)->sortBy("start_date")->values()->toArray();
            }

            $data->tasks = $tempTask;


            foreach ($data->tasks as $kType => $type) {
                $currMaxVal = 0;

                foreach ($type as $kHistory => $history) {
                    $currMaxVal += (int)$history->value;

                    $data->tasks[$kType][$kHistory]->calculated_value = $currMaxVal;
                }
            }

            $finData["real"] = [];
            $finData["plan"] = [];
            $finData["label"] = [];

            // merge, group dan sort
            $additionals = json_decode($data->additionals, true)[0];
            $pembobotan = $additionals->pembobotan ?? null;

            if ($pembobotan) {
                $pembobotanTaskReal = collect($data->tasks["real"])->map(function ($task) use ($pembobotan) {
                    $nData = $task;
                    $nData->value = ($pembobotan / 100) * $task->value;

                    return $nData;
                })->toArray();

                $pembobotanTaskPlan = collect($data->tasks["plan"])->map(function ($task) use ($pembobotan) {
                    $nData = $task;
                    $nData->value = ($pembobotan / 100) * $task->value;

                    return $nData;
                })->toArray();

                $finData["real"] = collect($finData["real"])->merge($pembobotanTaskReal)->toArray();
                $finData["plan"] = collect($finData["plan"])->merge($pembobotanTaskPlan)->toArray();
            } else {
                $finData["real"] = collect($finData["real"])->merge($data->tasks["real"])->toArray();
                $finData["plan"] = collect($finData["plan"])->merge($data->tasks["plan"])->toArray();
            }

            $finData["real"] = collect($finData["real"])->sortBy("start_date")->values()->toArray();
            $finData["plan"] = collect($finData["plan"])->sortBy("start_date")->values()->toArray();

            foreach ($finData as $kType => $type) {
                $currMaxVal = 0;

                foreach ($type as $kHistory => $history) {
                    $currMaxVal += $history->value;

                    $finData[$kType][$kHistory]->calculated_value = $currMaxVal;
                }
            }

            // data projek
            $mergeAllData = collect($finData["plan"])->merge($finData["real"]);


            // create raw kurva s data
            foreach ($finData as $tKey => $type) {
                $finData[$tKey] = $this->getComulativeValue(true, $type);
            }
            // create label data
            $minDate = Carbon::createMidnightDate($mergeAllData->sortBy("start_date")->first()->start_date);
            $maxDate = Carbon::createMidnightDate($mergeAllData->sortByDesc("end_date")->first()->end_date);
            $totalYear = $minDate->diffInYears($maxDate);

            if ($totalYear < 1) {
                return [
                    [
                        'label' => 'Daily',
                        'value' => 'daily',
                        'is_active' => true
                    ], [
                        'label' => 'Weekly',
                        'value' => 'weekly',
                        'is_active' => true
                    ],
                    [
                        'label' => 'Monthly',
                        'value' => 'monthly',
                        'is_active' => true
                    ],
                    [
                        'label' => 'Annually',
                        'value' => 'annually',
                        'is_active' => true

                    ]
                ];
            } else {
                return [
                    [
                        'label' => 'Daily',
                        'value' => 'daily',
                        'is_active' => false
                    ], [
                        'label' => 'Weekly',
                        'value' => 'weekly',
                        'is_active' => false
                    ],
                    [
                        'label' => 'Monthly',
                        'value' => 'monthly',
                        'is_active' => true
                    ],
                    [
                        'label' => 'Annually',
                        'value' => 'annually',
                        'is_active' => true

                    ]
                ];
            }


        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage());
        }
    }
}

?>
