<?php

namespace Database\Seeders;

use App\Models\CoordinateProvince;
use Illuminate\Database\Seeder;

class CoordinateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = [
            [
                // aceh
                'id_province' => 11,
                'lat' => 4.695135,
                'lng' => 96.7493993,
            ],
            [
                // sumut
                'id_province' => 12,
                'lat' => 2.1153547,
                'lng' => 99.5450974,
            ],
            [
                'id_province' => 13,
                'lat' => -0.7399397,
                'lng' => 100.8000051,
            ],
            [
                'id_province' => 14,
                'lat' => 0.2933469,
                'lng' => 101.7068294,
            ],
            [
                'id_province' => 15,
                'lat' => -1.4851831,
                'lng' => 102.4380581,
            ],
            [
                // SUMATERA SELATAN
                'id_province' => 16,
                'lat' => -3.3194374,
                'lng' => 103.914399,
            ],
            [
                'id_province' => 17,
                'lat' => -3.5778471,
                'lng' => 102.3463875,
            ],
            [
                'id_province' => 18,
                'lat' => -4.5585849,
                'lng' => 105.4068079,
            ],
            [
                'id_province' => 19,
                'lat' => -2.7410513,
                'lng' => 106.4405872,
            ],
            [
                'id_province' => 21,
                'lat' => 3.9456514,
                'lng' => 108.1428669,
            ],
            [
                // DKI JAKARTA
                'id_province' => 31,
                'lat' => -6.211544,
                'lng' => 106.845172,
            ],
            // jabar
            [
                'id_province' => 32,
                'lat' => -7.090911,
                'lng' => 107.668887,
            ],
            [
                'id_province' => 33,
                'lat' => -7.150975,
                'lng' => 110.1402594,
            ],
            [
                'id_province' => 34,
                'lat' => -7.8753849,
                'lng' => 110.4262088,
            ],
            [
                'id_province' => 35,
                'lat' => -7.5360639,
                'lng' => 112.2384017,
            ],
            [
                'id_province' => 36,
                'lat' => -6.4058172,
                'lng' => 106.0640179,
            ],
            [
                'id_province' => 51,
                'lat' => -8.4095178,
                'lng' => 115.188916,
            ],
            [
                'id_province' => 52,
                'lat' => -8.6529334,
                'lng' => 117.3616476,
            ],
            [
                'id_province' => 53,
                'lat' => -8.6573819,
                'lng' => 121.0793705,
            ],
            [
                'id_province' => 61,
                'lat' => -0.2787808,
                'lng' => 111.4752851,
            ],
            [
                'id_province' => 62,
                'lat' => -1.6814878,
                'lng' => 113.3823545,
            ],
            // kalsel
            [
                'id_province' => 63,
                'lat' => -3.316694,
                'lng' => 114.590111,
            ],[
                'id_province' => 64,
                'lat' => 1.6406296,
                'lng' => 116.419389,
            ],[
                'id_province' => 65,
                'lat' => 3.073093,
                'lng' =>116.0413895,
            ],[
                'id_province' => 71,
                'lat' => 0.6246932,
                'lng' => 123.9750018,
            ],[
                'id_province' => 72,
                'lat' => -1.430025,
                'lng' => 121.445618,
            ],[
                'id_province' => 73,
                'lat' => -3.668799,
                'lng' => 119.974052,
            ],[
                'id_province' => 74,
                'lat' => -4.14491,
                'lng' =>122.1746055,
            ],[
                'id_province' => 75,
                'lat' => 0.6999372,
                'lng' => 122.4467238,
            ],[
                'id_province' => 76,
                'lat' => -2.844137,
                'lng' => 119.232079,
            ],[
                'id_province' => 81,
                'lat' => -3.238462,
                'lng' => 130.145279
            ],[
                'id_province' => 82,
                'lat' => 1.5709993,
                'lng' => 127.8087693,
            ],[
                'id_province' => 91,	
                'lat' => -2.132020,
                'lng' => 133.177900,
            ],[
                'id_province' => 94,
                'lat' => -4.269928,
                'lng' => 138.0803529,
            ],
            
        ];
        $check = CoordinateProvince::where('id_province',11)->first();
        foreach ($provinces as $province) {
            if(empty($check)) {
                CoordinateProvince::create($province);
            } 
        }
    }
}
