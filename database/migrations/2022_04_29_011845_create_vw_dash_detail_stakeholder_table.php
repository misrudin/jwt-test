<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVwDashDetailStakeholderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_detail_stakeholder");
       DB::statement("CREATE VIEW vw_dash_detail_stakeholder AS SELECT s.name AS stackholder,
    st.name AS stackholder_types,
	ct.id as id_proyek
   FROM charter_stackholders cs
     JOIN charters ct ON cs.charter_code::text = ct.code::text
     JOIN stackholders s ON cs.stackholder_code::text = s.code::text
     JOIN stackholder_types st ON s.stackholder_type_code::text = st.code::text;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_detail_stakeholder');
    }
}
