<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwProjectRepostCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_project_repost_cost");

        DB::statement("
        CREATE VIEW vw_project_repost_cost AS 
        SELECT ct.code AS project_code,
        ct.descriptions AS project_deskripsi,
        ''::text AS cost_model,
        ct.value AS total_anggaran,
        ''::text AS total_realisasi,
        ''::text AS total_penyerapan,
        ct.updated_at AS terkahir_update
       FROM charters ct
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_project_repost_cost");

        // Schema::dropIfExists('vw_project_repost_cost');
    }
}
