<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashStakeholderPartnerCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_stakeholder_partner_customer");
        DB::statement("
        CREATE VIEW vw_dash_stakeholder_partner_customer AS 
        SELECT p.partner_name AS dataname,
        pp.top_type AS type,
        sum(top.value) AS realisasi,
        ( SELECT count(pp2.id) AS count
           FROM partner_payments pp2
          WHERE ((pp2.partner_code)::text = (p.code)::text)) AS proyek,
        c.planed_start_at,
        c.planed_end_at AS planed_end_at,
        c.deleted_at
        FROM (((partners p
        JOIN partner_payments pp ON (((pp.partner_code)::text = (p.code)::text)))
        JOIN charters c ON ((pp.charter_id = c.id)))
        JOIN term_of_payments top ON (((top.partner_payment_code)::text = (pp.code)::text)))
        WHERE c.deleted_at IS NULL
        GROUP BY p.code, p.partner_name, pp.top_type, c.planed_start_at, c.planed_end_at, c.deleted_at
        ORDER BY (sum(top.value)) DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_stakeholder_partner_customer");
    }
}
