<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanProjectManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_project_manager");
        DB::statement("CREATE VIEW vw_dash_laporan_project_manager AS SELECT st.name, st.code, NULL::character varying(20) AS jabatan, NULL::smallint AS level, count(crs.name) AS project_active, sum(ct.value) AS value FROM charters ct JOIN charter_stackholders cst ON cst.charter_code::text = ct.code::text JOIN stackholders st ON st.code::text = cst.stackholder_code::text JOIN stackholder_types sty ON sty.code::text = st.stackholder_type_code::text JOIN charter_running_statuses crs ON crs.code::text = ct.charter_running_status_code::text WHERE crs.name::text = 'Aktif'::text AND ct.deleted_at IS NULL GROUP BY st.name, st.code;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_laporan_project_manager');
    }
}
