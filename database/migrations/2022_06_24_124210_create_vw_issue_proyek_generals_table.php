<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwIssueProyekGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_issue_proyek_generals");
        DB::statement("
        CREATE VIEW vw_issue_proyek_generals AS
        SELECT rr.name AS issue_name,
        ct.code AS code_proyek,
        ct.name AS name_proyek,
        rc.name AS issue_category,
        rs.name AS issue_status,
        rr.created_at
        FROM (((risk_registers rr
        LEFT JOIN charters ct ON (((rr.charter_id)::text = (ct.id)::text)))
        LEFT JOIN risk_statuses rs ON (((rr.risk_status_code)::text = (rs.code)::text)))
        LEFT JOIN risk_categories rc ON (((rr.risk_category_code)::text = (rc.code)::text)))
        LEFT JOIN
        users
        ON ct.created_by = users.id
        WHERE (ct.deleted_at IS NULL AND users.tenant_id != 'sda')
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_issue_proyek_generals');
    }
}
