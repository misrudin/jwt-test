<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanListTopCharterProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_list_top_charter_proyek");
        DB::statement("
        CREATE VIEW vw_dash_laporan_list_top_charter_proyek AS 
         SELECT  DISTINCT pt.id AS id_partner,
	     pty.top_type AS type_partner
	 
	   FROM term_of_payments top
		 LEFT JOIN partner_payments pty ON top.partner_payment_code::text = pty.code::text
		 LEFT JOIN partners pt ON pty.partner_code::text = pt.code::text
		 LEFT JOIN pay_types ptp ON ptp.code::text = pty.pay_type_code::text
		 LEFT JOIN charters ct ON ct.id = pty.charter_id
	  WHERE ct.deleted_at IS NULL AND ct.closed_by IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_list_top_charter_proyek");
    }
}
