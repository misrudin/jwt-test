<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwActualCostChartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_actual_cost_chart");
        DB::statement("CREATE VIEW vw_actual_cost_chart AS SELECT t.charter_id,
            to_char(evd.audit_date, 'YYYY-MM-DD'::text) AS audit_date,
            EXTRACT(Week FROM evd.audit_date) AS week_num,
            sum(evd.actual_cost) as actual_cost
        FROM earn_value_detail evd
            LEFT JOIN earn_value ev ON evd.earn_value_id = ev.id
            LEFT JOIN tasks t ON t.id = ev.task_id::uuid
          WHERE t.type::text = 'task'::text
          GROUP BY 1,2,3;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_actual_cost_chart');
    }
}
