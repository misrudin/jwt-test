<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwStakeholderProjectManagerDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::statement("DROP VIEW IF EXISTS vw_stakeholder_project_manager_detail");

        DB::statement("
        CREATE VIEW vw_stakeholder_project_manager_detail AS 
        SELECT st.code,
        st.name,
        ctg.name AS category,
        ( SELECT count(ct1.id) AS count
           FROM ((charters ct1
             JOIN charter_stackholders cst1 ON (((cst1.charter_code)::text = (ct1.code)::text)))
             JOIN stackholders st1 ON (((st1.code)::text = (cst1.stackholder_code)::text)))
          WHERE (((st1.name)::text = (st.name)::text) AND ((ct1.kick_off_at)::date < (ct1.planed_start_at)::date) AND (ct1.closed_by IS NULL))) AS lead,
        ( SELECT count(ct1.id) AS count
           FROM ((charters ct1
             JOIN charter_stackholders cst1 ON (((cst1.charter_code)::text = (ct1.code)::text)))
             JOIN stackholders st1 ON (((st1.code)::text = (cst1.stackholder_code)::text)))
          WHERE (((st1.name)::text = (st.name)::text) AND (ct1.progress < '100'::numeric) AND (ct1.deleted_at IS NULL) AND (ct1.closed_by IS NULL) AND ((ct1.planed_start_at)::date < (now())::date) AND ((ct1.planed_end_at)::date > (now())::date))) AS leg,
        ( SELECT count(ct1.id) AS count
           FROM ((charters ct1
             JOIN charter_stackholders cst1 ON (((cst1.charter_code)::text = (ct1.code)::text)))
             JOIN stackholders st1 ON (((st1.code)::text = (cst1.stackholder_code)::text)))
          WHERE (((st1.name)::text = (st.name)::text) AND (ct1.deleted_at IS NULL) AND (ct1.closed_by IS NULL) AND ((ct1.planed_end_at)::date < (now())::date))) AS delay,
        ( SELECT count(ct1.id) AS count
           FROM ((charters ct1
             JOIN charter_stackholders cst1 ON (((cst1.charter_code)::text = (ct1.code)::text)))
             JOIN stackholders st1 ON (((st1.code)::text = (cst1.stackholder_code)::text)))
          WHERE (((st1.name)::text = (st.name)::text) AND (ct1.deleted_at IS NULL) AND (ct1.closed_by IS NOT NULL))) AS close
        FROM (((stackholders st
        JOIN charter_stackholders cst ON (((cst.stackholder_code)::text = (st.code)::text)))
        LEFT JOIN charters ct ON (((ct.code)::text = (cst.charter_code)::text)))
        LEFT JOIN charter_categories ctg ON (((ctg.code)::text = (ct.charter_category_code)::text)))
        GROUP BY st.code, st.name, ctg.name;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement("DROP VIEW IF EXISTS vw_stakeholder_project_manager_detail");

      // Schema::dropIfExists('vw_stakeholder_project_manager_detail');
    }
}
