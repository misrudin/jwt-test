<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwSumberAnggaranGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_sumber_anggaran_generals");
        DB::statement("CREATE VIEW vw_sumber_anggaran_generals AS 
        SELECT
        ch.name, 
        cr.name AS cost_name, 
        ch.descriptions, 
        ch.value, 
        ch.progress, 
        ch.budget_allocated, 
        ch.budget_spent,
        ch.budget_gap,
        ch.planed_start_at,
        ch.planed_end_at,
        ch.deleted_at,
				users.tenant_id,
				ch.created_at
        FROM
            charters AS ch
            LEFT JOIN cost_resources AS cr ON ( ( ( ch.code ) :: TEXT = ( cr.code ) :: TEXT ) )
        LEFT JOIN
        users
        ON ch.created_by = users.id
        WHERE (ch.deleted_at IS NULL AND users.tenant_id <> 'sda')
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_sumber_anggaran_generals');
    }
}
