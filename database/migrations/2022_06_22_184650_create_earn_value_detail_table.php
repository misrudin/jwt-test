<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEarnValueDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earn_value_detail', function (Blueprint $table) {
            $table->uuid('id')->unique()->nullable(false)->primary();
            $table->uuid('earn_value_id');
            $table->timestamp('audit_date')->nullable()->unique();
            $table->float('earn_value_bobot')->nullable();
            $table->integer('earn_value_percent')->nullable();
            $table->decimal('actual_cost', 19, 2, true)->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earn_value_detail');
    }
}
