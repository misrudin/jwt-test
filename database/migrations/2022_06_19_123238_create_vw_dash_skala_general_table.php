<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashSkalaGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_skala_general");

        DB::statement("
        CREATE VIEW vw_dash_skala_general AS
        SELECT
        cvt.name AS skala, 
        ct.code AS code_proyek, 
        ct.name AS nama_proyek, 
        ct.descriptions AS deskripsi_proyek, 
        ct.value AS nilai_proyek, 
        ct.created_at AS date_proyek, 
        ct.planed_start_at AS planed_start_at, 
        ct.planed_end_at AS planed_end_at, 
        1 AS jumlah, 
        users.email, 
        users.tenant_id
        FROM ( charters AS ct JOIN charter_value_categories AS cvt ON (((cvt.code)::text = (ct.value_category)::text)))
        LEFT JOIN
        users
        ON ct.created_by = users.id
        WHERE (ct.deleted_at IS NULL AND users.tenant_id != 'sda')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_skala_general');
    }
}
