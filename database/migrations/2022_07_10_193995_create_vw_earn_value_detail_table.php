<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwEarnValueDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_earn_value_detail");
        DB::statement("CREATE VIEW vw_earn_value_detail AS
             SELECT t.charter_id,
             sum(ev.budget_at_completion) AS bac
             FROM tasks t
             LEFT JOIN earn_value ev ON ev.task_id::uuid = t.id
             WHERE t.type::text = 'task'::text
             GROUP BY t.charter_id;
        ");
    }
    public function down()
    {
        Schema::dropIfExists('vw_earn_value_detail');
    }
}
