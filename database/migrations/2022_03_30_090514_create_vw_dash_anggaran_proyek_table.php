<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashAnggaranProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_anggaran_proyek");
        DB::statement("
        CREATE VIEW vw_dash_anggaran_proyek AS
        SELECT ct.code AS code_proyek,
        ct.budget_allocated AS rencana_anggaran,
        ct.budget_spent AS realisasi_anggaran,
        ct.budget_gap AS sisa_anggaran,
        ct.value AS nilai_proyek,
        crs.name AS charter_running_status,
        ct.name AS name_proyek,
        ct.descriptions AS deskripsi_proyek,
        ct.created_at AS date_proyek,
        ct.planed_start_at AS planed_start_at,
        ct.planed_end_at AS planed_end_at
         FROM (charters ct
        JOIN charter_running_statuses crs ON (((ct.charter_running_status_code)::text = (crs.code)::text)))
         WHERE (ct.deleted_at IS NULL);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_anggaran_proyek");
    }
}
