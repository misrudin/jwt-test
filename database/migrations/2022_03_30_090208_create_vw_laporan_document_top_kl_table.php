<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwLaporanDocumentTopKlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_top_kl");
        // todo masih bug di karakter  WHERE ((ctf.category_name)::text = 'Kontrak Layanan (KL)'::text)
    //     DB::statement('
    //     CREATE VIEW vw_laporan_document_top_kl AS 
    //     SELECT ct.code AS project_code,
    //     ct.descriptions AS project_name,
    //     ''::text AS idlop,
    //     ''::text AS no_pb,
    //     cs.name AS segment_name,
    //     ptp.name AS payment_name,
    //     ct.planed_end_at AS expected_date,
    //     crs.name AS status,
    //     ptf.id AS refrence_id_document
    //    FROM (((((((project_files ptf
    //      JOIN category_file ctf ON ((ctf.id = ptf.Category)))
    //      JOIN charters ct ON ((ct.id = ptf.ProjectID)))
    //      JOIN charter_segments cs ON (((cs.code)::text = (ct.charter_segment_code)::text)))
    //      JOIN partner_payments pty ON ((pty.charter_id = ct.id)))
    //      JOIN partners pt ON (((pt.code)::text = (pty.partner_code)::text)))
    //      JOIN pay_types ptp ON (((ptp.code)::text = (pty.pay_type_code)::text)))
    //      JOIN charter_running_statuses crs ON (((crs.code)::text = (ct.charter_running_status_code)::text)))
    //   WHERE ((ctf.category_name)::text = 'Kontrak Layanan (KL)'::text)
    //     ');

        // sementara menggunkan ini karna blm ada untuk report ini
        DB::statement('
        CREATE VIEW vw_laporan_document_top_kl AS 
        SELECT ct.code AS project_code,
        ct.descriptions AS project_name,
        ct.references AS idlop,
        Null AS no_pb,
        cs.name AS segment_name,
        ptp.name AS payment_name,
        ct.planed_end_at AS expected_date,
        crs.name AS status,
        ptf.id AS refrence_id_document
       FROM (((((((project_files ptf
         JOIN category_file ctf ON ((ctf.id = ptf."Category")))
         JOIN charters ct ON ((ct.id = ptf."ProjectID")))
         JOIN charter_segments cs ON (((cs.code)::text = (ct.charter_segment_code)::text)))
         JOIN partner_payments pty ON ((pty.charter_id = ct.id)))
         JOIN partners pt ON (((pt.code)::text = (pty.partner_code)::text)))
         JOIN pay_types ptp ON (((ptp.code)::text = (pty.pay_type_code)::text)))
         JOIN charter_running_statuses crs ON (((crs.code)::text = (ct.charter_running_status_code)::text)))
      
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_top_kl");
        // Schema::dropIfExists('vw_laporan_document_top_kl');
    }
}
