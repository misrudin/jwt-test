<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwRegionalGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_regional_group");
        DB::statement("
        CREATE VIEW vw_regional_group AS 
        SELECT charter_code, MAX(charter_regional_code) AS charter_regional_code FROM charter_organizations GROUP BY charter_code");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_regional_group');
    }
}
