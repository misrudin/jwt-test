<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashAchievementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_achievement");

        DB::statement("
        CREATE VIEW vw_dash_achievement AS 
        SELECT ct.code AS code_proyek,
        ct.descriptions AS deskripsi_proyek,
        ct.created_at AS date_proyek,
        ts.progress AS percent_progress
        FROM (charters ct
        JOIN tasks ts ON ((ts.charter_id = ct.id)));
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_achievement');
    }
}
