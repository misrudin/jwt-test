<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwPlanValueChartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_plan_value_chart");
        DB::statement("CREATE VIEW vw_plan_value_chart AS  SELECT t.charter_id,
            to_char(pvd.audit_date, 'YYYY-MM-DD'::text) AS audit_date,
            EXTRACT(Week FROM pvd.audit_date) AS week_num,
            avg(pvd.plan_value_percent) as plan_value_percent,
            SUM(pvd.plan_value_bobot) as plan_value_bobot,
            sum(ev.budget_at_completion::double precision * pvd.plan_value_percent::double precision / 100::double precision) AS plan_value_rupiah
        FROM plan_value_detail pvd
            LEFT JOIN earn_value ev ON pvd.earn_value_id = ev.id
            LEFT JOIN tasks t ON t.id = ev.task_id::uuid
          WHERE t.type::text = 'task'::text
          GROUP BY 1,2,3");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_plan_value_chart');
    }
}
