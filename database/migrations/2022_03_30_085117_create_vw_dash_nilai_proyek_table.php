<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashNilaiProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_nilai_proyek");

        DB::statement("
        CREATE VIEW vw_dash_nilai_proyek AS 
        SELECT ct.code AS code_proyek,
        ct.budget_allocated,
        ct.budget_spent,
        ct.budget_gap,
        crs.name AS charter_running_status,
        ct.name AS name_proyek,
        ct.descriptions AS deskripsi_proyek,
        ct.created_at AS date_proyek,
        ct.planed_start_at AS planed_start_at,
        ct.planed_end_at AS planed_end_at,
        ct.value AS total_value
        FROM (charters ct
        JOIN charter_running_statuses crs ON (((ct.charter_running_status_code)::text = (crs.code)::text)));
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_nilai_proyek');
    }
}
