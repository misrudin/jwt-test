<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwMainProyekGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_main_proyek_generals");
        DB::statement("CREATE VIEW vw_main_proyek_generals AS
        SELECT COUNT
	( ct.NAME ) AS total_proyek,
	COUNT ( CASE chs.NAME WHEN 'Kandidat' THEN 1 ELSE NULL END ) AS kandidat,
	COUNT ( CASE chs.NAME WHEN 'Aktif' THEN 1 ELSE NULL END ) AS aktif,
	COUNT ( CASE chs.NAME WHEN 'Closed' THEN 1 ELSE NULL END ) AS closed,
	COUNT ( CASE chs.NAME WHEN 'Draft' THEN 1 ELSE NULL END ) AS draft,
	COUNT ( CASE chs.NAME WHEN 'Tech Closed' THEN 1 ELSE NULL END ) AS tech_closed,
	ct.NAME AS proyek_name,
	ct.budget_allocated,
	ct.budget_spent,
	ct.planed_start_at,
	ct.planed_end_at 
FROM
	charters AS ct
	LEFT JOIN charter_running_statuses AS chs ON ct.charter_running_status_code = chs.code
	LEFT JOIN charter_stackholders ON ct.code = charter_stackholders.charter_code
	LEFT JOIN stackholders ON charter_stackholders.stackholder_code = stackholders.code
	LEFT JOIN users ON stackholders.email = users.email 
WHERE
	( ct.deleted_at IS NULL AND ( users.tenant_id IS NULL OR users.tenant_id <> 'sda' ) ) 
GROUP BY
	ct.NAME,
	chs.NAME,
	ct.budget_allocated,
	ct.budget_spent,
	ct.planed_start_at,
	ct.planed_end_at");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_main_proyek_generals');
    }
}
