<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateVwDashLaporanProjectManagerProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_project_manager_profile");
        DB::statement("CREATE VIEW vw_dash_laporan_project_manager_profile AS SELECT st.code, NULL::numeric(5) project_load, NULL::numeric(5) credit_point_this_week, NULL::numeric(5) achieved_project, NULL::numeric(5) total_credit_point,  sum(ct.value) as total_project_value, count(ct.code) as total_project, (select count(c.code) from charter_stackholders cs left join charters c 
on cs.charter_code=c.code 
left join stackholders s 
on cs.stackholder_code = s.code 
left join charter_running_statuses crs
on c.charter_running_status_code = crs.code
where s.code=st.code and crs.name = 'Aktif') as project_active_count,   (SELECT count(ct1.id) AS count
           FROM charters ct1
             JOIN charter_stackholders cst1 ON cst1.charter_code::text = ct1.code::text
             JOIN stackholders st1 ON st1.code::text = cst1.stackholder_code::text
          WHERE st1.code::text = st.code::text AND ct1.kick_off_at::date < ct1.planed_start_at::date 
				 AND ct1.closed_by IS NULL) AS lead, ( SELECT count(ct1.id) AS count
           FROM charters ct1
             JOIN charter_stackholders cst1 ON cst1.charter_code::text = ct1.code::text
             JOIN stackholders st1 ON st1.code::text = cst1.stackholder_code::text
          WHERE st1.code::text = st.code::text AND ct1.progress < '100'::numeric AND ct1.deleted_at IS NULL AND ct1.closed_by IS NULL AND ct1.planed_start_at::date < now()::date AND ct1.planed_end_at::date > now()::date) AS leg,
    	( SELECT count(ct1.id) AS count
           FROM charters ct1
             JOIN charter_stackholders cst1 ON cst1.charter_code::text = ct1.code::text
             JOIN stackholders st1 ON st1.code::text = cst1.stackholder_code::text
          WHERE st1.code::text = st.code::text AND ct1.deleted_at IS NULL AND 
		 ct1.closed_by IS NULL AND ct1.planed_end_at::date < now()::date) AS delay,
		 (SELECT sum(ct1.value)
           FROM charters ct1
             JOIN charter_stackholders cst1 ON cst1.charter_code::text = ct1.code::text
             JOIN stackholders st1 ON st1.code::text = cst1.stackholder_code::text
          WHERE ct1.deleted_at IS NULL AND 
		 ct1.closed_by IS NULL AND ct1.planed_end_at::date < now()::date) as budget_delay,
		 (SELECT sum(ct1.value) AS count
           FROM charters ct1
             JOIN charter_stackholders cst1 ON cst1.charter_code::text = ct1.code::text
             JOIN stackholders st1 ON st1.code::text = cst1.stackholder_code::text
          WHERE st1.code::text = st.code::text AND ct1.kick_off_at::date < ct1.planed_start_at::date 
				 AND ct1.closed_by IS NULL) as budget_lead,
		( SELECT sum(ct1.value) AS count
           FROM charters ct1
             JOIN charter_stackholders cst1 ON cst1.charter_code::text = ct1.code::text
             JOIN stackholders st1 ON st1.code::text = cst1.stackholder_code::text
          WHERE st1.code::text = st.code::text AND ct1.progress < '100'::numeric AND ct1.deleted_at IS NULL AND 
		 ct1.closed_by IS NULL AND ct1.planed_start_at::date < now()::date AND ct1.planed_end_at::date > now()::date) as budget_leg
		 FROM stackholders st
		INNER JOIN charter_stackholders cst ON cst.stackholder_code::text = st.code::text
		LEFT JOIN charters ct ON ct.code::text = cst.charter_code::text
		LEFT JOIN charter_categories ctg ON ctg.code::text = ct.charter_category_code::text
		LEFT JOIN charter_running_statuses crs ON ct.charter_running_status_code = crs.code
		GROUP BY st.code");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_laporan_project_manager_profile');
    }
}
