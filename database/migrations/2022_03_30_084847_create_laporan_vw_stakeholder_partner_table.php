<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporanVwStakeholderPartnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS laporan_vw_stakeholder_partner");

        DB::statement("
        CREATE VIEW laporan_vw_stakeholder_partner AS 
        SELECT DISTINCT ON (pt.code) pt.code AS kode_partner,
        pt.partner_name AS nama_pertner,
        ptp.name AS jenis_top,
        ( SELECT count(pty1.charter_id) AS count
            FROM (partner_payments pty1
                JOIN charters ct1 ON ((ct1.id = pty1.charter_id)))
            WHERE (((pty1.partner_code)::text = (pt.code)::text) AND (ct1.deleted_at IS NULL))) AS total_proyek,
        pty.top_type AS tipe_partner,
        ct.id AS refrence_project_to_document
    FROM (((partners pt
        JOIN partner_payments pty ON (((pty.partner_code)::text = (pt.code)::text)))
        JOIN pay_types ptp ON (((ptp.code)::text = (pty.pay_type_code)::text)))
        LEFT JOIN charters ct ON ((ct.id = pty.charter_id)))
    WHERE (ct.deleted_at IS NULL)
    GROUP BY pt.code, pt.partner_name, ptp.name, ct.id, pty.top_type, pty.created_at
    ORDER BY pt.code, pty.created_at DESC;
      ");
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_vw_stakeholder_partner');
    }
}
