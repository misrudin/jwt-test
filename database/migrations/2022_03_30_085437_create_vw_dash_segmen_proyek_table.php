<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashSegmenProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_segmen_proyek");

        DB::statement("
        CREATE VIEW vw_dash_segmen_proyek AS 
        SELECT cs.name AS nama_segment,
        0 AS nilai_realisasi,
        ct.code AS code_proyek,
        ct.descriptions AS deskripsi_proyek,
        ct.created_at AS date_proyek,
        ct.planed_start_at AS planed_start_at,
        ct.planed_end_at AS planed_end_at,
        1 AS jumlah
        FROM (charters ct
        JOIN charter_segments cs ON (((cs.code)::text = (ct.charter_segment_code)::text)))
        WHERE (ct.deleted_at IS NULL);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_segmen_proyek');
    }
}
