<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwLaporanProjectManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_project_manager");

        DB::statement("
        CREATE VIEW vw_laporan_project_manager AS 
        SELECT st.name,
        st.code,
        count(crs.name) AS project_active,
        sum(ct.value) AS value
        FROM ((((charters ct
        JOIN charter_stackholders cst ON (((cst.charter_code)::text = (ct.code)::text)))
        JOIN stackholders st ON (((st.code)::text = (cst.stackholder_code)::text)))
        JOIN stackholder_types sty ON (((sty.code)::text = (st.stackholder_type_code)::text)))
        JOIN charter_running_statuses crs ON (((crs.code)::text = (ct.charter_running_status_code)::text)))
        WHERE (((crs.name)::text = 'Aktif'::text) AND (ct.deleted_at IS NULL) AND ((sty.name)::text = 'Project Manager'::text))
        GROUP BY st.name, st.code;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_project_manager");

        // Schema::dropIfExists('vw_laporan_project_manager');
    }
}
