<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashCharterAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_charter_all");
        
        DB::statement("
           CREATE VIEW vw_dash_charter_all AS 
           SELECT ct.name AS nama_proyek,
           ct.descriptions AS deskripsi_proyek,
           ct.value AS nilai_proyek,
           (ct.progress / (100)::numeric) AS progress,
           CASE
           WHEN (ct.progress > (99)::numeric) THEN 'Selesai'::text
           ELSE 'Belum selesai'::text
           END AS done_status,
           ct.budget_allocated,
           ct.budget_spent,
           ct.budget_gap,
           CASE
           WHEN (ct.budget_allocated > (0)::numeric) THEN (ct.budget_spent / ct.budget_allocated)
           ELSE (0)::numeric
           END AS budget_performance,
        ct.created_at AS date_proyek,
        ct.planed_start_at AS planed_start_at,
        ct.planed_end_at AS planed_end_at,
        cvc.name AS skala,
        ct.duration,
        cs.name AS symptom,
        cs2.name AS segmen,
        cc.name AS category,
        ctg.name AS type_group,
        cds.name AS delivery_status,
        crs.name AS running_status,
        cas.name AS assignment_status,
        p.name AS province,
        c.name AS city,
        d.name AS district,
        1 AS jumlah
        FROM ((((((((((((charters ct
        LEFT JOIN charter_value_categories cvc ON (((cvc.code)::text = (ct.value_category)::text)))
        LEFT JOIN charter_symptoms cs ON (((cs.code)::text = (ct.charter_symptom_code)::text)))
        LEFT JOIN charter_segments cs2 ON (((cs2.code)::text = (ct.charter_segment_code)::text)))
        LEFT JOIN charter_categories cc ON (((cc.code)::text = (ct.charter_category_code)::text)))
        LEFT JOIN charter_type_groups ctg ON (((ctg.code)::text = (ct.charter_type_group_code)::text)))
        LEFT JOIN charter_delivery_statuses cds ON (((cds.code)::text = (ct.charter_delivery_status_code)::text)))
        LEFT JOIN charter_running_statuses crs ON (((crs.code)::text = (ct.charter_running_status_code)::text)))
        LEFT JOIN charter_assignment_statuses cas ON (((cas.code)::text = (ct.charter_assignment_status_code)::text)))
        LEFT JOIN charter_addresses ca ON (((ca.charter_code)::text = (ct.code)::text)))
        LEFT JOIN provinces p ON ((p.id = ca.province_id)))
        LEFT JOIN cities c ON ((c.id = ca.city_id)))
        LEFT JOIN districts d ON ((d.id = ca.district_id)))
        WHERE (ct.deleted_at IS NULL);
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_charter_all");

        // Schema::dropIfExists('vw_dash_charter_all');
    }
}
