<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwLaporanDocumentTopKbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_top_kb");
        // todo masih bug di karakter  WHERE ((ctf.category_name)::text = 'Kontrak Bersama (KB)'::text)
    //     DB::statement('
    //     CREATE VIEW vw_laporan_document_top_kb AS 
    //     SELECT ct.code AS project_code,
    //     ct.descriptions AS project_name,
    //     cs.name AS segment_name,
    //     st.name AS pm_name,
    //     ct.value AS project_value,
    //     ptp.name AS payment_name,
    //     ct.planed_end_at AS expected_date,
    //     crs.name AS status,
    //     ptf.id AS refrence_id_document
    //    FROM (((((((((project_files ptf
    //      JOIN category_file ctf ON ((ctf.id = ptf."Category")))
    //      JOIN charters ct ON ((ct.id = ptf."ProjectID")))
    //      JOIN charter_segments cs ON (((cs.code)::text = (ct.charter_segment_code)::text)))
    //      JOIN charter_stackholders cst ON (((cst.charter_code)::text = (ct.code)::text)))
    //      JOIN stackholders st ON (((st.code)::text = (cst.stackholder_code)::text)))
    //      JOIN partner_payments pty ON ((pty.charter_id = ct.id)))
    //      JOIN partners pt ON (((pt.code)::text = (pty.partner_code)::text)))
    //      JOIN pay_types ptp ON (((ptp.code)::text = (pty.pay_type_code)::text)))
    //      JOIN charter_running_statuses crs ON (((crs.code)::text = (ct.charter_running_status_code)::text)))
    //   WHERE ((ctf.category_name)::text = 'Kontrak Bersama (KB)'::text)
    //     ');

 // sementara menggunkan ini karna blm ada untuk report ini
        DB::statement('
        CREATE VIEW vw_laporan_document_top_kb AS 
        SELECT ct.code AS project_code,
        ct.descriptions AS project_name,
        cs.name AS segment_name,
        st.name AS pm_name,
        ct.value AS project_value,
        ptp.name AS payment_name,
        ct.planed_end_at AS expected_date,
        crs.name AS status,
        ptf.id AS refrence_id_document,
        ct.references AS id_lop
       FROM (((((((((project_files ptf
         JOIN category_file ctf ON ((ctf.id = ptf."Category")))
         JOIN charters ct ON ((ct.id = ptf."ProjectID")))
         JOIN charter_segments cs ON (((cs.code)::text = (ct.charter_segment_code)::text)))
         JOIN charter_stackholders cst ON (((cst.charter_code)::text = (ct.code)::text)))
         JOIN stackholders st ON (((st.code)::text = (cst.stackholder_code)::text)))
         JOIN partner_payments pty ON ((pty.charter_id = ct.id)))
         JOIN partners pt ON (((pt.code)::text = (pty.partner_code)::text)))
         JOIN pay_types ptp ON (((ptp.code)::text = (pty.pay_type_code)::text)))
         JOIN charter_running_statuses crs ON (((crs.code)::text = (ct.charter_running_status_code)::text)))
      
        ');
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_top_kb");

        // Schema::dropIfExists('vw_laporan_document_top_kb');
    }
}
