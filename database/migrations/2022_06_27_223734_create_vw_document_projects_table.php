<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDocumentProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_document_projects");

        DB::statement('
        CREATE VIEW vw_document_projects AS
        SELECT
        pf.id AS file_id,
        pf.code AS file_code,
        ch.name,
        ch.value,
        ch.project_link,
        pf.filename,
        pf."Path" AS path,
        pf."URL" AS proyek_url,
        pf."Mime_Type" AS format,
        pf.created_at as tanggal_upload
        FROM
        charters AS ch
        LEFT JOIN
        project_files AS pf
        ON ch.id::uuid = pf."ProjectID"::uuid WHERE ch.deleted_at IS NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_document_projects');
    }
}
