<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwPeringkatProyeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_peringkat_proyeks");
        DB::statement("CREATE VIEW vw_peringkat_proyeks AS
        SELECT
            ct.name AS proyek_name, 
            ct.budget_allocated, 
            ct.budget_spent, 
						ct.planed_start_at,
						ct.planed_end_at,
						users.tenant_id,
						ct.created_at,
						ct.progress,
						ct.duration,
						CASE
            WHEN (((ct.kick_off_at)::date < (ct.planed_start_at)::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL)) THEN true
            ELSE NULL::boolean
        END AS lead,
        CASE
            WHEN (((ct.planed_start_at)::date < (now())::date) AND ((ct.planed_end_at)::date > (now())::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL) AND (ct.progress < 100)) THEN true
            ELSE NULL::boolean
        END AS lag,
        CASE
            WHEN ((ct.deleted_at IS NULL) AND (ct.closed_by IS NULL) AND ((ct.planed_end_at)::date < (now())::date)) THEN true
            ELSE NULL::boolean
        END AS delay,
				( SELECT count(tasks.id) AS deliverable
          FROM tasks
         WHERE tasks.charter_id::text = ct.id::text) AS deliverable
        FROM
            charters AS ct
        LEFT JOIN
        users
        ON ct.created_by = users.id
        WHERE (ct.deleted_at IS NULL AND users.tenant_id != 'sda')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_peringkat_proyeks');
    }
}
