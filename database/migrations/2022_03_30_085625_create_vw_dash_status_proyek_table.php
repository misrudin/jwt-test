<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashStatusProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_status_proyek");

        DB::statement("
        CREATE VIEW vw_dash_status_proyek AS
        SELECT crs.name,
        count(c.id) AS count
        FROM (charters c
        JOIN charter_running_statuses crs ON (((crs.code)::text = (c.charter_running_status_code)::text)))
        WHERE (c.deleted_at IS NULL)
        GROUP BY crs.name;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_status_proyek');
    }
}
