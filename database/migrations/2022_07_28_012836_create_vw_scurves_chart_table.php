<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwScurvesChartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_scurves_chart");
        DB::statement("
        CREATE VIEW vw_scurves_chart AS
           select ct.id,
        ct.name,
        ct.code,
        ct.budget_allocated,
        ct.budget_spent,
        crs.name as running_stat,
        cds.name as deliv_stat,
        ct.additionals,
        COALESCE((select jsonb_agg(
              jsonb_build_object(
                'start_date', t.start_date,
                'duration', t.duration,
                'text', t.text,
                'progress', t.progress,
                'charter_id', t.charter_id,
                'bobot', t.bobot,
				'hst_progress', COALESCE((select jsonb_agg(
              		jsonb_build_object(
				  	'id', tpw.id,
				  	'type', tpw.type,
				  	'value', (t.bobot / 100) * tpw.value,
				  	'task_id', tpw.task_id,
				  	'start_date', tpw.start_date,
				  	'end_date',  tpw.end_date,
				  	'updated_at', tpw.updated_at,
				  	'duration', tpw.duration
            		)
					order by tpw.end_date desc
          		) res FROM task_progress_weights tpw
                    WHERE tpw.type='real' and tpw.task_id = t.id), '[]'::jsonb),
				'hst_weights', COALESCE((select jsonb_agg(
              		jsonb_build_object(
				  	'id', tpw.id,
				  	'type', tpw.type,
				  	'value', (t.bobot / 100) * tpw.value,
				  	'task_id', tpw.task_id,
				  	'start_date', tpw.start_date,
				  	'end_date',  tpw.end_date,
				  	'updated_at', tpw.updated_at,
				  	'duration', tpw.duration
            		)
					order by tpw.end_date desc
          		) res FROM task_progress_weights tpw
                    WHERE tpw.type='plan' and tpw.task_id = t.id), '[]'::jsonb)
            )
			order by t.start_date
          ) res FROM tasks t
                    WHERE t.charter_id = ct.id AND t.bobot > 0), '[]'::jsonb) as tasks

        from charters ct
        left join charter_running_statuses crs on crs.code = ct.charter_running_status_code
        left join charter_delivery_statuses cds on cds.code = ct.charter_delivery_status_code
          ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_scurves_chart');
    }
}
