<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashPartnerPaymentsChartersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_partner_payments_charters");
        DB::statement("
        CREATE VIEW vw_dash_partner_payments_charters AS
        SELECT c.name AS proyek_name,
        c.code AS code_proyek,
        c.created_at AS date_proyek,
        c.planed_start_at AS planed_start_at,
        c.planed_end_at AS planed_end_at,
        top.top_status AS status,
        pp.top_type AS type_partner,
        1 AS jumlah_proyek,
        top.partner_payment_code,
        p.code AS code_partner,
        p.partner_name AS name_partner,
        top.value AS realisasi_anggaran
        FROM (((term_of_payments top
        JOIN partner_payments pp ON (((top.partner_payment_code)::text = (pp.code)::text)))
        JOIN partners p ON (((pp.partner_code)::text = (p.code)::text)))
        JOIN charters c ON (((pp.charter_id)::text = (c.id)::text)))
        WHERE (c.deleted_at IS NULL);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_partner_payments_charters");
    }
}
