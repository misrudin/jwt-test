<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanValueDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_value_detail', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamp('audit_date')->nullable();
            $table->float('plan_value_percent')->nullable();
            $table->float('plan_value_bobot')->nullable();
            $table->uuid('earn_value_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_value_detail');
    }
}
