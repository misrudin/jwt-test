<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanLokasiProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_lokasi_proyek");
        DB::statement("
        CREATE VIEW vw_dash_laporan_lokasi_proyek AS 
        SELECT ct.code AS code_proyek,
        ( SELECT prv.name AS provinsi
            FROM provinces prv
            WHERE ((prv.id)::text = (ca.province_id)::text)) AS provinsi,
        ( SELECT cti.name AS city
            FROM cities cti
            WHERE ((cti.id)::text = (ca.city_id)::text)) AS city,
        ct.value AS nilai_proyek,
        ct.name AS name_proyek,
        ct.created_at AS date_proyek,
        ct.planed_start_at AS planed_start_at,
        ct.planed_end_at AS planed_end_at,
        1 AS jumlah,
        ( SELECT count(rr.name) AS issue
            FROM ((risk_registers rr
                LEFT JOIN risk_statuses rs ON (((rr.risk_status_code)::text = (rs.code)::text)))
                LEFT JOIN risk_categories rc ON (((rr.risk_category_code)::text = (rc.code)::text)))
            WHERE ((rr.charter_id)::text = (ct.id)::text)) AS issue
        FROM (charters ct
        LEFT JOIN charter_addresses ca ON (((ca.charter_code)::text = (ct.code)::text)))
        WHERE (ct.deleted_at IS NULL);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_lokasi_proyek");
    }
}
