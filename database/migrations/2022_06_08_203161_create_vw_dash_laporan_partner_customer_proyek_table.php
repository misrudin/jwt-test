<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanPartnerCustomerProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_partner_customer_proyek");
        DB::statement(" CREATE VIEW vw_dash_laporan_partner_customer_proyek AS
         SELECT
 	p.id AS stakeholder_id,
 	p.partner_name AS stakeholder_name,
    pp.top_type AS type,
	( SELECT SUM(top.value) AS count
          FROM partner_payments pp2
		  JOIN term_of_payments top ON top.partner_payment_code=pp2.code	
          WHERE pp2.partner_code::text = p.code::text) AS total_payment, 
	( SELECT SUM(top.value) AS count
          FROM partner_payments pp2
		  JOIN term_of_payments top ON top.partner_payment_code=pp2.code	
          WHERE pp2.partner_code::text = p.code::text AND  top.top_status::text ='PAID' ) AS realisasi, 
 	( SELECT SUM(top.value) AS count
          FROM partner_payments pp2
		  JOIN term_of_payments top ON top.partner_payment_code=pp2.code	
          WHERE pp2.partner_code::text = p.code::text AND  top.top_status::text ='UNPAID' ) AS remaining_payment, 

	( SELECT count(pp2.id) AS count
           FROM partner_payments pp2
          WHERE pp2.partner_code::text = p.code::text) AS total_proyek,
	null as document,
    c.planed_start_at,
    c.planed_end_at,
    c.deleted_at
   FROM partners p
     JOIN partner_payments pp ON pp.partner_code::text = p.code::text
     JOIN charters c ON pp.charter_id = c.id
     JOIN term_of_payments top ON top.partner_payment_code::text = pp.code::text
  WHERE c.deleted_at IS NULL
  GROUP BY p.id, p.code, p.partner_name, pp.top_type, c.planed_start_at, c.planed_end_at, c.deleted_at
  ORDER BY (sum(top.value)) DESC;
        
        
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_laporan_partner_customer_proyek');
    }
}
