<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporanVwProjectSegmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS laporan_vw_project_segment");

        DB::statement("
        CREATE VIEW laporan_vw_project_segment AS 
        SELECT cs.code AS segment_code,
        cs.name AS segment_name,
        count(ct.id) AS total,
        sum(ct.value) AS nilai,
        crs.name AS status
        FROM ((charters ct
        JOIN charter_segments cs ON (((cs.code)::text = (ct.charter_segment_code)::text)))
        JOIN charter_running_statuses crs ON (((crs.code)::text = (ct.charter_running_status_code)::text)))
        WHERE (ct.deleted_at IS NULL)
        GROUP BY cs.code, cs.name, crs.name;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_vw_project_segment');
    }
}
