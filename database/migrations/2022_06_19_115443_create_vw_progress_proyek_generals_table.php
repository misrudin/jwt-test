<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwProgressProyekGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_progress_proyek_generals");
        DB::statement("
        CREATE VIEW vw_progress_proyek_generals AS 
        SELECT
	ct.charter_running_status_code,
	crs.NAME AS charter_running_status,
CASE
		
		WHEN (
			( ( ct.kick_off_at ) :: DATE < ( ct.planed_start_at ) :: DATE ) 
			AND ( ct.closed_by IS NULL ) 
			AND ( ct.deleted_at IS NULL ) 
			) THEN
			TRUE ELSE NULL :: BOOLEAN 
		END AS LEAD,
	CASE
			
			WHEN (
				( ( ct.planed_start_at ) :: DATE < ( now( ) ) :: DATE ) 
				AND ( ( ct.planed_end_at ) :: DATE > ( now( ) ) :: DATE ) 
				AND ( ct.closed_by IS NULL ) 
				AND ( ct.deleted_at IS NULL ) 
				AND ( ct.progress < 100 ) 
				) THEN
				TRUE ELSE NULL :: BOOLEAN 
			END AS LAG,
		CASE
				
				WHEN (
					( ct.deleted_at IS NULL ) 
					AND ( ct.closed_by IS NULL ) 
					AND ( ( ct.planed_end_at ) :: DATE < ( now( ) ) :: DATE ) 
					) THEN
					TRUE ELSE NULL :: BOOLEAN 
				END AS delay,
				ct.ID,
				ct.code,
				ct.NAME,
				ct.descriptions,
				ct.
			VALUE
				,
				ct.is_strategic,
				ct.progress,
				ct.project_link,
				ct.rating,
				ct.release_periode,
				ct.sub_release_periode,
				ct.budget_allocated,
				ct.budget_spent,
				ct.budget_gap,
				ct.value_category,
				ct.duration,
				ct.created_by,
				ct.updated_by,
				ct.closed_by,
				ct.monitoring_at,
				ct.kick_off_at,
				ct.closed_date,
				ct.charter_symptom_code,
				ct.charter_segment_code,
				ct.charter_category_code,
				ct.charter_type_group_code,
				ct.charter_delivery_status_code,
				ct.charter_assignment_status_code,
				ct.REFERENCES,
				ct.deleted_at,
				ct.created_at AS date_proyek,
				ct.planed_start_at AS planed_start_at,
				ct.planed_end_at AS planed_end_at,
				ct.updated_at,
				ct.work_days,
				ct.is_private,
				users.tenant_id 
			FROM
				( charters ct JOIN charter_running_statuses crs ON ( ( ( ct.charter_running_status_code ) :: TEXT = ( crs.code ) :: TEXT ) ) )
				LEFT JOIN users ON ct.created_by = users.id 
		WHERE (ct.deleted_at IS NULL AND users.tenant_id != 'sda' )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_progress_proyek_generals');
    }
}
