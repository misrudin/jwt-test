<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwLaporanDocumentDirektoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_direktori");

    //     DB::statement('
    //     CREATE VIEW vw_laporan_document_direktori AS 
    //     SELECT
    //     pf.id AS file_id, 
    //     pf.code AS project_file_code, 
    //     ch.name AS proyek_name, 
    //     ch.value, 
    //     ch.project_link, 
    //     pf.code AS file_code, 
    //     pf.filename, 
    //     pf.created_at AS tanggal_upload, 
    //     pf."Path", 
    //     pf."URL", 
    //     pf."Mime_Type"
    // FROM
    //     charters AS ch
    //     LEFT JOIN
    //     project_files AS pf
    //     ON 
    //         ch."id" = pf."ProjectID" 
    // WHERE
	// ch.deleted_at IS NULL
    // ');
         \DB::statement($this->createView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function createView(): string
    {
        return <<<SQL
            CREATE VIEW vw_laporan_document_direktori AS 
            SELECT
            pfj."id" AS file_id,
            pfj.code AS file_code,
            pfj."Path",
            pfj.filename,
            pfj."Mime_Type" AS format,
            pfj.created_at AS tanggal_upload,
            pfj."URL" AS url_document,
            charters."name" 
        FROM
            ( project_files AS pfj JOIN category_file AS ctf ON ( ( ctf."id" = pfj."Category" ) ) )
            INNER JOIN charters ON pfj."ProjectID" = charters."id" 
        WHERE
            (
            split_part( pfj."Path", '/' :: TEXT, 1 ) IN ( ( SELECT charters.NAME FROM charters WHERE ( charters.deleted_at IS NULL ) ) ) 
            );
    SQL;
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_direktori");

        // Schema::dropIfExists('vw_laporan_document_direktori');
    }
}
