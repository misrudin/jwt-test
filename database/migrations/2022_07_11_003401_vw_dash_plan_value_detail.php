<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VwDashPlanValueDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_plan_value_detail");
        DB::statement("CREATE VIEW vw_dash_plan_value_detail AS
            SELECT t.charter_id,
            pvd.audit_date AS plan_audit_date,
            pvd.plan_value_percent,
            pvd.plan_value_bobot,
            pvd.plan_value_percent / ev.target_periode_end::double precision * ev.budget_at_completion::double precision AS plan_value_rupiah
            FROM tasks t
            LEFT JOIN earn_value ev ON ev.task_id::uuid = t.id
            LEFT JOIN plan_value_detail pvd ON pvd.earn_value_id = ev.id
            WHERE pvd.audit_date = (( SELECT max(pvd_1.audit_date) AS max
            FROM plan_value_detail pvd_1
            LEFT JOIN earn_value ev_1 ON pvd_1.earn_value_id = ev_1.id
            LEFT JOIN tasks ts ON ev_1.task_id::uuid = t.id
            WHERE ts.charter_id = t.charter_id));
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_plan_value_detail');
    }
}
