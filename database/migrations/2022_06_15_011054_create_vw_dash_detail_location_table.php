<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVwDashDetailLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_detail_location");
        DB::statement("CREATE VIEW vw_dash_detail_location AS 
        SELECT ct.id, prv.name AS provinsi,
    cti.name AS city, cti.geojson
   FROM charters ct
   LEFT JOIN charter_addresses ca ON ca.charter_code::text = ct.code::text
   LEFT JOIN provinces prv ON prv.id::text = ca.province_id::text
   LEFT JOIN cities cti ON cti.id::text = ca.city_id::text;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_detail_location');
    }
}
