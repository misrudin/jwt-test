<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashPortfolioPartnerProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_portfolio_partner_proyek");

        DB::statement("
        CREATE VIEW vw_dash_portfolio_partner_proyek AS
        SELECT pt.code AS kode_partner,
        pt.partner_name AS nama_proyek,
        count(ct.id) AS total_proyek,
        0 AS nila_realisasi
        FROM ((partners pt
        JOIN partner_payments pty ON (((pty.partner_code)::text = (pt.code)::text)))
        LEFT JOIN charters ct ON ((ct.id = pty.charter_id)))
        GROUP BY pt.code, pt.partner_name, ct.id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_portfolio_partner_proyek');
    }
}
