<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwLaporanRegionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_regional");
        DB::statement("CREATE VIEW vw_laporan_regional AS
        SELECT
			users.id AS user_id,
			users.email,
			users.username,
			users.NAME AS pic,
			charter_regionals.code AS code_regional,
			charter_regionals.NAME AS name_regional,
			charter_organizations.charter_code,
			ct.code AS proyek_code,
			ct.REFERENCES AS id_lop,
			ct.NAME AS proyek_name,
			cs.name AS segment_name,
		CASE
		WHEN (
			( ( ct.kick_off_at ) :: DATE < ( ct.planed_start_at ) :: DATE ) 
			AND ( ct.closed_by IS NULL ) 
			AND ( ct.deleted_at IS NULL ) 
			) THEN
			'lead' 
			WHEN (
				( ( ct.planed_start_at ) :: DATE < ( now( ) ) :: DATE ) 
				AND ( ( ct.planed_end_at ) :: DATE > ( now( ) ) :: DATE ) 
				AND ( ct.closed_by IS NULL ) 
				AND ( ct.deleted_at IS NULL ) 
				AND ( ct.progress < 100 ) 
				) THEN
				'lag' 
				WHEN (
					( ct.deleted_at IS NULL ) 
					AND ( ct.closed_by IS NULL ) 
					AND ( ( ct.planed_end_at ) :: DATE < ( now( ) ) :: DATE ) 
					) THEN
					'delay' ELSE NULL 
				END AS status_proyek,
				ct.progress 
			FROM
				charters AS ct
				LEFT JOIN (SELECT charter_code, MAX(charter_regional_code) AS charter_regional_code FROM charter_organizations GROUP BY charter_code) AS charter_organizations ON ct.code = charter_organizations.charter_code
				LEFT JOIN charter_regionals ON charter_organizations.charter_regional_code = charter_regionals.code
				LEFT JOIN users ON ct.created_by = users.id 
				LEFT JOIN charter_segments cs ON (((cs.code)::text = (ct.charter_segment_code)::text))
		WHERE
		ct.deleted_at IS NULL 
		");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_laporan_regional');
    }
}
