<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashStatusProyekAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_status_proyek_all");

        DB::statement("
        CREATE VIEW vw_dash_status_proyek_all AS 
        SELECT c.code AS code_proyek,
        c.name AS nama_proyek,
        c.descriptions AS deskripsi,
        crs.name AS status,
        c.created_at AS date_proyek,
        c.planed_start_at AS planed_start_at,
        c.planed_end_at AS planed_end_at,
        '1'::text AS jumlah
        FROM (charters c
        JOIN charter_running_statuses crs ON (((crs.code)::text = (c.charter_running_status_code)::text)))
        WHERE (c.deleted_at IS NULL);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_status_proyek_all');
    }
}
