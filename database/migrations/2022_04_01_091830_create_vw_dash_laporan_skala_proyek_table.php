<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanSkalaProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_skala_proyek");
        DB::statement("
        CREATE VIEW vw_dash_laporan_skala_proyek AS 
        SELECT
        ct.name AS proyek_name,
         ct.value AS nilai_proyek,
        cvt.name AS skala,
        cvt.code AS code_proyek,
         ( SELECT vwdsh.stackholder AS project_manager
                  FROM vw_dash_stake_holder vwdsh
                 WHERE vwdsh.code_proyek::text = ct.code::text LIMIT 1) AS project_manager,
         ( SELECT s.name AS account_manager
                  FROM charter_stackholders cs
                  JOIN charters c ON cs.charter_code::text = c.code::text
                  JOIN stackholders s ON cs.stackholder_code::text = s.code::text
                  JOIN stackholder_types st ON s.stackholder_type_code::text = st.code::text
               WHERE st.name::text = 'Account Manager'::text AND  c.code::text = ct.code::text AND  c.deleted_at IS NULL AND s.deleted_at IS NULL AND st.deleted_at IS NULL) AS account_manager,	
         CASE
           WHEN (((ct.kick_off_at)::date < (ct.planed_start_at)::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL)) THEN 'lead'
           WHEN (((ct.planed_start_at)::date < (now())::date) AND ((ct.planed_end_at)::date > (now())::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL) AND (ct.progress < 100))THEN 'lag'
           WHEN ((ct.deleted_at IS NULL) AND (ct.closed_by IS NULL) AND ((ct.planed_end_at)::date < (now())::date)) THEN 'delay'
               ELSE NULL
           END AS status_proyek,
           ct.progress AS progress,
           ct.updated_at ,
           ct.planed_start_at,
           ct.planed_end_at
          FROM charters ct
            JOIN charter_value_categories cvt ON cvt.code::text = ct.value_category::text
          WHERE (ct.deleted_at IS NULL)
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_skala_proyek");
    }
}
