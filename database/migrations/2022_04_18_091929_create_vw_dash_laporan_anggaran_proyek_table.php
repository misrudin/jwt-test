<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanAnggaranProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_anggaran_proyek");
        DB::statement("
        CREATE VIEW vw_dash_laporan_anggaran_proyek AS 
        SELECT
       crs.name AS charter_running_status,
       ct.name AS proyek_name,
       null::text as model_cost,  
       ct.value AS value,
       ct.budget_allocated,
       ct.budget_spent,
       ct.budget_gap,
       null::int as absorption_percentage,
       null::int as absorption_realization,
       null::int as absorption_difference,
       null::text as budget_status   
        
       FROM charters ct
       JOIN charter_running_statuses crs ON ct.charter_running_status_code::text = crs.code::text
       WHERE (ct.deleted_at IS NULL); 
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_anggaran_proyek");
    }
}
