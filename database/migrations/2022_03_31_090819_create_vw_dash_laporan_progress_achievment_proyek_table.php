<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanProgressAchievmentProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_progress_achievment_proyek");
        DB::statement("
        CREATE VIEW vw_dash_laporan_progress_achievment_proyek AS 
        SELECT 
        CASE
            WHEN (((ct.kick_off_at)::date < (ct.planed_start_at)::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL)) THEN true
            ELSE NULL::boolean
        END AS lead,
        CASE
            WHEN (((ct.planed_start_at)::date < (now())::date) AND ((ct.planed_end_at)::date > (now())::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL) AND (ct.progress < 100)) THEN true
            ELSE NULL::boolean
        END AS lag,
        CASE
            WHEN ((ct.deleted_at IS NULL) AND (ct.closed_by IS NULL) AND ((ct.planed_end_at)::date < (now())::date)) THEN true
            ELSE NULL::boolean
        END AS delay,
       crs.name AS charter_running_status,
       ct.name AS proyek_name,
       ct.id,
       ( SELECT vwdsh.stackholder AS project_manager
          FROM vw_dash_stake_holder vwdsh
         WHERE vwdsh.code_proyek::text = ct.code::text) AS project_manager,
         ( SELECT vwdsh.username
         FROM vw_dash_stake_holder vwdsh
        WHERE vwdsh.code_proyek::text = ct.code::text) AS pm_username,
       ( SELECT count(tasks.id) AS deliverable
          FROM tasks
         WHERE tasks.charter_id::text = ct.id::text) AS deliverable,
       ( SELECT 
        count(case  when issue_status::text='Registered' then 1 else null end) AS Open
          FROM vw_dash_issue_proyek vwdip
         WHERE vwdip.code_proyek::text = ct.code::text) AS issue_open,
             ( SELECT 
        count(case  when issue_status::text='Registered' OR  issue_status::text='Closed' then 1 else null end) AS Total
          FROM vw_dash_issue_proyek vwdip
         WHERE vwdip.code_proyek::text = ct.code::text) AS issue_total,
       ct.progress,
       ct.value,
       ct.updated_at,
       ct.planed_start_at,
       ct.planed_end_at
      FROM charters ct
      LEFT JOIN charter_running_statuses crs ON (((ct.charter_running_status_code)::text = (crs.code)::text))
        WHERE (ct.deleted_at IS NULL);
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_progress_achievment_proyek");
    }
}
