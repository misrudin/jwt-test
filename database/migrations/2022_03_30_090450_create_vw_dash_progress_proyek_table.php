<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashProgressProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_progress_proyek");
        DB::statement('
        CREATE VIEW vw_dash_progress_proyek AS 
        SELECT ct.charter_running_status_code,
    crs.name AS charter_running_status,
        CASE
            WHEN (((ct.kick_off_at)::date < (ct.planed_start_at)::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL)) THEN true
            ELSE NULL::boolean
        END AS lead,
        CASE
            WHEN (((ct.planed_start_at)::date < (now())::date) AND ((ct.planed_end_at)::date > (now())::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL) AND (ct.progress < 100)) THEN true
            ELSE NULL::boolean
        END AS lag,
        CASE
            WHEN ((ct.deleted_at IS NULL) AND (ct.closed_by IS NULL) AND ((ct.planed_end_at)::date < (now())::date)) THEN true
            ELSE NULL::boolean
        END AS delay,
        ct.id,
        ct.code,
        ct.name,
        ct.descriptions,
        ct.value,
        ct.is_strategic,
        ct.progress,
        ct.project_link,
        ct.rating,
        ct.release_periode,
        ct.sub_release_periode,
        ct.budget_allocated,
        ct.budget_spent,
        ct.budget_gap,
        ct.value_category,
        ct.duration,
        ct.created_by,
        ct.updated_by,
        ct.closed_by,
        ct.monitoring_at,
        ct.kick_off_at,
        ct.closed_date,
        ct.charter_symptom_code,
        ct.charter_segment_code,
        ct.charter_category_code,
        ct.charter_type_group_code,
        ct.charter_delivery_status_code,
        ct.charter_assignment_status_code,
        ct."references",
        ct.deleted_at,
        ct.created_at AS date_proyek,
        ct.planed_start_at AS planed_start_at,
        ct.planed_end_at AS planed_end_at,
        ct.updated_at,
        ct.work_days,
        ct.is_private
        FROM (charters ct
        JOIN charter_running_statuses crs ON (((ct.charter_running_status_code)::text = (crs.code)::text)))
        WHERE (ct.deleted_at IS NULL);
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_progress_proyek");
    }
}
