<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update2EarnValueDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('earn_value_detail', function (Blueprint $table) {
            $table->decimal('actual_cost_accumulation', 19, 2, true)->nullable();
            $table->float('earn_value_bobot_accumulation')->nullable();
            $table->float('earn_value_percent_accumulation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earn_value_detail');
    }
}
