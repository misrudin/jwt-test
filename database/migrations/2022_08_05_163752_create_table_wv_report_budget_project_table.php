<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableWvReportBudgetProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS table_wv_report_budget_project");
        DB::statement("
        CREATE VIEW table_wv_report_budget_project AS 
        SELECT
	ct.id,
	crs.name AS charter_running_status,
	ct.name AS proyek_name,
	ct.value AS value,
	ct.budget_allocated,
	ct.budget_spent,
	ct.budget_gap,
	cost_types.NAME AS model_cost,
	COALESCE (
		(
		SELECT
			jsonb_agg ( jsonb_build_object ( 'khs', khs.real_total_price, 'progress', T.progress, 'bu', bu.real_total_price ) ) res 
		FROM
			tasks
			T LEFT JOIN charter_cost_bottom_ups_trans bu ON T.ID = bu.task_id
			LEFT JOIN charter_cost_khs_trans khs ON T.ID = khs.task_id 
		WHERE
			T.charter_id = ct.ID 
		),
		'{}' :: jsonb 
	) AS anggaran 
    FROM
        charters AS ct
        LEFT JOIN charter_running_statuses AS crs ON ct.charter_running_status_code :: TEXT = crs.code ::
        TEXT LEFT JOIN cost_types ON ct.cost_type_code = cost_types.code 
    WHERE
        ( ct.deleted_at IS NULL )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_wv_report_budget_project');
    }
}
