<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanCategoryProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_category_proyek");
        DB::statement("
        CREATE VIEW vw_dash_laporan_category_proyek AS 
          SELECT     
          cc.name AS category,
         ( SELECT COUNT(*) AS total_project
                FROM charters ct
               WHERE ct.charter_category_code::text = cc.code::text AND ct.deleted_at IS NULL) AS total_project,
        ( SELECT SUM(value) AS total_value
                FROM charters ct
               WHERE ct.charter_category_code::text = cc.code::text AND ct.deleted_at IS NULL) AS total_value
         FROM charter_categories cc
         WHERE (cc.deleted_at IS NULL)
         ORDER BY total_project DESC;
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_category_proyek");
    }
}
