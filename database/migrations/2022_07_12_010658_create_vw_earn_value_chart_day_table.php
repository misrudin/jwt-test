<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwEarnValueChartDayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_earn_value_chart_day");
        DB::statement("CREATE VIEW vw_earn_value_chart_day AS
        SELECT t.charter_id,
		date_part('years', evd.audit_date::date) as years,
		date_part('months', evd.audit_date::date) AS monthly,
		date_part('week', evd.audit_date::date) AS weekly,
		date_part('days', evd.audit_date::date) AS days,
		avg(evd.earn_value_percent) AS earn_value_percent,
		sum(evd.earn_value_bobot) AS earn_value_bobot,
		sum(evd.actual_cost) as actual_cost,
		MIN(evd.audit_date) as audit_date,
		sum(ev.budget_at_completion::double precision * evd.earn_value_percent / 100::double precision) AS earn_value_rupiah
        FROM earn_value_detail evd
        LEFT JOIN earn_value ev ON evd.earn_value_id::uuid = ev.id::uuid
        LEFT JOIN tasks t ON t.id::uuid = ev.task_id::uuid
        WHERE t.type::text = 'task'::text
        GROUP BY 1,2,3,4,5
        order by 2,3,4,5");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
