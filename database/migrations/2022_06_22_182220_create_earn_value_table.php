<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEarnValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earn_value', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('task_id');
            $table->decimal('budget_at_completion', 19, 2, true)->nullable();
            $table->integer('target_periode_end')->unsigned();
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earn_value');
    }
}
