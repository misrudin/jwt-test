<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanTermOfPaymentProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_term_of_payment_proyek");
        DB::statement("
        CREATE VIEW vw_dash_laporan_term_of_payment_proyek AS 
        SELECT top.partner_payment_code,
        ct.name proyek_name,
        ct.code proyek_code,
        pt.id AS id_partner,
        pt.partner_name AS name_partner, 
        pty.top_type AS type_partner,  
        ptp.name AS payment_type,
        top.description AS payment_type_description,
        ct.value as total_value,
        top.value as value,
        top.top_status,
        top.paid_at,
        top.due_date
          FROM term_of_payments top
            LEFT JOIN partner_payments pty ON top.partner_payment_code::text = pty.code::text
            LEFT JOIN partners pt ON pty.partner_code::text = pt.code::text
            LEFT JOIN pay_types ptp ON ptp.code::text = pty.pay_type_code::text
            LEFT JOIN charters ct ON ct.id = pty.charter_id
         WHERE ct.deleted_at IS NULL AND ct.closed_by IS NULL
         ORDER BY top.due_date DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_term_of_payment_proyek");
    }
}
