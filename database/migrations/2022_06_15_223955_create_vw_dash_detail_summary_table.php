<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVwDashDetailSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_detail_summary");
        DB::statement("CREATE VIEW vw_dash_detail_summary AS
         SELECT ct.progress,
            ct.id,
            cc.name as category_name,
            cvc.name AS category_value,
            ct.planed_start_at,
            ct.planed_end_at,
            date_part('day'::text, ct.planed_end_at - now()::date) AS time_remaining,
                CASE
                    WHEN ct.kick_off_at::date < ct.planed_start_at::date AND ct.closed_by IS NULL THEN 'LEAD'::text
                    WHEN ct.planed_start_at::date < now()::date AND ct.planed_end_at::date > now()::date AND ct.closed_by IS NULL AND ct.progress < '100'::numeric THEN 'LAG'::text
                    WHEN ct.closed_by IS NULL AND ct.planed_end_at::date < now()::date THEN 'DELAY'::text
                    ELSE NULL::text
                END AS status_project,
            ( SELECT count(rr.name) AS issue
                FROM risk_registers rr
                    LEFT JOIN risk_statuses rs ON rr.risk_status_code::text = rs.code::text
                    LEFT JOIN risk_categories rc ON rr.risk_category_code::text = rc.code::text
                WHERE rr.charter_id::text = ct.id::text) AS issue
        FROM charters ct
            LEFT JOIN charter_value_categories cvc ON ct.value_category::text = cvc.code::text
            LEFT JOIN charter_categories cc ON cc.code = ct.charter_category_code;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_detail_summary');
    }
}
