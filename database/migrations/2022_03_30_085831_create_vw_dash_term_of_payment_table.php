<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashTermOfPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_term_of_payment");

        DB::statement("
        CREATE VIEW vw_dash_term_of_payment AS
        SELECT 
        top.partner_payment_code,
        top.description,
        pt.code AS kode_partner,
        pt.partner_name AS nama_pertner,
        1 AS total_proyek,
        top.top_status,
        ptp.name AS jenis_top,
        pty.top_type AS tipe_partner,
        top.value,
        pty.value_total,
        top.due_date,
        ct.planed_start_at,
        ct.planed_end_at AS planed_end_at
        FROM (term_of_payments top
        LEFT JOIN partner_payments pty ON (((top.partner_payment_code)::text = (pty.code)::text))
        LEFT JOIN partners pt ON (((pty.partner_code)::text = (pt.code)::text))
        LEFT JOIN pay_types ptp ON (((ptp.code)::text = (pty.pay_type_code)::text))
        LEFT JOIN charters ct ON ((ct.id) = (pty.charter_id)))
        WHERE (ct.deleted_at IS NULL) AND (ct.closed_by IS NULL)
        ORDER BY due_date ASC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_term_of_payment');
    }
}
