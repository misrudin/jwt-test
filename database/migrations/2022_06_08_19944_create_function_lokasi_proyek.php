<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFunctionLokasiProyek extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::unprepared("
  create or replace function fn_dash_lokasi_project(dateFrom text default null, dateTo text default null)
  returns table (province text, totalProyek integer, totalNilai bigint, isu integer)
as
$$
 select p.name,
 CASE 
	 WHEN dateFrom IS null OR dateTo IS null THEN
		 (select COALESCE(sum(jumlah), 0) from vw_dash_lokasi_proyek l
		  where l.provinsi = p.name)::integer
	 ELSE
		 (select COALESCE(sum(jumlah), 0) from vw_dash_lokasi_proyek
		  where 
		  (provinsi = p.name
		   AND  planed_start_at <= Date($1) 
		   AND planed_end_at >= Date($2)) 
		   OR (provinsi = p.name 
		   AND  planed_start_at <= Date($1) 
		   AND planed_end_at >= Date($1)
		   AND planed_end_at <= Date($2)) 
		   OR (provinsi = p.name 
			AND  planed_start_at >= Date($1) 
			AND  planed_start_at <= Date($2) 
			AND planed_end_at <= Date($2)) 
		   OR (provinsi = p.name AND  planed_start_at >= Date($1) 
			AND  planed_start_at <= Date($2) 
			AND planed_end_at >= Date($2)))::integer
	END::integer AS totalProyek,
(SELECT 
	  CASE 
		 WHEN dateFrom IS null OR dateTo IS null THEN
		 (select COALESCE(sum(nilai_proyek), 0) from vw_dash_lokasi_proyek l
		  where l.provinsi = p.name)::bigint
		 ELSE
		 (select COALESCE(sum(nilai_proyek), 0) from vw_dash_lokasi_proyek 
		  where (provinsi = p.name
		   AND  planed_start_at <= Date($1) 
		   AND planed_end_at >= Date($2)) 
		   OR (provinsi = p.name 
		   AND  planed_start_at <= Date($1) 
		   AND planed_end_at >= Date($1)
		   AND planed_end_at <= Date($2)) 
		   OR (provinsi = p.name 
			   AND  planed_start_at >= Date($1) 
			   AND  planed_start_at <= Date($2) 
			   AND planed_end_at <= Date($2)) 
			   OR (provinsi = p.name AND  planed_start_at >= Date($1) 
			  AND  planed_start_at <= Date($2) 
			  AND planed_end_at >= Date($2)))::bigint
	  END)::bigint as totalNilai,  
(SELECT 
	  CASE 
		 WHEN dateFrom IS null OR dateTo IS null THEN
		 	(select COALESCE(sum(issue), 0) from vw_dash_lokasi_proyek l
			  where l.provinsi = p.name)::integer
		 ELSE
			 (select COALESCE(sum(issue), 0) from vw_dash_lokasi_proyek 
			  where (provinsi = p.name
			   AND  planed_start_at <= Date($1) 
			   AND planed_end_at >= Date($2)) 
			   OR (provinsi = p.name 
			   AND  planed_start_at <= Date($1) 
			   AND planed_end_at >= Date($1)
			   AND planed_end_at <= Date($2)) 
			   OR (provinsi = p.name 
				   AND  planed_start_at >= Date($1) 
				   AND  planed_start_at <= Date($2) 
				   AND planed_end_at <= Date($2)) 
				   OR (provinsi = p.name AND  planed_start_at >= Date($1) 
				  AND  planed_start_at <= Date($2) 
				  AND planed_end_at >= Date($2)))::integer
	  END)::integer as isu
 from provinces p
$$
language sql;
    ");
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::unprepared('DROP FUNCTION IF EXISTS fn_dash_lokasi_project');
	}
}
