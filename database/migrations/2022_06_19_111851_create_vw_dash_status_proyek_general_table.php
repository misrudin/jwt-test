<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashStatusProyekGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_status_proyek_general");

        DB::statement("
        CREATE VIEW vw_dash_status_proyek_general AS 
        SELECT ct.code AS code_proyek,
        ct.name AS nama_proyek,
        ct.descriptions AS deskripsi,
        crs.name AS status,
        ct.created_at AS date_proyek,
        ct.planed_start_at AS planed_start_at,
        ct.planed_end_at AS planed_end_at,
        '1'::text AS jumlah,
				users.tenant_id
        FROM (charters ct
        JOIN charter_running_statuses crs ON (((crs.code)::text = (ct.charter_running_status_code)::text)))
				JOIN users ON ct.created_by = users.id
        WHERE (ct.deleted_at IS NULL AND users.tenant_id != 'sda');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_status_proyek_general');
    }
}
