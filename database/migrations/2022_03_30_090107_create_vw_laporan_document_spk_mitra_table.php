<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwLaporanDocumentSpkMitraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_spk_mitra");
        // todo masih bug di karakter WHERE ((ctf.category_name)::text = "Surat Perintah Kerja (SPK)/P8"::text)
    //     DB::statement('
    //     CREATE VIEW vw_laporan_document_spk_mitra AS 
      //   SELECT pt.code AS kode_partner,
      //   pt.partner_name AS nama_pertner,
      //   ptp.name AS jenis_top,
      //   ct.id AS refrence_project_to_document
      //  FROM project_files ptf
      //    JOIN category_file ctf ON ctf.id = ptf."Category"
      //    JOIN charters ct ON ct.id = ptf."ProjectID"
      //    JOIN partner_payments pty ON pty.charter_id = ct.id
      //    JOIN partners pt ON pt.code = pty.partner_code
      //    JOIN pay_types ptp ON ptp.code =  pty.pay_type_code
      // WHERE ((ctf.category_name)::text = "Surat Perintah Kerja (SPK)/P8"::text)
    //   ');
      // sementara menggunkan ini karna blm ada untuk report ini
      DB::statement('
        CREATE VIEW vw_laporan_document_spk_mitra AS 
        SELECT
          pt.code AS kode_partner, 
          pt.partner_name AS nama_partner, 
          ptp."name" AS jenis_top, 
          ct."id" AS refrence_project_to_document, 
          pty.value_total, 
          pt.created_at AS date, 
          charter_running_statuses."name" AS status
        FROM
          project_files AS ptf
          JOIN
          category_file AS ctf
          ON 
            ctf."id" = ptf."Category"
          JOIN
          charters AS ct
          ON 
            ct."id" = ptf."ProjectID"
          JOIN
          partner_payments AS pty
          ON 
            pty.charter_id = ct."id"
          JOIN
          partners AS pt
          ON 
            pt.code = pty.partner_code
          JOIN
          pay_types AS ptp
          ON 
            ptp.code = pty.pay_type_code
          LEFT JOIN
          charter_running_statuses
          ON 
            ct.code = charter_running_statuses.code
          WHERE ct.deleted_at IS NULL
      ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_spk_mitra");

        // Schema::dropIfExists('vw_laporan_document_spk_mitra');
    }
}
