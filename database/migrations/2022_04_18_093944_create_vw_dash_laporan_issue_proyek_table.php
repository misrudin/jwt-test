<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanIssueProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_issue_proyek");
        DB::statement("
        CREATE VIEW vw_dash_laporan_issue_proyek AS
        SELECT rr.name AS issue_name,
        c.code AS code_proyek,
        c.name AS name_proyek,
        rc.name AS issue_category,
        rs.name AS issue_status,
        ( SELECT ri.name AS impacts
        FROM risk_impacts ri
        WHERE ri.code::text = rr.risk_impact_code::text AND ri.deleted_at IS NULL
        LIMIT 1) AS impacts,
        ( SELECT name AS response_plans
        FROM risk_response_plans rrp
        WHERE rrp.risk_register_code::text = rr.code::text AND rrp.deleted_at IS NULL LIMIT 1) AS response_plans,
        rr.created_at,
        rr.updated_at
        FROM (((risk_registers rr
        LEFT JOIN charters c ON (((rr.charter_id)::text = (c.id)::text)))
        LEFT JOIN risk_statuses rs ON (((rr.risk_status_code)::text = (rs.code)::text)))
        LEFT JOIN risk_categories rc ON (((rr.risk_category_code)::text = (rc.code)::text)))
        WHERE ((rr.deleted_at IS NULL) AND c.deleted_at IS NULL);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_issue_proyek");
    }
}
