<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashGrafikkProgressGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_grafikk_progress_general");
        DB::statement("
        CREATE VIEW vw_dash_grafikk_progress_general AS
        SELECT ct.name AS project,
        ct.charter_running_status_code,
        ct.progress,
        ct.value AS nilai_proyek,
        CASE
            WHEN (((ct.kick_off_at)::date < (ct.planed_start_at)::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL)) THEN true
            ELSE NULL::boolean
        END AS lead,
        CASE
            WHEN (((ct.planed_start_at)::date < (now())::date) AND ((ct.planed_end_at)::date > (now())::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL) AND (ct.progress < '100'::numeric)) THEN true
            ELSE NULL::boolean
        END AS lag,
        CASE
            WHEN ((ct.deleted_at IS NULL) AND (ct.closed_by IS NULL) AND ((ct.planed_end_at)::date < (now())::date)) THEN true
            ELSE NULL::boolean
        END AS delay,
        ( SELECT p.partner_name AS company
           FROM (partners p
             JOIN partner_payments pp ON (((pp.partner_code)::text = (p.code)::text)))
          WHERE (((pp.top_type)::text = 'CUSTOMER'::text) AND ((pp.charter_id)::text = (ct.id)::text))
         LIMIT 1) AS company,
        ( SELECT s.name AS stakeholder
           FROM ((charter_stackholders cs
             JOIN stackholders s ON (((cs.stackholder_code)::text = (s.code)::text)))
             JOIN stackholder_types st ON (((s.stackholder_type_code)::text = (st.code)::text)))
          WHERE (((st.name)::text = 'Project Manager'::text) AND (s.deleted_at IS NULL) AND (st.deleted_at IS NULL) AND ((cs.charter_code)::text = (ct.code)::text))
         LIMIT 1) AS stakeholder,
        ct.created_at AS date_proyek,
        ct.planed_start_at AS planed_start_at,
        ct.planed_end_at AS planed_end_at
        FROM (charters ct
        JOIN charter_running_statuses crs ON (((ct.charter_running_status_code)::text = (crs.code)::text)))
        LEFT JOIN
        users
        ON ct.created_by = users.id
        WHERE (ct.deleted_at IS NULL AND users.tenant_id != 'sda')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_grafikk_progress_general');
    }
}
