<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDocumentAcquisitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_document_acquisition");

        DB::statement("
        CREATE VIEW vw_document_acquisition AS 
        SELECT charters.code AS project_code,
        charters.name AS project_name,
        charters.value,
        date_part('year'::text, charters.planed_start_at) AS tahun,
        charter_segments.name AS segmen,
        stackholders.name AS pm
        FROM ((((charters
        LEFT JOIN charter_segments ON (((charter_segments.code)::text = (charters.charter_segment_code)::text)))
        JOIN charter_stackholders ON (((charter_stackholders.charter_code)::text = (charters.code)::text)))
        JOIN stackholders ON (((stackholders.code)::text = (charter_stackholders.stackholder_code)::text)))
        JOIN stackholder_types ON (((stackholder_types.code)::text = (stackholders.stackholder_type_code)::text)))
        WHERE ((stackholder_types.name)::text = 'Project Manager'::text);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_document_acquisition');
    }
}
