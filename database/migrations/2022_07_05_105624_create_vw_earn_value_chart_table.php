<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwEarnValueChartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_earn_value_chart");
        DB::statement("CREATE VIEW vw_earn_value_chart AS SELECT t.charter_id,
    to_char(evd.audit_date, 'YYYY-MM-DD'::text) AS audit_date,
    EXTRACT(week FROM evd.audit_date) AS week_num,
	avg(evd.earn_value_percent) as earn_value_percent,
	SUM(evd.earn_value_bobot) as earn_value_bobot,
    sum(ev.budget_at_completion::double precision * evd.earn_value_percent::double precision / 100::double precision) AS earn_value_rupiah
FROM earn_value_detail evd
    LEFT JOIN earn_value ev ON evd.earn_value_id = ev.id
    LEFT JOIN tasks t ON t.id = ev.task_id::uuid
  WHERE t.type::text = 'task'::text
  GROUP BY 1,2,3");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_earn_value_chart');
    }
}
