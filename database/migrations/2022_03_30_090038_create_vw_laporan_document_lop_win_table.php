<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwLaporanDocumentLopWinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_lop_win");

        DB::statement('
        CREATE VIEW vw_laporan_document_lop_win AS 
        SELECT ct.code AS project_code,
        ct.descriptions AS project_name,
        cs.name AS segment_name,
        cs.id AS segment_id,
        Null AS am,
        ct.references AS no_quote,
        Null AS no_so,
        Null AS spk,
        ptf.id AS refrence_id_document
       FROM (((project_files ptf
         JOIN category_file ctf ON ((ctf.id = ptf."Category")))
         JOIN charters ct ON ((ct.id = ptf."ProjectID")))
         JOIN charter_segments cs ON (((cs.code)::text = (ct.charter_segment_code)::text)))
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_laporan_document_lop_win");

        // Schema::dropIfExists('vw_laporan_document_lop_win');
    }
}
