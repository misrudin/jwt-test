<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashProgresProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_progres_proyek");

        DB::statement("
        CREATE VIEW vw_dash_progres_proyek AS
        SELECT count(
            CASE
                WHEN (((ct.kick_off_at)::date < (ct.planed_start_at)::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL)) THEN ct.id
                ELSE NULL::uuid
            END) AS lead,
        count(
            CASE
                WHEN (((ct.planed_start_at)::date < (now())::date) AND ((ct.planed_end_at)::date > (now())::date) AND (ct.closed_by IS NULL) AND (ct.deleted_at IS NULL) AND (ct.progress < '100'::numeric)) THEN ct.id
                ELSE NULL::uuid
            END) AS lag,
        count(
            CASE
                WHEN ((ct.deleted_at IS NULL) AND (ct.closed_by IS NULL) AND ((ct.planed_end_at)::date < (now())::date)) THEN ct.id
                ELSE NULL::uuid
            END) AS delay
       FROM charters ct
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_progres_proyek');
    }
}
