<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVwDashDetailCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::statement("DROP VIEW IF EXISTS vw_dash_detail_cost");
       DB::statement("CREATE VIEW vw_dash_detail_cost AS SELECT code AS code_proyek,
    budget_allocated AS total_anggaran,
    budget_spent AS realisasi_anggaran,
    budget_gap AS sisa_anggaran,
	id,
	CASE
		WHEN budget_allocated IS NULL OR budget_spent IS NULL THEN 0::numeric
		ELSE FLOOR(NULLIF(budget_spent, 0) / NULLIF(budget_allocated, 0) * 100)
		END as penyerapan
   FROM charters");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_detail_cost');
    }
}
