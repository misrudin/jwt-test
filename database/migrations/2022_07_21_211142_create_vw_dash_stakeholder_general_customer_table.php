<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashStakeholderGeneralCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_stakeholder_general_customer");
        DB::statement("
        CREATE VIEW vw_dash_stakeholder_general_customer AS 
        SELECT P
        .partner_name AS dataname,
        pp.top_type AS TYPE,
        SUM ( top.VALUE ) AS realisasi,
        SUM ( C.VALUE ) AS nilai_pembayaran,
        ( SELECT COUNT ( pp2.ID ) AS COUNT FROM partner_payments pp2 WHERE ( ( pp2.partner_code ) :: TEXT = ( P.code ) :: TEXT ) ) AS proyek,
        C.planed_start_at,
        C.planed_end_at AS planed_end_at,
        C.deleted_at,
        C.budget_allocated,
        C.budget_spent,
        C.progress,
        users.tenant_id
    FROM
        (
            (
                ( partners AS P JOIN partner_payments AS pp ON ( ( ( pp.partner_code ) :: TEXT = ( P.code ) :: TEXT ) ) )
                JOIN charters AS C ON ( ( pp.charter_id = C.ID ) ) 
            )
            JOIN term_of_payments AS top ON ( ( ( top.partner_payment_code ) :: TEXT = ( pp.code ) :: TEXT ) ) 
        ) 
        JOIN users ON c.created_by = users.id
    WHERE
        ( C.deleted_at IS NULL AND users.tenant_id != 'sda') 
    GROUP BY
        P.code,
        P.partner_name,
        pp.top_type,
        C.planed_start_at,
        C.progress,
        C.budget_allocated,
        C.budget_spent,
        C.planed_end_at,
        C.deleted_at ,
        users.tenant_id
    ORDER BY
        SUM ( top.VALUE ) DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_stakeholder_general_customer');
    }
}
