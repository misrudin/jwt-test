<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashResikoGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_resiko_general");
        DB::statement("
        CREATE VIEW vw_dash_resiko_general AS
        SELECT
            COUNT(ct.id) as total,
            rp.NAME AS resiko 
        FROM
            risk_probabilities rp
            LEFT JOIN charters ct ON rp.code = ct.code
            LEFT JOIN charter_stackholders ON ct.code = charter_stackholders.charter_code
            LEFT JOIN stackholders ON charter_stackholders.stackholder_code = stackholders.code
            LEFT JOIN users ON stackholders.email = users.email 
        WHERE
            ( ct.deleted_at IS NULL AND ( users.tenant_id IS NULL OR users.tenant_id <> 'sda' ) )
        GROUP BY  rp.name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_resiko_general');
    }
}
