<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwDashStakeHolderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_stake_holder CASCADE");
        DB::statement("
        CREATE VIEW vw_dash_stake_holder AS
        SELECT c.name AS name_proyek,
        c.code AS code_proyek,
        c.value AS nilai_proyek,
        1 AS total_proyek,
        c.created_at AS date_proyek,
        c.planed_start_at AS planed_start_at,
        c.planed_end_at AS planed_end_at,
        s.name AS stackholder,
        s.username,
        s.code AS stackholder_code,
        st.name AS stackholder_types,
        c.deleted_at,
        cs.created_at AS created_at
         FROM (((charter_stackholders cs
        JOIN charters c ON (((cs.charter_code)::text = (c.code)::text)))
        JOIN stackholders s ON (((cs.stackholder_code)::text = (s.code)::text)))
        JOIN stackholder_types st ON (((s.stackholder_type_code)::text = (st.code)::text)))
        WHERE (((st.name)::text = 'Project Manager'::text) AND (c.deleted_at IS NULL) AND (s.deleted_at IS NULL) AND (st.deleted_at IS NULL));
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_stake_holder");
    }
}
