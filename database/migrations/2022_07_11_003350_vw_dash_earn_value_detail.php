<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VwDashEarnValueDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_earn_value_detail");
        DB::statement("CREATE VIEW vw_dash_earn_value_detail AS
             SELECT t.charter_id,
    evd.audit_date AS earn_audit_date,
    evd.earn_value_percent_accumulation as earn_value_percent,
    evd.earn_value_bobot_accumulation as earn_value_bobot,
    evd.earn_value_percent_accumulation::double precision / ev.target_periode_end::double precision * ev.budget_at_completion::double precision AS earn_value_rupiah,
    evd.actual_cost_accumulation as actual_cost,
    evd.earn_value_percent_accumulation::double precision / ev.target_periode_end::double precision * ev.budget_at_completion::double precision - evd.actual_cost_accumulation::double precision AS cv,
    evd.earn_value_percent_accumulation::double precision / ev.target_periode_end::double precision * ev.budget_at_completion::double precision / evd.actual_cost_accumulation::double precision AS cpi,
    ev.budget_at_completion::double precision / (evd.earn_value_percent_accumulation::double precision / ev.target_periode_end::double precision * ev.budget_at_completion::double precision / evd.actual_cost_accumulation::double precision) AS eac,
    ev.budget_at_completion::double precision / (evd.earn_value_percent_accumulation::double precision / ev.target_periode_end::double precision * ev.budget_at_completion::double precision / evd.actual_cost_accumulation::double precision) - evd.actual_cost_accumulation::double precision AS etc
           FROM tasks t
           LEFT JOIN earn_value ev ON ev.task_id::uuid = t.id
           LEFT JOIN earn_value_detail evd ON evd.earn_value_id = ev.id
           WHERE evd.audit_date = (( SELECT max(evd_1.audit_date) AS max
           FROM earn_value_detail evd_1
           LEFT JOIN earn_value ev_1 ON evd_1.earn_value_id = ev_1.id
           LEFT JOIN tasks ts ON ev_1.task_id::uuid = t.id
          WHERE ts.charter_id = t.charter_id));
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_earn_value_detail');
    }
}
