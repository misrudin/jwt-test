<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVwDashLaporanProjectManagerDetailProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_dash_laporan_project_manager_detail_project");
        DB::statement(" CREATE VIEW vw_dash_laporan_project_manager_detail_project AS select 
  s.name as stakeholder_name,
  s.code as stakeholder_code,
  ctg.name as charter_type_group_name,
  NULL::text lop_code,
  NULL::numeric(5) achievement,
   CASE
            WHEN ct.kick_off_at::date < ct.planed_start_at::date AND ct.closed_by IS NULL THEN 'LEAD'::text
            WHEN ct.planed_start_at::date < now()::date AND ct.planed_end_at::date > now()::date AND ct.closed_by 
			IS NULL AND ct.progress < '100'::numeric THEN 'LAG'::text
            WHEN ct.closed_by IS NULL AND ct.planed_end_at::date < now()::date THEN 'DELAY'::text
            ELSE NULL::text
        END AS status_project,
  ct.name as project_name, 
  ct.kick_off_at as start_date,
   ct.closed_date as end_date,
  ct.code as project_code, 
  ct.value, p.partner_name, 
  pp.top_type as type_partner 
  FROM charters ct left join charter_stackholders cs on ct.code=cs.charter_code left join stackholders s 
  on cs.stackholder_code=s.code left join partner_payments pp on pp.charter_id=ct.id left join partners p 
  on pp.partner_code=p.code
  left join charter_type_groups ctg on ct.charter_type_group_code=ctg.code");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_dash_laporan_project_manager_detail_project');
    }
}
