<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwPlanValueChartYearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_plan_value_chart_year");
        DB::statement("CREATE VIEW vw_plan_value_chart_year AS
        SELECT t.charter_id,
        date_part('years', pvd.audit_date::date) AS years,
		avg(pvd.plan_value_percent) AS plan_value_percent,
		sum(pvd.plan_value_bobot) AS plan_value_bobot,
		min(pvd.audit_date) as audit_date,
		sum(ev.budget_at_completion::double precision * pvd.plan_value_percent / 100::double precision) AS plan_value_rupiah
        FROM plan_value_detail pvd
        LEFT JOIN earn_value ev ON pvd.earn_value_id::uuid = ev.id::uuid
        LEFT JOIN tasks t ON t.id::uuid = ev.task_id::uuid
        WHERE t.type::text = 'task'::text
        GROUP BY 1,2
        order by 2");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
